# Blog_VTK

Blog 주소 : https://mathmakeworld.tistory.com/
Mesh Processing 카테고리의 TMI Project글을 보시면 됩니다.

main.cpp 에서 원하는 Example Code 실행시키면 됩니다.

Example 0
 - 가장 기초되는 VTK 기본 Code
 - Cube Rendering
 
Example 1
 - VTK에서 제공하는 Collision Detection Code
 - 두 개의 Sphere가 충돌하는 Animation 제공하는
 
Example 2부터 공통으로 사용되는 Key
  - a : 각각의 Object를 이동 및 회전 Mode
  - t : 카메라 이동 및 회전 Mode
  - z : 현재 Rendering 되어있는 Object들 저장
 
Example 2
 - Bounding Box 생성하는 Code
 - Key 설명
  - space : Bounding Box Rendering 여부 결정
  - 4 : Bounding Box AABB로 Setting
  - 5 : Bounding Box OBB로 Setting
  - 6 : Bounding Box Sphere로 Setting
  
Example 3
 - Bounding Box Collision Detection Code
 - Key 설명
  - space : Bounding Box Rendering 여부 결정
  - 4 : Bounding Box AABB로 Setting 
  - 6 : Bounding Box Sphere로 Setting 
 - 4, 6 key를 누르고 마우스로 Object를 움직이면 Collision Check 됨 Collision 된 Object는 BBox 빨간색으로 변경
 
Example 4
 - Bounding Volume Hierarchy Code
 - Key 설명
  - b : Bounding Volume Hierarchy 생성
  - 0 : Depth를 0으로 Setting
  - . : Depth를 최대 Depth로 Setting
  - + : Depth를 1 증가
  - - : Depth를 1 감소
  
Example 5
 - Bounding Volume Hierarchy Collision Detection
 - Key 설명
  - 1 : BVH를 사용해서 Collision Detection을 진행
  - 2 : BVH를 사용하지 않고 전체 탐색을 하면서 Collision Detection 진행
	- 1,2 키를 눌렀을 때 수행시간이 출력됨
  - m : 마우스로 Object를 움직이면 Collision Check 됨 Collision 된 Object는 BBox 빨간색으로 변경
