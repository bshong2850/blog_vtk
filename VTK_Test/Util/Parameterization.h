#pragma once
#include "Vec.h"
#include <vtkSmartPointer.h>

#include <vector>

#include "../../Eigen/Dense"
#include "../../Eigen/Sparse"

class vtkPolyData;
class vtkIdList;
class vtkActor;

enum WeightOption
{
	Uniform,
	Harmonic
};

class  CParameterization
{
public:
	CParameterization() {};
	~CParameterization() {};

	
	
	void Parametrization(vtkSmartPointer<vtkPolyData> polyData, WeightOption wOption);

	vtkSmartPointer<vtkPolyData> boundaryLine3DPolyData;
	vtkSmartPointer<vtkPolyData> boundaryPoints3DPolyData;
	vtkSmartPointer<vtkPolyData> InnerPoints3DPolyData;

	vtkSmartPointer<vtkPolyData> innerLine2DPolyData;
	vtkSmartPointer<vtkPolyData> boundaryLine2DPolyData;
	vtkSmartPointer<vtkPolyData> boundaryPoints2DPolyData;
	vtkSmartPointer<vtkPolyData> innerPoints2DPolyData;
private:


	Eigen::SparseMatrix<double> SetUniformLaplacianSparseMatrix(vtkSmartPointer<vtkPolyData> polyData, std::vector<unsigned int> boundaryPointIndexVec);
	Eigen::SparseMatrix<double> SetHarmonicLaplacianSparseMatrix(vtkSmartPointer<vtkPolyData> polyData, std::vector<unsigned int> boundaryPointIndexVec);
	std::vector<CVec> InnerPointParameterizationSparseMatrix(Eigen::SparseMatrix<double> laplacianM, std::pair<Eigen::VectorXd, Eigen::VectorXd> bXbY);

	float GetHarmonicWeight(vtkSmartPointer<vtkPolyData> polyData, vtkIdType point1, vtkIdType point2);
	float ComputeLengthTriangle(vtkSmartPointer<vtkPolyData> polyData, unsigned int point1, unsigned int point2, unsigned int point3);
	float ComputeAngle(vtkSmartPointer<vtkPolyData> polyData, unsigned int pointIndex1, unsigned int pointIndex2, unsigned int pointIndex3);

	

	std::pair<std::vector<unsigned int>, std::vector<unsigned int>> GetBoundaryInnerIndex(vtkSmartPointer<vtkPolyData> polyData);
	std::vector<std::pair<unsigned int, unsigned int>> SetPointNeighborPair(vtkSmartPointer<vtkPolyData> polyData, std::vector<unsigned int> boundaryPointIndexVec);
	std::vector<std::pair< unsigned int, unsigned int>> SortLinePair(std::vector<std::pair< unsigned int, unsigned int>> lineIDZip);
	std::pair<std::vector<unsigned int>, std::vector<CVec>> BoundaryPointParameterization(vtkSmartPointer<vtkPolyData> polyData, std::vector<std::pair<unsigned int, unsigned int>> boundaryNeighborVec);


	std::pair<Eigen::VectorXd, Eigen::VectorXd> SetbXbY(std::pair<std::vector<unsigned int>, std::vector<CVec>> indexParameterizationPointPairVec, int size);
};