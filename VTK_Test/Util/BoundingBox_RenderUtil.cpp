#include "../Util/BoundingBox_RenderUtil.h"
#include "../Util/MeshUtil.h"

#include <vtkTransform.h>
#include <vtkTransformFilter.h>
#include <vtkOutlineFilter.h>
#include <vtkPolyDataMapper.h>
#include <vtkActor.h>
#include <vtkProperty.h>
#include <vtkNamedColors.h>
#include <vtkCubeSource.h>
#include <vtkOBBTree.h>
#include <vtkTubeFilter.h>
#include <vtkLineSource.h>
#include <vtkAppendPolyData.h>
#include <vtkSphereSource.h>

#include <time.h>

//GetAABBLineActor
//min max 값을 가지고 outline Actor로 반환
vtkSmartPointer<vtkActor> CBBox_RenderUtil::GetAABBLineActor(
	vtkSmartPointer<vtkPolyData> polyData, vtkSmartPointer<vtkMatrix4x4> matrix, std::string color)
{
	auto transformPolyData = CMeshUtil::SetTransform(polyData, matrix);

	double bound[6];
	transformPolyData->GetBounds(bound);

	double min[3] = { bound[0] , bound[2], bound[4] };
	double max[3] = { bound[1] , bound[3], bound[5] };
	
	return GetAABBLineActor(min, max, color);
}

vtkSmartPointer<vtkActor> CBBox_RenderUtil::GetAABBLineActor(CBBox_AABB aabb, std::string color)
{
	double min[3] = { aabb.GetMin().x, aabb.GetMin().y, aabb.GetMin().z };
	double max[3] = { aabb.GetMax().x, aabb.GetMax().y, aabb.GetMax().z };

	return GetAABBLineActor(min, max, color);
}

vtkSmartPointer<vtkActor> CBBox_RenderUtil::GetAABBLineActor(
	double min[3], double max[3], std::string color)
{
	double point[8][3];

	point[0][0] = min[0], point[0][1] = min[1], point[0][2] = min[2];
	point[1][0] = min[0], point[1][1] = max[1], point[1][2] = min[2];
	point[2][0] = min[0], point[2][1] = min[1], point[2][2] = max[2];
	point[3][0] = min[0], point[3][1] = max[1], point[3][2] = max[2];
	point[4][0] = max[0], point[4][1] = min[1], point[4][2] = min[2];
	point[5][0] = max[0], point[5][1] = max[1], point[5][2] = min[2];
	point[6][0] = max[0], point[6][1] = min[1], point[6][2] = max[2];
	point[7][0] = max[0], point[7][1] = max[1], point[7][2] = max[2];
	
	return  GetBoxActor(point, color);
}

//GetOBBLineActor
//OBB BBox를 Actor로 반환
vtkSmartPointer<vtkActor> CBBox_RenderUtil::GetOBBLineActor(
	vtkSmartPointer<vtkPolyData> polyData, vtkSmartPointer<vtkMatrix4x4> matrix, std::string color)
{

	double axis[3][3];
	double extent[3];
	double center[3];

	GetOBBData(polyData, matrix, axis, extent, center);

	return GetOBBLineActor(axis, extent, center, color);
}

//GetOBBLineActor
//Axis, Extent, Center 값을 받아서 Orienation Bounding Box 생성
vtkSmartPointer<vtkActor> CBBox_RenderUtil::GetOBBLineActor(
	double axis[3][3], double extent[3], double center[3], std::string color)
{
	double point[8][3];
	for (int i = 0; i < 8; ++i)
	{
		int xSign = (i < 4) ? -1 : 1;
		int ySign = (i % 2 == 0) ? -1 : 1;
		int zSign = (i % 4 < 2) ? -1 : 1;

		for (int j = 0; j < 3; ++j)
		{
			point[i][j] = center[j] + (xSign * axis[0][j] * extent[0] + ySign * axis[1][j] * extent[1] + zSign * axis[2][j] * extent[2]) * 0.5;
		}
	}

	return GetBoxActor(point, color);
}

//GetBoxActor
//8개의 Point를 Box Outline으로 반환
vtkSmartPointer<vtkActor> CBBox_RenderUtil::GetBoxActor(double point[8][3], std::string color)
{

	//   2 __________ 6
	//    /|       / |
	//   / |      /  |
	// 4 _________ 8 |
	//  |  |      |  |
	//  |  |______|__| 5
	//  | / 1     | /
	//  |/________|/
	//   3         7
	//

	vtkSmartPointer<vtkAppendPolyData> appendPolyData = vtkSmartPointer<vtkAppendPolyData>::New();
	appendPolyData->AddInputData(CMeshUtil::GetLine(point[0], point[1]));
	appendPolyData->AddInputData(CMeshUtil::GetLine(point[0], point[2]));
	appendPolyData->AddInputData(CMeshUtil::GetLine(point[1], point[3]));
	appendPolyData->AddInputData(CMeshUtil::GetLine(point[2], point[3]));

	appendPolyData->AddInputData(CMeshUtil::GetLine(point[1], point[5]));
	appendPolyData->AddInputData(CMeshUtil::GetLine(point[3], point[7]));
	appendPolyData->AddInputData(CMeshUtil::GetLine(point[0], point[4]));
	appendPolyData->AddInputData(CMeshUtil::GetLine(point[2], point[6]));

	appendPolyData->AddInputData(CMeshUtil::GetLine(point[4], point[5]));
	appendPolyData->AddInputData(CMeshUtil::GetLine(point[4], point[6]));
	appendPolyData->AddInputData(CMeshUtil::GetLine(point[5], point[7]));
	appendPolyData->AddInputData(CMeshUtil::GetLine(point[6], point[7]));

	appendPolyData->Update();
	return CMeshUtil::PolyDataToActor(appendPolyData->GetOutput(), color);

}




//ComputeOBB
//OBB를 계산(Test Code)
void CBBox_RenderUtil::GetOBBData(vtkSmartPointer<vtkPolyData> polyData, vtkSmartPointer<vtkMatrix4x4> matrix, double axis[][3], double extent[], double center[])
{
	auto transformPolyData = CMeshUtil::SetTransform(polyData, matrix);

	vtkSmartPointer<vtkOBBTree> obbTree = vtkSmartPointer<vtkOBBTree>::New();
	obbTree->SetDataSet(transformPolyData);
	
	double corner[3];
	double size[3];

	obbTree->ComputeOBB(transformPolyData, corner, axis[0], axis[1], axis[2], size);

	for (int i = 0; i < 3; i++)
	{
		extent[i] = std::sqrt(std::pow(axis[i][0], 2) + std::pow(axis[i][1], 2) + std::pow(axis[i][2], 2));
		for(int j = 0; j < 3; j++)
			axis[i][j] /= extent[i];
	}
		

	for (int i = 0; i < 3; i++)
		center[i] = corner[i] + (axis[0][i] * extent[0] + axis[1][i] * extent[1] + axis[2][i] * extent[2]) * 0.5f;

}



//GetSphereLineActor
//Sphere BBox를 Actor로 반환
vtkSmartPointer<vtkActor> CBBox_RenderUtil::GetSphereLineActor(
	vtkSmartPointer<vtkPolyData> polyData, vtkSmartPointer<vtkMatrix4x4> matrix, double radius , std::string color)
{

	double center[3];
	double maxLength = std::numeric_limits<double>::min();

	auto transformPolyData = CMeshUtil::SetTransform(polyData, matrix);
	transformPolyData->GetCenter(center);

	vtkSmartPointer<vtkSphereSource> sphere = vtkSmartPointer<vtkSphereSource>::New();
	sphere->SetRadius(radius);
	sphere->SetPhiResolution(31);
	sphere->SetThetaResolution(31);
	sphere->SetCenter(center);
	sphere->Update();

	auto sphereActor = CMeshUtil::PolyDataToActor(sphere->GetOutput(), color);
	sphereActor->PickableOff();
	sphereActor->GetProperty()->SetRepresentationToWireframe();

	return sphereActor;

}


//ComputeRadius
//Sphere BBox의 Radius를 계산
double CBBox_RenderUtil::ComputeRadius(vtkSmartPointer<vtkPolyData> polyData)
{
	double center[3];
	double maxLength = std::numeric_limits<double>::min();

	polyData->GetCenter(center);
	int nSize = polyData->GetNumberOfPoints();

	for (int i = 0; i < nSize; ++i)
	{
		double point[3];
		polyData->GetPoint(i, point);
		double length = std::sqrt((point[0] - center[0]) * (point[0] - center[0])
			+ (point[1] - center[1]) * (point[1] - center[1])
			+ (point[2] - center[2]) * (point[2] - center[2]));

		if (length > maxLength)
			maxLength = length;
	}
	return maxLength;
}

