#pragma once
#include <vtkSmartPointer.h>
#include <iostream>
#include <memory>
#include <vector>
#include "Vec.h"

class vtkPolyData;
class vtkActor;
class vtkMatrix4x4;
class CBVH;
class CBBox_AABB;
class vtkIdList;

class  CMeshUtil
{
public:
	CMeshUtil() {};
	~CMeshUtil() {};

	//RenderingUtil
	static vtkSmartPointer<vtkActor> PolyDataToActor(vtkSmartPointer<vtkPolyData> polyData, std::string setColor = "Red");
	static vtkSmartPointer<vtkActor> PolyDataToActor(vtkSmartPointer<vtkPolyData> polyData, double color[3]);
	static vtkSmartPointer<vtkActor> PolyDataIndexTriangleToActor(vtkSmartPointer<vtkPolyData> polyData, int index, std::string setColor = "Red");



	static vtkSmartPointer<vtkPolyData> PointsToPointsPolyData(std::vector<CVec> points);
	static vtkSmartPointer<vtkPolyData> GetLine(std::vector<std::pair<CVec, CVec>> lineInfo);
	static vtkSmartPointer<vtkPolyData> GetLine(double start[3], double end[3]);
	static vtkSmartPointer<vtkPolyData> SetTransform(vtkSmartPointer<vtkPolyData> polyData, vtkSmartPointer<vtkMatrix4x4> matrix);
	static vtkSmartPointer<vtkPolyData> ReadObj(std::string path);
	
	//BVH Util
	static std::shared_ptr<CBVH> GetBVH(vtkSmartPointer<vtkPolyData> polyData, vtkSmartPointer<vtkMatrix4x4> matrix);
	static bool isCollision(
		vtkSmartPointer<vtkPolyData> polyData1, std::shared_ptr<CBVH> bvh1,
		vtkSmartPointer<vtkPolyData> polyData2, std::shared_ptr<CBVH> bvh2, std::pair<int, int>& collisionPair);
	static bool isCollision(vtkSmartPointer<vtkPolyData> polyData1, std::shared_ptr<CBVH> bvh1, double rayOrigin[3], double rayDirection[3]);
		
	static void FindPointNeighbor(vtkSmartPointer<vtkPolyData> polyData, vtkIdType tmp, vtkSmartPointer<vtkIdList> nei);
	static void FindCellNeighbor(vtkSmartPointer<vtkPolyData> polyData, vtkIdType tmp, vtkSmartPointer<vtkIdList> nei);

};