#include "MeshUtil.h"

#include "Parameterization.h"
#include <iostream>
#include <cmath>
#include <vector>
#include <algorithm>

#include <time.h>

#include <vtkPolyData.h>
#include <vtkActor.h>
#include <vtkIdFilter.h>
#include <vtkFeatureEdges.h>
#include <vtkPointData.h>
#include <vtkCellData.h>
#include <vtkLine.h>
#include <vtkCellArray.h>

#include <vtkProperty.h>


#include <vtkNamedColors.h>
#include <vtkRenderWindow.h>
#include <vtkRenderer.h>
#include <vtkRenderWindowInteractor.h>

#include <queue>
// Parameterization 순서
// 1. Boundary와 안쪽의 Vertex 나눔
// 2. Boundary Point들 원하는 2D 모양에 맞게 Parameterization 실행 B_0(b_0.x, b_0.y) ~ B_n-1(b_n-1.x, b_n-1.y) 생성
// 3. 연결성 정보를 활용하여 Laplacian Matrix 생성 W_0~W_n+k-1, B_0~B_n+k-1
// 4. Laplacian Matrix Inverse 계산 후 W^-1 * B 수행
// 5. n~n+k-1 에 해당하는 안쪽 vertex들 값을가지고 Graph 생성

#define PrintLog 0

//Parameterization
//Parameterization 실행
void CParameterization::Parametrization(vtkSmartPointer<vtkPolyData> polyData, WeightOption wOption)
{
	clock_t totalComputeTime;
	totalComputeTime = clock();

	//Boundary, Inner Point로 분리
	std::pair<std::vector<unsigned int>, std::vector<unsigned int>> boundaryInnerPointIndexPair = GetBoundaryInnerIndex(polyData);
	std::vector<unsigned int> boundaryPointIndexVec = boundaryInnerPointIndexPair.first;
	std::vector<unsigned int> innerPointIndexVec = boundaryInnerPointIndexPair.second;
	std::vector<unsigned int> totalPointIndexVec;
	for (int i = 0; i < polyData->GetNumberOfPoints(); ++i)
	{
		totalPointIndexVec.push_back(i);
	}

	//Boundary가 없다면 Return
	if (boundaryPointIndexVec.size() == 0)
	{
#if PrintLog
		std::cout << "No Boundary" << std::endl;
#endif
		return;
	}

#if PrintLog
	std::cout << " Separate Boundary & Inner complete!" << std::endl;
#endif

	time_t start;
	start = clock();
	Eigen::SparseMatrix<double> laplacianM;
	//Boundary POint를 활용하여 Option에 맞는 Laplacian Matrix 생성
	switch (wOption)
	{
	case Uniform:
		laplacianM = SetUniformLaplacianSparseMatrix(polyData, boundaryPointIndexVec);
		break;
	case Harmonic:
		laplacianM = SetHarmonicLaplacianSparseMatrix(polyData, boundaryPointIndexVec);
		break;
	default:
		return;
		break;
	}
#if PrintLog
	std::cout << clock() - start << "ms" << std::endl;
	std::cout << " SetLaplacianMatrix complete!" << std::endl;
#endif

	//Boundary Point들의 Neighbor 탐색(방향성 없음)
	std::vector<std::pair<unsigned int, unsigned int>> boundaryNeighborVec;
	boundaryNeighborVec = SetPointNeighborPair(polyData, boundaryPointIndexVec);

	//Boundary Point들의 Neighbor 방향성 설정
	std::vector<std::pair<unsigned int, unsigned int>> sortBoundaryNeighborVec;
	sortBoundaryNeighborVec = SortLinePair(boundaryNeighborVec);
#if PrintLog
	std::cout << " Boundary Line Sort complete!" << std::endl;
#endif

	//Boundary Point들 Parameterization
	std::pair<std::vector<unsigned int>, std::vector<CVec>> boundary2DIndexPointPairVec;
	boundary2DIndexPointPairVec = BoundaryPointParameterization(polyData, sortBoundaryNeighborVec);
#if PrintLog
	std::cout << " Boundary Parameterization complete!" << std::endl;
#endif

	//bXbY 구하기
	std::pair<Eigen::VectorXd, Eigen::VectorXd> bXbY;
	bXbY = SetbXbY(boundary2DIndexPointPairVec, polyData->GetNumberOfPoints());
#if PrintLog
	std::cout << " Compute bXbY complete!" << std::endl;
#endif

	//Inner Point들 Parameterization
	start = clock();
	std::vector<CVec> innerParameterizationPointVec = InnerPointParameterizationSparseMatrix(laplacianM, bXbY);

#if PrintLog
	std::cout << clock() - start << "ms" << std::endl;
	std::cout << " Inner Parameterization complete!" << std::endl;

	std::cout << clock() - totalComputeTime << "ms" << std::endl;
	std::cout << " Parameterization Compute complete!" << std::endl;
#endif


	//////////////////////////////////////// Rendering에 필요한 PolyData 생성 ///////////////////////////////////////

	
	//화면에 그려줄 PolyData 생성하기
	//3D Mesh
	std::vector<std::pair<CVec, CVec>> totalBoundaryLineInfo;
	for (int i = 0; i < sortBoundaryNeighborVec.size(); ++i)
	{
		std::pair<CVec, CVec> linePair;
		linePair.first = polyData->GetPoint(sortBoundaryNeighborVec[i].first);
		linePair.second = polyData->GetPoint(sortBoundaryNeighborVec[i].second);
		totalBoundaryLineInfo.push_back(linePair);
	}
	boundaryLine3DPolyData = CMeshUtil::GetLine(totalBoundaryLineInfo);
	
	
	//Boundary 및 Inner Point PolyData 생성
	std::vector<CVec> boundaryPoints;
	std::vector<CVec> insidePoints;
	for (int i = 0; i < boundaryPointIndexVec.size(); ++i)
	{
		boundaryPoints.push_back(CVec(polyData->GetPoint(boundaryPointIndexVec[i])));
	}
	for (int i = 0; i < innerPointIndexVec.size(); ++i)
	{
		insidePoints.push_back(CVec(polyData->GetPoint(innerPointIndexVec[i])));
	}
	boundaryPoints3DPolyData = CMeshUtil::PointsToPointsPolyData(boundaryPoints);
	InnerPoints3DPolyData = CMeshUtil::PointsToPointsPolyData(insidePoints);
	

	//화면에 그려줄 PolyData 생성하기
	//2D Parameterization

	//total Line 생성
	std::vector<std::pair<unsigned int, unsigned int>> innerNeighborVec;
	innerNeighborVec = SetPointNeighborPair(polyData, totalPointIndexVec);

	//2D Parameterization total 연결 Line 그리기
	std::vector<std::pair<CVec, CVec>> totalLineInfo;
	std::vector<CVec> innerPoints2D;
	for (int i = 0; i < innerNeighborVec.size(); ++i)
	{
		auto tmp1 = std::find(boundary2DIndexPointPairVec.first.begin(), boundary2DIndexPointPairVec.first.end(), innerNeighborVec[i].first);
		
		if(tmp1 == boundary2DIndexPointPairVec.first.end())
			innerPoints2D.push_back(innerParameterizationPointVec[innerNeighborVec[i].first]);
		auto tmp2 = std::find(boundary2DIndexPointPairVec.first.begin(), boundary2DIndexPointPairVec.first.end(), innerNeighborVec[i].second);

		if (tmp1 == boundary2DIndexPointPairVec.first.end() || tmp2 == boundary2DIndexPointPairVec.first.end())
		{
			std::pair<CVec, CVec> linePair;
			linePair.first = innerParameterizationPointVec[innerNeighborVec[i].first];
			linePair.second = innerParameterizationPointVec[innerNeighborVec[i].second];
			totalLineInfo.push_back(linePair);
		}
	}
	innerLine2DPolyData = CMeshUtil::GetLine(totalLineInfo);
	innerPoints2DPolyData = CMeshUtil::PointsToPointsPolyData(innerPoints2D);
		
	//2D Boundary Line 연결
	std::vector<std::pair<CVec, CVec>> boundaryLineInfo;
	for (int i = 0; i < boundary2DIndexPointPairVec.second.size(); ++i)
	{
		std::pair<CVec, CVec> linePair;
		if (i != boundary2DIndexPointPairVec.second.size() - 1)
		{
			linePair.first = boundary2DIndexPointPairVec.second[i];
			linePair.second = boundary2DIndexPointPairVec.second[i + 1];
		}
		else
		{
			linePair.first = boundary2DIndexPointPairVec.second[i];
			linePair.second = boundary2DIndexPointPairVec.second[0];
		}
		boundaryLineInfo.push_back(linePair);
	}

	boundaryLine2DPolyData = CMeshUtil::GetLine(boundaryLineInfo);	
	boundaryPoints2DPolyData = CMeshUtil::PointsToPointsPolyData(boundary2DIndexPointPairVec.second);



	return;
}



//GetBoundaryInnerIndex
//Mesh의 Boundary 와 Inner Index를 vector로 반환
std::pair<std::vector<unsigned int>, std::vector<unsigned int>> CParameterization::GetBoundaryInnerIndex(vtkSmartPointer<vtkPolyData> polyData)
{
	vtkSmartPointer<vtkIdFilter> idFilter = vtkSmartPointer<vtkIdFilter>::New();

	idFilter->SetInputData(polyData);
	idFilter->SetPointIds(true);
	idFilter->SetCellIds(false);
	idFilter->SetPointIdsArrayName("ids");
	idFilter->Update();

	vtkSmartPointer<vtkFeatureEdges> outSideFeatureEdges =
		vtkSmartPointer<vtkFeatureEdges>::New();
	outSideFeatureEdges->SetInputData(idFilter->GetOutput());
	outSideFeatureEdges->BoundaryEdgesOn();
	outSideFeatureEdges->FeatureEdgesOff();
	outSideFeatureEdges->ManifoldEdgesOff();
	outSideFeatureEdges->NonManifoldEdgesOff();
	outSideFeatureEdges->Update();

	int pointSize = outSideFeatureEdges->GetOutput()->GetNumberOfPoints();
	vtkDataArray *_array = outSideFeatureEdges->GetOutput()->GetPointData()->GetArray("ids");
	std::pair<std::vector<unsigned int>, std::vector<unsigned int>> boundaryInnerIndexPair;
	std::vector<unsigned int> boundaryPointIndexVec;
	std::vector<unsigned int> InnerPointIndexVec;
	for (int j = 0; j < pointSize; ++j)
	{
		int index = _array->GetVariantValue(j).ToInt();
		boundaryPointIndexVec.push_back(index);
	}

	for (int i = 0; i < polyData->GetNumberOfPoints(); ++i)
	{
		auto tmp = std::find(boundaryPointIndexVec.begin(), boundaryPointIndexVec.end(), i);
		if (tmp == boundaryPointIndexVec.end())
		{
			InnerPointIndexVec.push_back(i);
		}
	}

	boundaryInnerIndexPair.first = boundaryPointIndexVec;
	boundaryInnerIndexPair.second = InnerPointIndexVec;
	return boundaryInnerIndexPair;

}


//SortLinePair
//무작위 pair로 들어온 Line 정보를 겹친 것 제외하고 하나의 Line으로 반환
std::vector<std::pair< unsigned int, unsigned int>> CParameterization::SortLinePair(std::vector<std::pair< unsigned int, unsigned int>> lineIndexZip)
{
	unsigned int start = lineIndexZip[0].first;
	std::vector<std::pair<unsigned int, unsigned int>> edgeLineIndexPairVec;
	std::queue<std::pair<unsigned int, unsigned int>> q;
	q.push(lineIndexZip[0]);
	edgeLineIndexPairVec.push_back(lineIndexZip[0]);
	lineIndexZip.erase(lineIndexZip.begin());

	while (!q.empty())
	{
		std::pair<unsigned int, unsigned int > tmp;
		tmp = q.front();
		q.pop();

		for (int i = 0; i < lineIndexZip.size(); ++i)
		{
			if (lineIndexZip[i].first == tmp.second && lineIndexZip[i].second != tmp.first)
			{
				q.push(lineIndexZip[i]);
				edgeLineIndexPairVec.push_back(lineIndexZip[i]);
				lineIndexZip.erase(lineIndexZip.begin() + i);
			}
			else if (lineIndexZip[i].first == tmp.second && lineIndexZip[i].second != tmp.first)
			{
				edgeLineIndexPairVec.push_back(lineIndexZip[i]);
			}
		}
	}

	return edgeLineIndexPairVec;
}


//SetPointNeighborPair
//Neighbor Point Index를 찾아 Line 정보를 생성 (순서 없음, 겹치는 것 존재)
std::vector<std::pair<unsigned int, unsigned int>> CParameterization::SetPointNeighborPair(vtkSmartPointer<vtkPolyData> polyData, std::vector<unsigned int> pointIndexVec)
{
	polyData->BuildLinks();
	std::vector<std::pair<unsigned int, unsigned int>> pointNeighborVec;
	for (int i = 0; i < pointIndexVec.size(); ++i)
	{
		vtkSmartPointer<vtkIdList> neighbors = vtkSmartPointer<vtkIdList>::New();
		CMeshUtil::FindPointNeighbor(polyData, pointIndexVec[i], neighbors);
		int numNei = neighbors->GetNumberOfIds();
		for (int numNeighbor = 0; numNeighbor < numNei; ++numNeighbor)
		{
			auto tmp = std::find(pointIndexVec.begin(), pointIndexVec.end(), neighbors->GetId(numNeighbor));
			if (tmp != pointIndexVec.end() && *tmp != pointIndexVec[i])
			{
				std::pair<unsigned int, unsigned int> neighborInfo;
				neighborInfo.first = pointIndexVec[i];
				neighborInfo.second = *tmp;
				pointNeighborVec.push_back(neighborInfo);
			}
		}
	}
	return pointNeighborVec;
}


//BoundaryPointParameterization
//Sort된 Boundary를 Circle에 Parameterization 적용
std::pair<std::vector<unsigned int>, std::vector<CVec>> CParameterization::BoundaryPointParameterization(vtkSmartPointer<vtkPolyData> polyData, std::vector<std::pair<unsigned int, unsigned int>> sortBoundaryNeighborVec)
{
	std::pair<std::vector<unsigned int>, std::vector<CVec>> indexParameterizationPointPairVec;
	std::vector<unsigned int> boundaryIndexVec;
	std::vector<CVec> boundaryParameterizationPointVec;
	int radius = 1;
	float totalLenth = 0;

	float *lenthVec = new float[sortBoundaryNeighborVec.size()];

	for (int i = 0; i < sortBoundaryNeighborVec.size(); ++i)
		lenthVec[i] = 0;

	for (int i = 0; i < sortBoundaryNeighborVec.size(); ++i)
	{
		CVec point1;
		CVec point2;
		point1 = CVec(polyData->GetPoint(sortBoundaryNeighborVec[i].first));
		point2 = CVec(polyData->GetPoint(sortBoundaryNeighborVec[i].second));
		float lenth = (float)sqrt(pow(point2.x - point1.x, 2) + pow(point2.y - point1.y, 2) + pow(point2.z - point1.z, 2));
		lenthVec[i] = lenth;
		totalLenth += lenth;
	}

	float totalLenthInCircle = 0;
	float totalAngleInCircle = 0;
	for (int i = 0; i < sortBoundaryNeighborVec.size(); ++i)
	{
		std::pair<unsigned int, CVec> indexParameterizationPointPair;
		CVec parameterizationPoint;
		float ratio = lenthVec[i] / totalLenth;
		float angleInCircle = ratio * 2 * 3.141592;
		totalAngleInCircle += angleInCircle;

		float x = cos(totalAngleInCircle) * radius;
		float y = sin(totalAngleInCircle) * radius;
		parameterizationPoint.x = x;
		parameterizationPoint.y = y;
		parameterizationPoint.z = 0;
		boundaryIndexVec.push_back(sortBoundaryNeighborVec[i].first);
		//parameterizationPoint.Print();
		boundaryParameterizationPointVec.push_back(parameterizationPoint);
	}
	indexParameterizationPointPairVec.first = boundaryIndexVec;
	indexParameterizationPointPairVec.second = boundaryParameterizationPointVec;
	delete[] lenthVec;
	return indexParameterizationPointPairVec;
}

//SetbXbY
//bX bY Setting
std::pair<Eigen::VectorXd, Eigen::VectorXd> CParameterization::SetbXbY(std::pair<std::vector<unsigned int>, std::vector<CVec>> indexParameterizationPointPairVec, int size)
{
	std::vector<unsigned int> boundaryIndexVec = indexParameterizationPointPairVec.first;
	std::vector<CVec> boundaryParameterizationPointVec = indexParameterizationPointPairVec.second;

	//Initialize to Zero
	Eigen::VectorXd bX = Eigen::VectorXd(size, 1);
	Eigen::VectorXd bY = Eigen::VectorXd(size, 1);
	for (int i = 0; i < size; ++i)
	{
		bX(i, 0) = 0;
		bY(i, 0) = 0;
	}
	for (int i = 0; i < boundaryIndexVec.size(); ++i)
	{
		bX(boundaryIndexVec[i], 0) = boundaryParameterizationPointVec[i].x;
		bY(boundaryIndexVec[i], 0) = boundaryParameterizationPointVec[i].y;
	}

	std::pair<Eigen::VectorXd, Eigen::VectorXd> bXbY;
	bXbY.first = bX;
	bXbY.second = bY;

	return bXbY;

}

//InnerPointParameterizationSparseMatrix
//Inner Point을 bX bY와 Laplacian Sparse Matrix를 활용하여 Parameterization 실행
std::vector<CVec> CParameterization::InnerPointParameterizationSparseMatrix(Eigen::SparseMatrix<double> laplacianM, std::pair<Eigen::VectorXd, Eigen::VectorXd> bXbY)
{
	//역행렬 계산
	std::vector<CVec> totalParameterizationPointVec;

	Eigen::SparseLU<Eigen::SparseMatrix<double> > solverA;
	solverA.compute(laplacianM);

	Eigen::MatrixXd bc;;

	bc.resize(bXbY.first.size(), 2);

	bc.block(0, 0, bXbY.first.size(), 1) = bXbY.first;
	bc.block(0, 1, bXbY.first.size(), 1) = bXbY.second;

	Eigen::MatrixXd solution = solverA.solve(bc);
	for (int i = 0; i < bXbY.first.size(); i++)
	{
		CVec point;
		point.x = solution(i, 0);
		point.y = solution(i, 1);
		point.z = 0;
		totalParameterizationPointVec.push_back(point);
	}
	return totalParameterizationPointVec;
}



//SetUniformLaplacianMatrix
//Uniform Laplacian Matrix 생성
Eigen::SparseMatrix<double> CParameterization::SetUniformLaplacianSparseMatrix(vtkSmartPointer<vtkPolyData> polyData, std::vector<unsigned int> boundaryPointIndexVec)
{
	polyData->BuildLinks();
	int totalMatrixSize = polyData->GetNumberOfPoints();
	int boundarySize = boundaryPointIndexVec.size();
	int insideSize = totalMatrixSize - boundarySize;
	Eigen::SparseMatrix<double> laplacianM(totalMatrixSize, totalMatrixSize);

	std::vector<Eigen::Triplet<double>> trp;
	for (int i = 0; i < totalMatrixSize; ++i)
	{
		//0으로 initialize
		auto tmp = std::find(boundaryPointIndexVec.begin(), boundaryPointIndexVec.end(), i);
		if (tmp != boundaryPointIndexVec.end())
		{
			//Boundary이므로 identity
			trp.push_back(Eigen::Triplet<double>(i, i, 1));
		}
		else
		{
			//Boundary가 아니므로 neighbor 탐색
			vtkSmartPointer<vtkIdList> neighbors = vtkSmartPointer<vtkIdList>::New();
			CMeshUtil::FindPointNeighbor(polyData, i, neighbors);

			int numNei = neighbors->GetNumberOfIds();

			//neighbor에 해당하는 곳 W값 부여
			for (int numNeighbor = 0; numNeighbor < numNei; ++numNeighbor)
			{
				int neighborIndex = neighbors->GetId(numNeighbor);
				trp.push_back(Eigen::Triplet<double>(i, neighborIndex, 1));
			}
			//i = i 인 곳은 -numNei 부여
			trp.push_back(Eigen::Triplet<double>(i, i, -numNei));
		}
	}

	laplacianM.setFromTriplets(trp.begin(), trp.end());
	return laplacianM;
}

Eigen::SparseMatrix<double> CParameterization::SetHarmonicLaplacianSparseMatrix(vtkSmartPointer<vtkPolyData> polyData, std::vector<unsigned int> boundaryPointIndexVec)
{
	polyData->BuildLinks();
	int totalMatrixSize = polyData->GetNumberOfPoints();
	int boundarySize = boundaryPointIndexVec.size();
	int insideSize = totalMatrixSize - boundarySize;
	Eigen::SparseMatrix<double> laplacianM(totalMatrixSize, totalMatrixSize);
	std::vector<Eigen::Triplet<double>> trp;

	//Laplacian Sparse Matrix생성
	for (int i = 0; i < totalMatrixSize; ++i)
	{
		auto tmp = std::find(boundaryPointIndexVec.begin(), boundaryPointIndexVec.end(), i);
		if (tmp != boundaryPointIndexVec.end())
		{
			//Boundary이므로 identity
			trp.push_back(Eigen::Triplet<double>(i, i, 1));
		}
		else
		{
			//Boundary가 아니므로 neighbor 탐색
			vtkSmartPointer<vtkIdList> neighbors = vtkSmartPointer<vtkIdList>::New();
			CMeshUtil::FindPointNeighbor(polyData, i, neighbors);

			int numNei = neighbors->GetNumberOfIds();

			//neighbor에 해당하는 곳 W값 부여
			float totalWeight = 0;
			for (int numNeighbor = 0; numNeighbor < numNei; ++numNeighbor)
			{
				int neighborIndex = neighbors->GetId(numNeighbor);
				if (i != neighborIndex)
				{
					float weight = GetHarmonicWeight(polyData, i, neighborIndex);
					trp.push_back(Eigen::Triplet<double>(i, neighborIndex, weight));
					totalWeight += weight;
				}
			}
			//i = i 인 곳은 -numNei 부여
			trp.push_back(Eigen::Triplet<double>(i, i, -totalWeight));
		}
	}

	laplacianM.setFromTriplets(trp.begin(), trp.end());
	return laplacianM;
}


float CParameterization::GetHarmonicWeight(vtkSmartPointer<vtkPolyData> polyData, vtkIdType point1, vtkIdType point2)
{
	// point1과 point2의 Neighbor를 다 찾아서 함께 공유하는 point3를 찾아낸다
	polyData->BuildLinks();
	vtkSmartPointer<vtkIdList> neighborCellsPoint1 = vtkSmartPointer<vtkIdList>::New();
	CMeshUtil::FindCellNeighbor(polyData, point1, neighborCellsPoint1);
	int numNeiCellsPoint1 = neighborCellsPoint1->GetNumberOfIds();

	vtkSmartPointer<vtkIdList> neighborCellsPoint2 = vtkSmartPointer<vtkIdList>::New();
	CMeshUtil::FindCellNeighbor(polyData, point2, neighborCellsPoint2);
	int numNeiCellsPoint2 = neighborCellsPoint2->GetNumberOfIds();
	
	std::vector<unsigned int> trianglePoint;
	//neighbor에 해당하는 곳 W값 부여
	for (int numNeighborCellPoint1 = 0; numNeighborCellPoint1 < numNeiCellsPoint1; ++numNeighborCellPoint1)
	{
		int neighborCellIndexPoint1 = neighborCellsPoint1->GetId(numNeighborCellPoint1);
		for (int numNeighborCellPoint2 = 0; numNeighborCellPoint2 < numNeiCellsPoint2; ++numNeighborCellPoint2)
		{
			int neighborCellIndexPoint2 = neighborCellsPoint2->GetId(numNeighborCellPoint2);
			if (neighborCellIndexPoint1 == neighborCellIndexPoint2)
			{
				const vtkIdType* pts;
				vtkIdType npts;
				polyData->GetCellPoints(neighborCellIndexPoint1, npts, pts);
				for (int j = 0; j < 3; ++j)
				{
					if (pts[j] != point1 && pts[j] != point2)
						trianglePoint.push_back(pts[j]);
				}
			}
		}
	}


	float weight = 0;
	//Weight 계산에 필요한 angle 계산
	if (trianglePoint.size() == 2)
	{
		//Inner 두개의 Triangle
		// A = arccos((b^2 + c^2 - a^2)/(2bc)) 
		float angle1 = ComputeAngle(polyData, point1, point2, trianglePoint[0]);
		float angle2 = ComputeAngle(polyData, point1, point2, trianglePoint[1]);
		weight = ((1 / tan(angle1)) + (1 / tan(angle2))) / 2;
	}
	else
	{
		//Boundary
		float angle1 = ComputeAngle(polyData, point1, point2, trianglePoint[0]);
		weight = (1 / tan(angle1)) / 2;
	}
	return weight;

}

//삼각형 둘레 계산
float CParameterization::ComputeLengthTriangle(vtkSmartPointer<vtkPolyData> polyData, unsigned int point1, unsigned int point2, unsigned int point3)
{
	CVec triPoint1 = CVec(polyData->GetPoint(point1));
	CVec triPoint2 = CVec(polyData->GetPoint(point2));
	CVec triPoint3 = CVec(polyData->GetPoint(point3));

	CVec triPoint12 = (triPoint2 - triPoint1);
	CVec triPoint23 = (triPoint3 - triPoint2);
	CVec triPoint31 = (triPoint1 - triPoint3);
	float a = sqrt(pow(triPoint12.x, 2) + pow(triPoint12.y, 2) + pow(triPoint12.z, 2));
	float b = sqrt(pow(triPoint23.x, 2) + pow(triPoint23.y, 2) + pow(triPoint23.z, 2));
	float c = sqrt(pow(triPoint31.x, 2) + pow(triPoint31.y, 2) + pow(triPoint31.z, 2));
	return a + b + c;
}

//삼각형 Harmonic에 필요한 각도 계산
float CParameterization::ComputeAngle(vtkSmartPointer<vtkPolyData> polyData, unsigned int pointIndex1, unsigned int pointIndex2, unsigned int pointIndex3)
{
	CVec triPoint1 = CVec(polyData->GetPoint(pointIndex1));
	CVec triPoint2 = CVec(polyData->GetPoint(pointIndex2));
	CVec triPoint3 = CVec(polyData->GetPoint(pointIndex3));

	CVec triPoint12 = (triPoint2 - triPoint1);
	CVec triPoint23 = (triPoint3 - triPoint2);
	CVec triPoint31 = (triPoint1 - triPoint3);
	float a = sqrt(pow(triPoint12.x, 2) + pow(triPoint12.y, 2) + pow(triPoint12.z, 2));
	float b = sqrt(pow(triPoint23.x, 2) + pow(triPoint23.y, 2) + pow(triPoint23.z, 2));
	float c = sqrt(pow(triPoint31.x, 2) + pow(triPoint31.y, 2) + pow(triPoint31.z, 2));

	float tmp = pow(b, 2) + pow(c, 2) - pow(a, 2) / (2 * b * c);

	float result = 0;
	if (tmp > 0)
	{
		int tmp2 = (tmp + 1) / 2;
		result = tmp - tmp2 * 2;
	}
	else
	{
		int tmp2 = (tmp - 1) / 2;
		result = tmp - tmp2 * 2;
	}
	//몫 나머지
	return (acos(result) * 2 * 3.141592) / 360;
}

