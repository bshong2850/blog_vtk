
#include "BoundingVolumeHierarchy.h"

// 생성자
CBVH::CBVH()
{
	Initialize();
}

// 소멸자
CBVH::~CBVH() 
{
	free(mNodes);
}

// init
// 초기화 코드
void CBVH::Initialize() 
{
	mRootNodeID = TreeNode::NULL_NODE;
	mNbNodes = 0;
	mNbAllocatedNodes = 1;

	mNodes = static_cast<TreeNode*>(malloc(static_cast<size_t>(mNbAllocatedNodes) * sizeof(TreeNode)));
	assert(mNodes);
	std::memset(mNodes, 0, static_cast<size_t>(mNbAllocatedNodes) * sizeof(TreeNode));

	for (int i = 0; i<mNbAllocatedNodes - 1; ++i) 
	{
		mNodes[i].nextNodeID = i + 1;
		mNodes[i].height = -1;
	}
	mNodes[mNbAllocatedNodes - 1].nextNodeID = TreeNode::NULL_NODE;
	mNodes[mNbAllocatedNodes - 1].height = -1;
	mFreeNodeID = 0;
}

// Reset
// BVH Reset
void CBVH::Reset() 
{
	free(mNodes);
	Initialize();
}

// AllocateNode
// BVH에 새로운 노드 할당 새로 생성된 Node ID return
int CBVH::AllocateNode() 
{
	if (mFreeNodeID == TreeNode::NULL_NODE)
	{
		assert(mNbNodes == mNbAllocatedNodes);

		int oldNbAllocatedNodes = mNbAllocatedNodes;
		mNbAllocatedNodes *= 2;
		TreeNode* oldNodes = mNodes;
		mNodes = static_cast<TreeNode*>(malloc(static_cast<size_t>(mNbAllocatedNodes) * sizeof(TreeNode)));
		assert(mNodes);
		memcpy(mNodes, oldNodes, static_cast<size_t>(mNbNodes) * sizeof(TreeNode));
		free(oldNodes);

		for (int i = mNbNodes; i<mNbAllocatedNodes - 1; i++) 
		{
			mNodes[i].nextNodeID = i + 1;
			mNodes[i].height = -1;
		}
		mNodes[mNbAllocatedNodes - 1].nextNodeID = TreeNode::NULL_NODE;
		mNodes[mNbAllocatedNodes - 1].height = -1;
		mFreeNodeID = mNbNodes;
	}

	int freeNodeID = mFreeNodeID;
	mFreeNodeID = mNodes[freeNodeID].nextNodeID;
	mNodes[freeNodeID].parentID = TreeNode::NULL_NODE;
	mNodes[freeNodeID].height = 0;
	mNbNodes++;

	return freeNodeID;
}

// ReleaseNode
// 메모리 해제 및 초기화
void CBVH::ReleaseNode(int nodeID) 
{
	assert(mNbNodes > 0);
	assert(nodeID >= 0 && nodeID < mNbAllocatedNodes);
	assert(mNodes[nodeID].height >= 0);
	mNodes[nodeID].nextNodeID = mFreeNodeID;
	mNodes[nodeID].height = -1;
	mFreeNodeID = nodeID;
	mNbNodes--;
}

// AddObjectInternal
// 입력으로 들어온 aabb를 bvh에 추가
int CBVH::AddObjectInternal(CBBox_AABB aabb) 
{
	int nodeID = AllocateNode();
	
	mNodes[nodeID].aabb.SetMin(aabb.GetMin());
	mNodes[nodeID].aabb.SetMax(aabb.GetMax());
	mNodes[nodeID].height = 0;

	InsertLeafNode(nodeID);
	assert(mNodes[nodeID].isLeaf());
	assert(nodeID >= 0);

	return nodeID;
}

// RemoveObject
// BVH에에서 nodeID에 해당하는 Object 삭제
void CBVH::RemoveObject(int nodeID) 
{
	assert(nodeID >= 0 && nodeID < mNbAllocatedNodes);
	assert(mNodes[nodeID].isLeaf());

	RemoveLeafNode(nodeID);
	ReleaseNode(nodeID);
}

// UpdateObject
// nodeID에 해당하는 Object가 이동했을 때 기존의 node의 aabb를 삭제하고 새로운 AABB를 nodeID에 해당하는 node에 추가
bool CBVH::UpdateObject(int nodeID, CBBox_AABB newAABB, bool forceReinsert) 
{
	assert(nodeID >= 0 && nodeID < mNbAllocatedNodes);
	assert(mNodes[nodeID].isLeaf());
	assert(mNodes[nodeID].height >= 0);

	if (!forceReinsert && mNodes[nodeID].aabb.isContain(newAABB)) 
		return false;

	RemoveLeafNode(nodeID);

	mNodes[nodeID].aabb = newAABB;
	assert(mNodes[nodeID].aabb.isContain(newAABB));

	InsertLeafNode(nodeID);

	return true;
}

// InsertLeafNode
// nodeID를 가지는 새로운 LeafNode 추가
// 새로운 LeafNode를 추가할 때 좌우 어느 쪽으로 들어갈지 Balance Check
void CBVH::InsertLeafNode(int nodeID) 
{
	if (mRootNodeID == TreeNode::NULL_NODE) 
	{
		mRootNodeID = nodeID;
		mNodes[mRootNodeID].parentID = TreeNode::NULL_NODE;
		return;
	}

	assert(mRootNodeID != TreeNode::NULL_NODE);

	CBBox_AABB newNodeAABB = mNodes[nodeID].aabb;
	int currentNodeID = mRootNodeID;
	while (!mNodes[currentNodeID].isLeaf()) 
	{
		int leftChild = mNodes[currentNodeID].children[0];
		int rightChild = mNodes[currentNodeID].children[1];

		double volumeAABB = mNodes[currentNodeID].aabb.GetVolume();
		CBBox_AABB mergedAABBs;
		mergedAABBs.mergeTwoAABBs(mNodes[currentNodeID].aabb, newNodeAABB);
		double mergedVolume = mergedAABBs.GetVolume();

		double costS = double(2.0) * mergedVolume;

		double costI = double(2.0) * (mergedVolume - volumeAABB);

		double costLeft;
		CBBox_AABB currentAndLeftAABB;
		currentAndLeftAABB.mergeTwoAABBs(newNodeAABB, mNodes[leftChild].aabb);
		if (mNodes[leftChild].isLeaf()) 
		{   
			costLeft = currentAndLeftAABB.GetVolume() + costI;
		}
		else 
		{
			double leftChildVolume = mNodes[leftChild].aabb.GetVolume();
			costLeft = costI + currentAndLeftAABB.GetVolume() - leftChildVolume;
		}

		double costRight;
		CBBox_AABB currentAndRightAABB;
		currentAndRightAABB.mergeTwoAABBs(newNodeAABB, mNodes[rightChild].aabb);
		if (mNodes[rightChild].isLeaf()) 
		{   
			costRight = currentAndRightAABB.GetVolume() + costI;
		}
		else 
		{
			double rightChildVolume = mNodes[rightChild].aabb.GetVolume();
			costRight = costI + currentAndRightAABB.GetVolume() - rightChildVolume;
		}

		if (costS < costLeft && costS < costRight) 
			break;

		if (costLeft < costRight) 
		{
			currentNodeID = leftChild;
		}
		else 
		{
			currentNodeID = rightChild;
		}
	}

	int siblingNode = currentNodeID;

	int oldParentNode = mNodes[siblingNode].parentID;
	int newParentNode = AllocateNode();
	mNodes[newParentNode].parentID = oldParentNode;
	mNodes[newParentNode].aabb.mergeTwoAABBs(mNodes[siblingNode].aabb, newNodeAABB);
	mNodes[newParentNode].height = mNodes[siblingNode].height + 1;
	assert(mNodes[newParentNode].height > 0);

	if (oldParentNode != TreeNode::NULL_NODE)
	{
		assert(!mNodes[oldParentNode].isLeaf());
		if (mNodes[oldParentNode].children[0] == siblingNode) 
		{
			mNodes[oldParentNode].children[0] = newParentNode;
		}
		else 
		{
			mNodes[oldParentNode].children[1] = newParentNode;
		}
		mNodes[newParentNode].children[0] = siblingNode;
		mNodes[newParentNode].children[1] = nodeID;
		mNodes[siblingNode].parentID = newParentNode;
		mNodes[nodeID].parentID = newParentNode;
	}
	else 
	{ 
		mNodes[newParentNode].children[0] = siblingNode;
		mNodes[newParentNode].children[1] = nodeID;
		mNodes[siblingNode].parentID = newParentNode;
		mNodes[nodeID].parentID = newParentNode;
		mRootNodeID = newParentNode;
	}

	currentNodeID = mNodes[nodeID].parentID;
	assert(!mNodes[currentNodeID].isLeaf());
	while (currentNodeID != TreeNode::NULL_NODE) 
	{
		currentNodeID = BalanceSubTreeAtNode(currentNodeID);
		assert(mNodes[nodeID].isLeaf());

		assert(!mNodes[currentNodeID].isLeaf());
		int leftChild = mNodes[currentNodeID].children[0];
		int rightChild = mNodes[currentNodeID].children[1];
		assert(leftChild != TreeNode::NULL_NODE);
		assert(rightChild != TreeNode::NULL_NODE);

		mNodes[currentNodeID].height = std::max(mNodes[leftChild].height, mNodes[rightChild].height) + 1;
		assert(mNodes[currentNodeID].height > 0);

		mNodes[currentNodeID].aabb.mergeTwoAABBs(mNodes[leftChild].aabb, mNodes[rightChild].aabb);

		currentNodeID = mNodes[currentNodeID].parentID;
	}
	assert(mNodes[nodeID].isLeaf());
}

// InsertLeafNode
// nodeID를 가지는 LeafNode 제거 하고 Tree 재구성
void CBVH::RemoveLeafNode(int nodeID) {

	assert(nodeID >= 0 && nodeID < mNbAllocatedNodes);
	assert(mNodes[nodeID].isLeaf());

	if (mRootNodeID == nodeID) 
	{
		mRootNodeID = TreeNode::NULL_NODE;
		return;
	}

	int parentNodeID = mNodes[nodeID].parentID;
	int grandParentNodeID = mNodes[parentNodeID].parentID;
	int siblingNodeID;
	if (mNodes[parentNodeID].children[0] == nodeID)
	{
		siblingNodeID = mNodes[parentNodeID].children[1];
	}
	else 
	{
		siblingNodeID = mNodes[parentNodeID].children[0];
	}

	if (grandParentNodeID != TreeNode::NULL_NODE) 
	{
		if (mNodes[grandParentNodeID].children[0] == parentNodeID)
		{
			mNodes[grandParentNodeID].children[0] = siblingNodeID;
		}
		else 
		{
			assert(mNodes[grandParentNodeID].children[1] == parentNodeID);
			mNodes[grandParentNodeID].children[1] = siblingNodeID;
		}
		mNodes[siblingNodeID].parentID = grandParentNodeID;
		ReleaseNode(parentNodeID);

		int currentNodeID = grandParentNodeID;
		while (currentNodeID != TreeNode::NULL_NODE)
		{
			currentNodeID = BalanceSubTreeAtNode(currentNodeID);

			assert(!mNodes[currentNodeID].isLeaf());

			int leftChildID = mNodes[currentNodeID].children[0];
			int rightChildID = mNodes[currentNodeID].children[1];

			mNodes[currentNodeID].aabb.mergeTwoAABBs(mNodes[leftChildID].aabb, mNodes[rightChildID].aabb);
			mNodes[currentNodeID].height = std::max(mNodes[leftChildID].height,	mNodes[rightChildID].height) + 1;
			assert(mNodes[currentNodeID].height > 0);

			currentNodeID = mNodes[currentNodeID].parentID;
		}
	}
	else 
	{ 
		mRootNodeID = siblingNodeID;
		mNodes[siblingNodeID].parentID = TreeNode::NULL_NODE;
		ReleaseNode(parentNodeID);
	}
}

// BalanceSubTreeAtNode
// 하위 Node들의 Balance 계산
int CBVH::BalanceSubTreeAtNode(int nodeID)
{
	assert(nodeID != TreeNode::NULL_NODE);

	TreeNode* nodeA = mNodes + nodeID;
	if (nodeA->isLeaf() || nodeA->height < 2) 
	{
		return nodeID;
	}

	int nodeBID = nodeA->children[0];
	int nodeCID = nodeA->children[1];
	assert(nodeBID >= 0 && nodeBID < mNbAllocatedNodes);
	assert(nodeCID >= 0 && nodeCID < mNbAllocatedNodes);
	TreeNode* nodeB = mNodes + nodeBID;
	TreeNode* nodeC = mNodes + nodeCID;
	int balanceFactor = nodeC->height - nodeB->height;

	if (balanceFactor > 1) 
	{
		assert(!nodeC->isLeaf());
		int nodeDID = nodeC->children[0];
		int nodeEID = nodeC->children[1];
		assert(nodeDID >= 0 && nodeDID < mNbAllocatedNodes);
		assert(nodeEID >= 0 && nodeEID < mNbAllocatedNodes);
		TreeNode* nodeD = mNodes + nodeDID;
		TreeNode* nodeE = mNodes + nodeEID;

		nodeC->children[0] = nodeID;
		nodeC->parentID = nodeA->parentID;
		nodeA->parentID = nodeCID;

		if (nodeC->parentID != TreeNode::NULL_NODE) 
		{

			if (mNodes[nodeC->parentID].children[0] == nodeID) 
			{
				mNodes[nodeC->parentID].children[0] = nodeCID;
			}
			else 
			{
				assert(mNodes[nodeC->parentID].children[1] == nodeID);
				mNodes[nodeC->parentID].children[1] = nodeCID;
			}
		}
		else 
		{
			mRootNodeID = nodeCID;
		}

		assert(!nodeC->isLeaf());
		assert(!nodeA->isLeaf());

		if (nodeD->height > nodeE->height)
		{
			nodeC->children[1] = nodeDID;
			nodeA->children[1] = nodeEID;
			nodeE->parentID = nodeID;

			nodeA->aabb.mergeTwoAABBs(nodeB->aabb, nodeE->aabb);
			nodeC->aabb.mergeTwoAABBs(nodeA->aabb, nodeD->aabb);

			nodeA->height = std::max(nodeB->height, nodeE->height) + 1;
			nodeC->height = std::max(nodeA->height, nodeD->height) + 1;
			assert(nodeA->height > 0);
			assert(nodeC->height > 0);
		}
		else 
		{  
			nodeC->children[1] = nodeEID;
			nodeA->children[1] = nodeDID;
			nodeD->parentID = nodeID;

			nodeA->aabb.mergeTwoAABBs(nodeB->aabb, nodeD->aabb);
			nodeC->aabb.mergeTwoAABBs(nodeA->aabb, nodeE->aabb);

			nodeA->height = std::max(nodeB->height, nodeD->height) + 1;
			nodeC->height = std::max(nodeA->height, nodeE->height) + 1;
			assert(nodeA->height > 0);
			assert(nodeC->height > 0);
		}

		return nodeCID;
	}

	if (balanceFactor < -1) 
	{
		assert(!nodeB->isLeaf());

		int nodeFID = nodeB->children[0];
		int nodeGID = nodeB->children[1];
		assert(nodeFID >= 0 && nodeFID < mNbAllocatedNodes);
		assert(nodeGID >= 0 && nodeGID < mNbAllocatedNodes);
		TreeNode* nodeF = mNodes + nodeFID;
		TreeNode* nodeG = mNodes + nodeGID;

		nodeB->children[0] = nodeID;
		nodeB->parentID = nodeA->parentID;
		nodeA->parentID = nodeBID;

		if (nodeB->parentID != TreeNode::NULL_NODE) 
		{

			if (mNodes[nodeB->parentID].children[0] == nodeID) 
			{
				mNodes[nodeB->parentID].children[0] = nodeBID;
			}
			else 
			{
				assert(mNodes[nodeB->parentID].children[1] == nodeID);
				mNodes[nodeB->parentID].children[1] = nodeBID;
			}
		}
		else 
		{
			mRootNodeID = nodeBID;
		}

		assert(!nodeB->isLeaf());
		assert(!nodeA->isLeaf());

		if (nodeF->height > nodeG->height) 
		{
			nodeB->children[1] = nodeFID;
			nodeA->children[0] = nodeGID;
			nodeG->parentID = nodeID;

			nodeA->aabb.mergeTwoAABBs(nodeC->aabb, nodeG->aabb);
			nodeB->aabb.mergeTwoAABBs(nodeA->aabb, nodeF->aabb);

			nodeA->height = std::max(nodeC->height, nodeG->height) + 1;
			nodeB->height = std::max(nodeA->height, nodeF->height) + 1;
			assert(nodeA->height > 0);
			assert(nodeB->height > 0);
		}
		else 
		{ 
			nodeB->children[1] = nodeGID;
			nodeA->children[0] = nodeFID;
			nodeF->parentID = nodeID;

			nodeA->aabb.mergeTwoAABBs(nodeC->aabb, nodeF->aabb);
			nodeB->aabb.mergeTwoAABBs(nodeA->aabb, nodeG->aabb);

			nodeA->height = std::max(nodeC->height, nodeF->height) + 1;
			nodeB->height = std::max(nodeA->height, nodeG->height) + 1;
			assert(nodeA->height > 0);
			assert(nodeB->height > 0);
		}

		return nodeBID;
	}

	return nodeID;
}

// GetNodeData
// nodeID에 해당하는 node의 data 반환
int CBVH::GetNodeData(int nodeID) 
{
	assert(nodeID >= 0 && nodeID < mNbAllocatedNodes);
	assert(mNodes[nodeID].isLeaf());
	return mNodes[nodeID].data;
}

// GetRootAABB
// RootNode의 AABB를 반환
CBBox_AABB CBVH::GetRootAABB() 
{
	assert(mRootNodeID >= 0 && mRootNodeID < mNbAllocatedNodes);
	return mNodes[mRootNodeID].aabb;
}

// GetTargetDepthAABBVec
// 입력으로 들어온 depth 및 leafnode에 해당하는 AABB를 모두 vector로 반환
std::vector<CBBox_AABB> CBVH::GetTargetDepthAABBVec(int findDepth)
{
	std::vector<CBBox_AABB> targetDepthAABBVec;
	int bvhrootID = GetRootNodeID();

	std::queue<std::pair<int, int>> depthQueue;
	depthQueue.push(std::pair<int, int>(bvhrootID, 0));

	// Queue에 하나라도 데이터가 있으면 진행
	while (depthQueue.size() > 0)
	{
		// Queue에 들어있는 node 번호 받아옴
		int nodeIDToVisit = depthQueue.front().first;
		if (nodeIDToVisit == TreeNode::NULL_NODE)
			continue;

		int depth = depthQueue.front().second;
		depthQueue.pop();

		// Queue에 들어있는 node 받아옴
		TreeNode* nodeToVisit = GetmNodes() + nodeIDToVisit;

		// 현재 node의 depth가 찾으려는 depth와 같다면 Bounding Box 저장
		// 아니라면 2개의 자식 노트 Queue에 저장
		if (depth == findDepth || nodeToVisit->isLeaf())
		{
			targetDepthAABBVec.push_back(nodeToVisit->aabb);
		}
		else if (depth < findDepth && !nodeToVisit->isLeaf())
		{
			depthQueue.push(std::pair<int, int>(nodeToVisit->children[0], depth + 1));
			depthQueue.push(std::pair<int, int>(nodeToVisit->children[1], depth + 1));
		}
		nodeToVisit = nullptr;
	}
	return targetDepthAABBVec;
}

// AddObject
// data에 해당하는 aabb를 Add
void CBVH::AddObject(CBBox_AABB aabb, int data) 
{
	int nodeId = AddObjectInternal(aabb);
	mNodes[nodeId].data = data;

	return ;
}