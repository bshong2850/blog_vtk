#pragma once
#include <iostream>
//Data Structure
class CTriangleIntersection
{
public:
	CTriangleIntersection() {};
	~CTriangleIntersection() {};

	int TriTriIntersectionTest3D(double p1[3], double q1[3], double r1[3],
		double p2[3], double q2[3], double r2[3],
		int * coplanar,
		double source[3], double target[3]);

	bool TriRayIntersection(double p1[3], double q1[3], double r1[3], double rayOrigin[3], double rayDirection[3]);

private:
	void Cross(double* dest, double* v1, double* v2);
	double Dot(double* v1, double* v2);
	void Sub(double* dest, double* v1, double* v2);
	void Scalar(double* dest, double alpha, double* v);
	double Orient2D(double* a, double* b, double* c);

	int IntersectionTestEdge(
		double* p1, double* q1, double* r1,
		double* p2, double* q2, double* r2);
	int IntersectionTestVertex(
		double* p1, double* q1, double* r1,
		double* p2, double* q2, double* r2);

	int ConstructIntersection(
		double* p1, double* q1, double* r1,
		double* p2, double* q2, double* r2,
		double* N1, double* N2, double* source, double* target);
	int TriTriInter3D(
		double* p1, double* q1, double* r1,
		double* p2, double* q2, double* r2, double* N1, int* coplanar,
		double dp2, double dq2, double dr2, double* source, double* target,
		double* N2);


	int CoplanarTriTri3D(
		double* p1, double* q1, double* r1,
		double* p2, double* q2, double* r2,
		double* N1);

	int TriTriOverlapTest2D(
		double* p1, double* q1, double* r1,
		double* p2, double* q2, double* r2);

	int CCWTriTriIntersection2D(
		double* p1, double* q1, double* r1,
		double* p2, double* q2, double* r2);

};