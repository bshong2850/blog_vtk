#pragma once
#include "Vec.h"

//CBBox_AABB
//AABB Class
class  CBBox_AABB
{
public:
	CBBox_AABB(double _min[], double _max[], double _center[]);
	CBBox_AABB(double _min[], double _max[]);
	CBBox_AABB(CVec _min, CVec _max);
	CBBox_AABB();
	~CBBox_AABB() {};

	void GetMax(double _max[]);
	void GetMin(double _min[]);
	void GetCenter(double _center[]);

	bool isCollision(double min1[], double max1[], double min2[], double max2[]);
	bool isCollision(CBBox_AABB target);
	bool isCollision(double rayOrigin[], double rayDirection[]);
	bool isContain(CBBox_AABB aabb);

	CVec GetMax();
	CVec GetMin();
	void SetMin(CVec _min);
	void SetMin(double minX, double minY, double minZ);
	void SetMax(CVec _max);
	void SetMax(double maxX, double maxY, double maxZ);

	CVec GetCenter();
	CVec GetExtent();
	double GetVolume();

	void CBBox_AABB::mergeTwoAABBs(CBBox_AABB aabb1, CBBox_AABB aabb2); 
	void CBBox_AABB::mergeWithAABB(CBBox_AABB aabb);
	void SetAABBForTriangle(double _axis[][3]);
	void SetAABBForTriangle(CVec* trianglePoints);

private:
	CVec min;		// AABB min ��ǥ
	CVec max;		// AABB max ��ǥ
	CVec center;	// AABB center ��ǥ
	CVec extent;	// AABB extent



};

//CBBox_OBB
//OBB Class
class  CBBox_OBB
{
public:
	CBBox_OBB(double _axis[][3], double _extent[], double _center[]);
	~CBBox_OBB() {};
	
	bool isCollision(CBBox_OBB target);
	void PrintInfo();
private:
	CVec axisX;		// OBB 3���� Axis ����
	CVec axisY;		// OBB 3���� Axis ����
	CVec axisZ;		// OBB 3���� Axis ����
	CVec extent;	// OBB Extent ��
	CVec center;	// OBB center ��ǥ
	
	CVec GetAxisX();
	CVec GetAxisY();
	CVec GetAxisZ();
	CVec GetExtent();
	CVec GetCenter();

	bool GetSeparatingPlane(CVec RPos, CVec Plane, CBBox_OBB target);
};

//CBBox_SPHERE
//Sphere Bounding Box Class
class  CBBox_SPHERE 
{
public:
	CBBox_SPHERE(double center[], double radius);
	~CBBox_SPHERE() {};

	double GetRadius();
	void GetCenter(double _center[]);

	bool isCollision(CBBox_SPHERE target);

private:
	double center[3];	// Sphere Center ��ǥ
	double radius;		// Sphere Radius

	double GetDistance(double x1[], double x2[]);
};