#include "BoundingBox.h"
#include <iostream>
#include <cmath>
#include <algorithm>

//CBBox_AABB
//생성자
CBBox_AABB::CBBox_AABB(double _min[], double _max[], double _center[])
	: min(CVec(_min)), max(CVec(_max)), center(CVec(_center))
{
}
CBBox_AABB::CBBox_AABB(double _min[], double _max[]) : min(CVec(_min)), max(CVec(_max))
{
	center = CVec((_min[0] + _max[0]) * 0.5f, (_min[1] + _max[1]) * 0.5f, (_min[2] + _max[2]) * 0.5f);
}
CBBox_AABB::CBBox_AABB(CVec _min, CVec _max) : min(CVec(_min.x, _min.y, _min.z)), max(CVec(_max.x, _max.y, _max.z))
{
	center = (min + max) * 0.5f;
}
CBBox_AABB::CBBox_AABB() : min(CVec(0, 0, 0)), max(CVec(0, 0, 0)), center(CVec(0, 0, 0))
{

}

//GetMax
//Max 좌표 반환
void CBBox_AABB::GetMax(double _max[])
{
	_max[0] = max.x;
	_max[1] = max.y;
	_max[2] = max.z;
}

//GetMin
//Min 좌표 반환
void CBBox_AABB::GetMin(double _min[])
{
	_min[0] = min.x;
	_min[1] = min.y;
	_min[2] = min.z;
}

//GetCenter
//Center 좌표 반환
void CBBox_AABB::GetCenter(double _center[])
{
	_center[0] = center.x;
	_center[1] = center.y;
	_center[2] = center.z;
}



//GetMax
//Max 좌표 벡터 반환
CVec CBBox_AABB::GetMax()
{
	return max;
}
//GetMin
//Min 좌표 벡터 반환
CVec CBBox_AABB::GetMin()
{
	return min;
}

//SetMin Max
//Min Max 좌표 설정
void CBBox_AABB::SetMin(CVec _min)
{
	min.x = _min.x;
	min.y = _min.y;
	min.z = _min.z;

}
void CBBox_AABB::SetMin(double minX, double minY, double minZ)
{
	min.x = minX;
	min.y = minY;
	min.z = minZ;
}
void CBBox_AABB::SetMax(CVec _max)
{
	max.x = _max.x;
	max.y = _max.y;
	max.z = _max.z;
}
void CBBox_AABB::SetMax(double maxX, double maxY, double maxZ)
{
	max.x = maxX;
	max.y = maxY;
	max.z = maxZ;
}


//GetCenter
//Center 좌표 벡터 반환
CVec CBBox_AABB::GetCenter()
{
	return center;
}
//GetExtent
//Center 좌표 벡터 반환
CVec CBBox_AABB::GetExtent()
{
	return GetMax() - GetMin();
}
double CBBox_AABB::GetVolume()
{
	return GetExtent().x * GetExtent().y *GetExtent().z;
}

bool CBBox_AABB::isContain(CBBox_AABB aabb) {

	bool isInside = true;
	isInside = isInside && GetMin().x <= aabb.GetMin().x;
	isInside = isInside && GetMin().y <= aabb.GetMin().y;
	isInside = isInside && GetMin().z <= aabb.GetMin().z;

	isInside = isInside && GetMax().x >= aabb.GetMax().x;
	isInside = isInside && GetMax().y >= aabb.GetMax().y;
	isInside = isInside && GetMax().z >= aabb.GetMax().z;
	return isInside;
}



//isCollision
//두 AABB의 min max 값을 가지고 Collision Check
bool CBBox_AABB::isCollision(double min1[], double max1[], double min2[], double max2[])
{
	if (max2[0] < min1[0] || max1[0] < min2[0])
		return false;
	if (max2[1] < min1[1] || max1[1] < min2[1])
		return false;
	if (max2[2] < min1[2] || max1[2] < min2[2])
		return false;
	return true;
}
bool CBBox_AABB::isCollision(CBBox_AABB target)
{		
	if (GetMax().x < target.GetMin().x || target.GetMax().x < GetMin().x)
		return false;
	if (GetMax().y < target.GetMin().y || target.GetMax().y < GetMin().y)
		return false;
	if (GetMax().z < target.GetMin().z || target.GetMax().z < GetMin().z)
		return false;
	return true;
}

bool CBBox_AABB::isCollision(double rayOrigin[], double rayDirection[])
{
	CVec dirfrac;
	dirfrac.x = 1.0f / rayDirection[0];
	dirfrac.y = 1.0f / rayDirection[1];
	dirfrac.z = 1.0f / rayDirection[2];

	double t1 = (GetMin().x - rayOrigin[0])*dirfrac.x;
	double t2 = (GetMax().x - rayOrigin[0])*dirfrac.x;
	double t3 = (GetMin().y - rayOrigin[1])*dirfrac.y;
	double t4 = (GetMax().y - rayOrigin[1])*dirfrac.y;
	double t5 = (GetMin().z - rayOrigin[2])*dirfrac.z;
	double t6 = (GetMax().z - rayOrigin[2])*dirfrac.z;

	double tmin = std::max(std::max(std::min(t1, t2), std::min(t3, t4)), std::min(t5, t6));
	double tmax = std::min(std::min(std::max(t1, t2), std::max(t3, t4)), std::max(t5, t6));
	double t;

	// if tmax < 0, ray (line) is intersecting AABB, but the whole AABB is behind us
	if (tmax < 0)
	{
		t = tmax;
		return false;
	}

	// if tmin > tmax, ray doesn't intersect AABB
	if (tmin > tmax)
	{
		t = tmax;
		return false;
	}

	t = tmin;
	return true;

	
}


// Merge the AABB in parameter with the current one
void CBBox_AABB::mergeWithAABB(CBBox_AABB aabb) {

	SetMin(std::min(GetMin().x, aabb.GetMin().x), std::min(GetMin().y, aabb.GetMin().y), std::min(GetMin().z, aabb.GetMin().z));
	SetMax(std::max(GetMax().x, aabb.GetMax().x), std::max(GetMax().y, aabb.GetMax().y), std::max(GetMax().z, aabb.GetMax().z));
}

// Replace the current AABB with a new AABB that is the union of two AABBs in parameters
void CBBox_AABB::mergeTwoAABBs(CBBox_AABB aabb1, CBBox_AABB aabb2) {

	SetMin(std::min(aabb1.GetMin().x, aabb2.GetMin().x), std::min(aabb1.GetMin().y, aabb2.GetMin().y), std::min(aabb1.GetMin().z, aabb2.GetMin().z));
	SetMax(std::max(aabb1.GetMax().x, aabb2.GetMax().x), std::max(aabb1.GetMax().y, aabb2.GetMax().y), std::max(aabb1.GetMax().z, aabb2.GetMax().z));
}


void CBBox_AABB::SetAABBForTriangle(double tps[][3]) {
	CVec trianglePoints[3] = { CVec(tps[0][0] , tps[0][1], tps[0][2]),
		CVec(tps[1][0] , tps[1][1], tps[1][2]),
		CVec(tps[2][0] , tps[2][1], tps[2][2]) };
	SetAABBForTriangle(trianglePoints);
}

// Create and return an AABB for a triangle
void CBBox_AABB::SetAABBForTriangle(CVec* trianglePoints) {


	/*std::cout << " Triangle Points = " << std::endl;
	for (int i = 0; i < 3; ++i)
	{
		trianglePoints[i].Print();
	}*/

	CVec minTmp(trianglePoints[0].x, trianglePoints[0].y, trianglePoints[0].z);
	CVec maxTmp(trianglePoints[0].x, trianglePoints[0].y, trianglePoints[0].z);

	if (trianglePoints[1].x < minTmp.x) 
		minTmp.x = trianglePoints[1].x;
	if (trianglePoints[1].y < minTmp.y) 
		minTmp.y = trianglePoints[1].y;
	if (trianglePoints[1].z < minTmp.z) 
		minTmp.z = trianglePoints[1].z;

	if (trianglePoints[2].x < minTmp.x) 
		minTmp.x = trianglePoints[2].x;
	if (trianglePoints[2].y < minTmp.y) 
		minTmp.y = trianglePoints[2].y;
	if (trianglePoints[2].z < minTmp.z) 
		minTmp.z = trianglePoints[2].z;

	/*std::cout << " minTmp = ";

	std::cout << minTmp.x << " // " << minTmp.y << " // " << minTmp.z << std::endl;*/

	SetMin(minTmp);

	/*std::cout << " min = ";
	GetMin().Print();*/

	if (trianglePoints[1].x > maxTmp.x) maxTmp.x = trianglePoints[1].x;
	if (trianglePoints[1].y > maxTmp.y) maxTmp.y = trianglePoints[1].y;
	if (trianglePoints[1].z > maxTmp.z) maxTmp.z = trianglePoints[1].z;

	if (trianglePoints[2].x > maxTmp.x) maxTmp.x = trianglePoints[2].x;
	if (trianglePoints[2].y > maxTmp.y) maxTmp.y = trianglePoints[2].y;
	if (trianglePoints[2].z > maxTmp.z) maxTmp.z = trianglePoints[2].z;

	/*std::cout << " maxTmp = ";
	maxTmp.Print();*/

	SetMax(maxTmp);

	/*std::cout << " max = ";
	GetMax().Print();
	std::cout << std::endl;*/


	return ;
}






//CBBox_OBB
//생성자
CBBox_OBB::CBBox_OBB(double _axis[][3], double _extent[], double _center[])
	: axisX(CVec(_axis[0])), axisY(CVec(_axis[1])), axisZ(CVec(_axis[2])), extent(CVec(_extent)), center(CVec(_center))
{
}

//GetAxisX
//첫번째 Axis 벡터 반환
CVec CBBox_OBB::GetAxisX()
{
	return axisX;
}
//GetAxisY
//두번째 Axis 벡터 반환
CVec CBBox_OBB::GetAxisY()
{
	return axisY;
}
//GetAxisZ
//세번째 Axis 벡터 반환
CVec CBBox_OBB::GetAxisZ()
{
	return axisZ;
}
//GetExtent
//세 축에 해당하는 Extent 값 반환
CVec CBBox_OBB::GetExtent()
{
	return extent;
}
//GetCenter
//OBB Center 값 반환
CVec CBBox_OBB::GetCenter()
{
	return center;
}
//PrintInfo
//각 값들 Print
void CBBox_OBB::PrintInfo()
{
	std::cout << " axisX = ";
	axisX.Print();
	std::cout << " axisY = ";
	axisY.Print();
	std::cout << " axisZ = ";
	axisZ.Print();
	std::cout << " extent = ";
	extent.Print();
	std::cout << " center = ";
	center.Print();

	std::cout << std::endl;
} 
 
//isCollision
//자기 자신과 target OBB간의 충돌 처리 체크
bool CBBox_OBB::isCollision(CBBox_OBB target)
{
	// 두 OBB의 센터 사이의 거리
	CVec RPos;
	RPos = GetCenter() - target.GetCenter();
	
	// 총 15가지 분리축 후보를 가지고 각 후보 축에서 분리가 되는지 확인
	return !(GetSeparatingPlane(RPos, GetAxisX(), target) ||
		GetSeparatingPlane(RPos, GetAxisY(), target) ||
		GetSeparatingPlane(RPos, GetAxisZ(), target) ||
		GetSeparatingPlane(RPos, target.GetAxisX(), target) ||
		GetSeparatingPlane(RPos, target.GetAxisY(), target) ||
		GetSeparatingPlane(RPos, target.GetAxisZ(), target) ||
		GetSeparatingPlane(RPos, GetAxisX().Cross(target.GetAxisX()), target) ||
		GetSeparatingPlane(RPos, GetAxisX().Cross(target.GetAxisY()), target) ||
		GetSeparatingPlane(RPos, GetAxisX().Cross(target.GetAxisZ()), target) ||
		GetSeparatingPlane(RPos, GetAxisY().Cross(target.GetAxisX()), target) ||
		GetSeparatingPlane(RPos, GetAxisY().Cross(target.GetAxisY()), target) ||
		GetSeparatingPlane(RPos, GetAxisY().Cross(target.GetAxisZ()), target) ||
		GetSeparatingPlane(RPos, GetAxisZ().Cross(target.GetAxisX()), target) ||
		GetSeparatingPlane(RPos, GetAxisZ().Cross(target.GetAxisY()), target) ||
		GetSeparatingPlane(RPos, GetAxisZ().Cross(target.GetAxisZ()), target));
}


//GetSeparatingPlane
//분리축 이론에 따라 SeparartingAxis 기준으로 분리가 되는지 확인
bool CBBox_OBB::GetSeparatingPlane(CVec RPos, CVec SeparartingAxis, CBBox_OBB target)
{
	double centerDist = fabs(RPos.Dot(SeparartingAxis));
	double axisXDist = fabs((GetAxisX() * GetExtent().x * 0.5).Dot(SeparartingAxis));
	double axisYDist = fabs((GetAxisY() * GetExtent().y * 0.5).Dot(SeparartingAxis));
	double axisZDist = fabs((GetAxisZ() * GetExtent().z * 0.5).Dot(SeparartingAxis));
	double targetAxisXDist = fabs((target.GetAxisX() * target.GetExtent().x * 0.5).Dot(SeparartingAxis));
	double targetAxisYDist = fabs((target.GetAxisY() * target.GetExtent().y * 0.5).Dot(SeparartingAxis));
	double targetAxisZDist = fabs((target.GetAxisZ() * target.GetExtent().z * 0.5).Dot(SeparartingAxis));
	
	//두 센터 사이의 거리를 Plane에 투영시킨 것 보다 6 Vector를 투영시킨 거리의 합이 더 작으면 분리되는 축이 존재
	if (centerDist > axisXDist + axisYDist + axisZDist + targetAxisXDist + targetAxisYDist + targetAxisZDist)
	{
		return true;
	}
	else
		return false;
}

//CBBox_SPHERE
//생성자
CBBox_SPHERE::CBBox_SPHERE(double _center[], double _radius) : radius(_radius)
{
	for (int i = 0; i < 3; ++i)
	{
		center[i] = _center[i];
	}
}

//GetRadius
//Radius 반환
double CBBox_SPHERE::GetRadius()
{
	return radius;
}

//GetCenter
//Center 좌표 반환
void CBBox_SPHERE::GetCenter(double _center[])
{
	for (int i = 0; i < 3; ++i)
	{
		_center[i] = center[i];
	}
}

//isCollision
//두 AABB의 min max 값을 가지고 Collision Check
bool CBBox_SPHERE::isCollision(CBBox_SPHERE target)
{
	double targetCenter[3];
	target.GetCenter(targetCenter);	
	if (radius + target.GetRadius() >= GetDistance(center, targetCenter))
		return true;
	return false;
}

//GetDistance
//두 좌표의 거리를 반환
double CBBox_SPHERE::GetDistance(double x1[], double x2[])
{
	return std::sqrt(std::pow(x1[0] - x2[0], 2) + std::pow(x1[1] - x2[1], 2) + std::pow(x1[2] - x2[2], 2));
}