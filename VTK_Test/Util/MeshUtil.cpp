#include "MeshUtil.h"


#include <vtkNamedColors.h>
#include <vtkMatrix4x4.h>
#include <vtkPolyData.h>
#include <vtkPolyDataMapper.h>
#include <vtkActor.h>
#include <vtkProperty.h>
#include <vtkOBJReader.h>

#include <vtkTransform.h>
#include <vtkTransformPolyDataFilter.h>
#include "../Util/BoundingVolumeHierarchy.h"
#include "../Util/TriangleIntersection.h"


#include <vtkVertexGlyphFilter.h>
#include <vtkLineSource.h>
#include <vtkAppendPolyData.h>
#include <vtkLine.h>

vtkSmartPointer<vtkActor> CMeshUtil::PolyDataToActor(vtkSmartPointer<vtkPolyData> polyData, std::string setColor)
{
	double color[3];
	vtkSmartPointer<vtkNamedColors> colors = vtkSmartPointer<vtkNamedColors>::New();
	color[0] = colors->GetColor3d(setColor).GetData()[0];
	color[1] = colors->GetColor3d(setColor).GetData()[1];
	color[2] = colors->GetColor3d(setColor).GetData()[2];

	return PolyDataToActor(polyData, color);
}


vtkSmartPointer<vtkActor> CMeshUtil::PolyDataToActor(vtkSmartPointer<vtkPolyData> polyData, double color[3])
{
	vtkSmartPointer<vtkPolyDataMapper> mapper = vtkSmartPointer<vtkPolyDataMapper>::New();
	mapper->SetInputData(polyData);

	vtkSmartPointer<vtkActor> actor = vtkSmartPointer<vtkActor>::New();
	actor->SetMapper(mapper);

	actor->GetProperty()->SetColor(color);
	return actor;
}

vtkSmartPointer<vtkActor> CMeshUtil::PolyDataIndexTriangleToActor(vtkSmartPointer<vtkPolyData> polyData, int index, std::string setColor)
{
	vtkSmartPointer<vtkPoints> points = vtkSmartPointer<vtkPoints>::New();


	const vtkIdType* pts;
	vtkIdType npts;
	polyData->GetCellPoints(index, npts, pts);
	double trianglePoints[3][3];
	for (int pointIndex = 0; pointIndex < 3; ++pointIndex)
	{
		polyData->GetPoint(pts[pointIndex], trianglePoints[pointIndex]);
	}

	for (int i = 0; i < 3; ++i)
	{
		double point[3];
		point[0] = trianglePoints[i][0];
		point[1] = trianglePoints[i][1];
		point[2] = trianglePoints[i][2];
		points->InsertNextPoint(point);
	}

	vtkSmartPointer<vtkPolyData> pointsPolydata = vtkSmartPointer<vtkPolyData>::New();
	pointsPolydata->SetPoints(points);
	vtkSmartPointer<vtkVertexGlyphFilter> vertexFilter = vtkSmartPointer<vtkVertexGlyphFilter>::New();
	vertexFilter->SetInputData(pointsPolydata);
	vertexFilter->Update();
	vtkSmartPointer<vtkPolyData> pointsPolyData = vtkSmartPointer<vtkPolyData>::New();
	pointsPolyData->ShallowCopy(vertexFilter->GetOutput());


	vtkSmartPointer<vtkAppendPolyData> appendPolyData = vtkSmartPointer<vtkAppendPolyData>::New();
	appendPolyData->AddInputData(pointsPolyData);
	appendPolyData->AddInputData(CMeshUtil::GetLine(trianglePoints[0], trianglePoints[1]));
	appendPolyData->AddInputData(CMeshUtil::GetLine(trianglePoints[1], trianglePoints[2]));
	appendPolyData->AddInputData(CMeshUtil::GetLine(trianglePoints[2], trianglePoints[0]));
	appendPolyData->Update();

	vtkSmartPointer<vtkActor> triangleActor = vtkSmartPointer<vtkActor>::New();
	triangleActor = PolyDataToActor(appendPolyData->GetOutput(), setColor);
	triangleActor->GetProperty()->SetPointSize(4.0f);
	triangleActor->GetProperty()->SetLineWidth(4.0f);

	return triangleActor;

}

//PointsToPointsPolyData
//Point들을 받아와서 point PolyData로 반환
vtkSmartPointer<vtkPolyData> CMeshUtil::PointsToPointsPolyData(std::vector<CVec> pts)
{
	vtkSmartPointer<vtkPoints> points =
		vtkSmartPointer<vtkPoints>::New();

	size_t numPoints = pts.size();
	for (int i = 0; i < numPoints; ++i)
	{
		double point[3];
		point[0] = pts[i].x;
		point[1] = pts[i].y;
		point[2] = pts[i].z;
		points->InsertNextPoint(point);
	}

	vtkSmartPointer<vtkPolyData> pointsPolydata =
		vtkSmartPointer<vtkPolyData>::New();

	pointsPolydata->SetPoints(points);

	vtkSmartPointer<vtkVertexGlyphFilter> vertexFilter =
		vtkSmartPointer<vtkVertexGlyphFilter>::New();
	vertexFilter->SetInputData(pointsPolydata);
	vertexFilter->Update();

	return vertexFilter->GetOutput();

}

//GetLine
//line Information을 받아와서 line PolyData로 반환
vtkSmartPointer<vtkPolyData> CMeshUtil::GetLine(std::vector<std::pair<CVec, CVec>> lineInfo)
{
	vtkSmartPointer<vtkPoints> vtklinePoints =
		vtkSmartPointer<vtkPoints>::New();

	size_t lineInfoSize = lineInfo.size();
	size_t linePointSize = lineInfoSize * 2;
	for (int i = 0; i < lineInfoSize; ++i)
	{
		vtklinePoints->InsertNextPoint(lineInfo[i].first.x, lineInfo[i].first.y, lineInfo[i].first.z);
		vtklinePoints->InsertNextPoint(lineInfo[i].second.x, lineInfo[i].second.y, lineInfo[i].second.z);
	}

	vtkSmartPointer<vtkCellArray> lines =
		vtkSmartPointer<vtkCellArray>::New();
	vtkSmartPointer<vtkPolyData> linesPolyData =
		vtkSmartPointer<vtkPolyData>::New();
	for (int i = 0; i < linePointSize - 1; i += 2)
	{
		vtkSmartPointer<vtkLine> line =
			vtkSmartPointer<vtkLine>::New();
		line->GetPointIds()->SetId(0, i);
		line->GetPointIds()->SetId(1, i + 1);
		lines->InsertNextCell(line);
	}

	linesPolyData->SetPoints(vtklinePoints);
	linesPolyData->SetLines(lines);

	return linesPolyData;

}














//GetLine
//Line를 생성
vtkSmartPointer<vtkPolyData> CMeshUtil::GetLine(double start[3], double end[3])
{
	vtkSmartPointer<vtkLineSource> lineSource = vtkSmartPointer<vtkLineSource>::New();
	lineSource->SetPoint1(start);
	lineSource->SetPoint2(end);
	lineSource->Update();
	return lineSource->GetOutput();
}

vtkSmartPointer<vtkPolyData> CMeshUtil::SetTransform(vtkSmartPointer<vtkPolyData> polyData, vtkSmartPointer<vtkMatrix4x4> matrix)
{
	vtkSmartPointer<vtkTransform> transform = vtkSmartPointer<vtkTransform>::New();
	transform->SetMatrix(matrix);

	vtkSmartPointer<vtkTransformPolyDataFilter> transformFilter = vtkSmartPointer<vtkTransformPolyDataFilter>::New();
	transformFilter->SetInputData(polyData);
	transformFilter->SetTransform(transform);
	transformFilter->Update();
	
	return transformFilter->GetOutput();
}

//ReadObj
//path에 있는 OBJ를 읽어서 Polydata로 return
vtkSmartPointer<vtkPolyData> CMeshUtil::ReadObj(std::string path)
{
	vtkSmartPointer<vtkPolyData> obj = vtkSmartPointer<vtkPolyData>::New();
	vtkSmartPointer<vtkOBJReader> reader = vtkSmartPointer<vtkOBJReader>::New();
	reader->SetFileName(path.c_str());
	reader->Update();

	obj->DeepCopy(reader->GetOutput());

	return obj;

}


std::shared_ptr<CBVH> CMeshUtil::GetBVH(vtkSmartPointer<vtkPolyData> polyData, vtkSmartPointer<vtkMatrix4x4> matrix)
{
	auto transformPolyData = SetTransform(polyData, matrix);
	
	//bvh 생성
	std::shared_ptr<CBVH> tree = std::make_shared<CBVH>();

	//모든 Face를 돌면서 각 Face의 AABB를 tree에 넣어줌
	for (int i = 0; i < transformPolyData->GetNumberOfCells(); ++i)
	{
		const vtkIdType* pts;
		vtkIdType npts;
		transformPolyData->GetCellPoints(i, npts, pts);

		double trianglePoints[3][3];
		for (int pointIndex = 0; pointIndex < 3; ++pointIndex)
		{
			transformPolyData->GetPoint(pts[pointIndex], trianglePoints[pointIndex]);
		}
		CBBox_AABB aabb;
		
		//Face의 3 point를 가지고 AABB 생성
		aabb.SetAABBForTriangle(trianglePoints);
		tree->AddObject(aabb, i);
	}

	return tree;
}


#include <stack>
bool CMeshUtil::isCollision(
	vtkSmartPointer<vtkPolyData> polyData1,	std::shared_ptr<CBVH> bvh1, 
	vtkSmartPointer<vtkPolyData> polyData2, std::shared_ptr<CBVH> bvh2, std::pair<int, int>& collisionPair)
{
	int bvh1rootID = bvh1->GetRootNodeID();
	int bvh2rootID = bvh2->GetRootNodeID();

	std::stack<std::pair<int, int>> collisionStack;
	collisionStack.push(std::pair<int, int>(bvh1rootID, bvh2rootID));

	int isTriCollision;

	
	

	// While there are still nodes to visit
	while (collisionStack.size() > 0)
	{
		// Get the next node ID to visit
		std::pair<int, int> nodeIDToVisit = collisionStack.top();
		collisionStack.pop();

		// Skip it if it is a null node
		if (nodeIDToVisit.first == TreeNode::NULL_NODE || nodeIDToVisit.second == TreeNode::NULL_NODE)
			continue;

		// Get the corresponding node
		TreeNode* nodeToVisit1 = bvh1->GetmNodes() + nodeIDToVisit.first;
		TreeNode* nodeToVisit2 = bvh2->GetmNodes() + nodeIDToVisit.second;

		// If the AABB in parameter overlaps with the AABB of the node to visit
		if (nodeToVisit1->aabb.isCollision(nodeToVisit2->aabb)) {
			if (nodeToVisit1->isLeaf() && nodeToVisit2->isLeaf())
			{
				const vtkIdType* pts1;
				vtkIdType npts1;
				polyData1->GetCellPoints(nodeToVisit1->data, npts1, pts1);
				double triangle1Points[3][3];
				for (int pointIndex = 0; pointIndex < 3; ++pointIndex)
				{
					polyData1->GetPoint(pts1[pointIndex], triangle1Points[pointIndex]);
				}

				const vtkIdType* pts2;
				vtkIdType npts2;
				polyData2->GetCellPoints(nodeToVisit2->data, npts2, pts2);
				double triangle2Points[3][3];
				for (int pointIndex = 0; pointIndex < 3; ++pointIndex)
				{
					polyData2->GetPoint(pts2[pointIndex], triangle2Points[pointIndex]);
				}


				int tmp = 1;
				int * coplanar = &tmp;
				double source[3];
				double dest[3];

				collisionPair.first = nodeToVisit1->data;
				collisionPair.second = nodeToVisit2->data;
				CTriangleIntersection tritriIntersection;
				isTriCollision = tritriIntersection.TriTriIntersectionTest3D(
					triangle1Points[0], triangle1Points[1], triangle1Points[2],
					triangle2Points[0], triangle2Points[1], triangle2Points[2], coplanar, source, dest);

				if(isTriCollision == 1)
					return true;
			}
			else if (nodeToVisit1->isLeaf() && !nodeToVisit2->isLeaf())
			{
				collisionStack.push(std::pair<int, int>(nodeIDToVisit.first, nodeToVisit2->children[0]));
				collisionStack.push(std::pair<int, int>(nodeIDToVisit.first, nodeToVisit2->children[1]));
			}
			else if (!nodeToVisit1->isLeaf() && nodeToVisit2->isLeaf())
			{
				collisionStack.push(std::pair<int, int>(nodeToVisit1->children[0], nodeIDToVisit.second));
				collisionStack.push(std::pair<int, int>(nodeToVisit1->children[1], nodeIDToVisit.second));
			}
			else
			{
				collisionStack.push(std::pair<int, int>(nodeToVisit1->children[0], nodeToVisit2->children[0]));
				collisionStack.push(std::pair<int, int>(nodeToVisit1->children[1], nodeToVisit2->children[0]));
				collisionStack.push(std::pair<int, int>(nodeToVisit1->children[0], nodeToVisit2->children[1]));
				collisionStack.push(std::pair<int, int>(nodeToVisit1->children[1], nodeToVisit2->children[1]));
			}
		}
		nodeToVisit1->NULL_NODE;
		nodeToVisit2->NULL_NODE;
	}

	return false;
}

bool CMeshUtil::isCollision(vtkSmartPointer<vtkPolyData> polyData, std::shared_ptr<CBVH> bvh, double rayOrigin[3], double rayDirection[3])
{
	int bvh1rootID = bvh->GetRootNodeID();

	std::stack<int> collisionStack;
	collisionStack.push(bvh1rootID);

	// While there are still nodes to visit
	while (collisionStack.size() > 0)
	{
		// Get the next node ID to visit
		int nodeIDToVisit = collisionStack.top();
		collisionStack.pop();

		// Skip it if it is a null node
		if (nodeIDToVisit == TreeNode::NULL_NODE)
			continue;

		// Get the corresponding node
		TreeNode* nodeToVisit1 = bvh->GetmNodes() + nodeIDToVisit;

		// If the AABB in parameter overlaps with the AABB of the node to visit
		if (nodeToVisit1->aabb.isCollision(rayOrigin, rayDirection)) {
			if (nodeToVisit1->isLeaf())
			{
				const vtkIdType* pts;
				vtkIdType npts;
				polyData->GetCellPoints(nodeToVisit1->data, npts, pts);
				double trianglePoints[3][3];
				for (int pointIndex = 0; pointIndex < 3; ++pointIndex)
				{
					polyData->GetPoint(pts[pointIndex], trianglePoints[pointIndex]);
				}

				CTriangleIntersection triangleIntersection;
				triangleIntersection.TriRayIntersection(trianglePoints[0], trianglePoints[1], trianglePoints[2], rayOrigin, rayDirection);
			}
			else
			{
				collisionStack.push(nodeToVisit1->children[0]);
				collisionStack.push(nodeToVisit1->children[1]);
			}
		}
		nodeToVisit1->NULL_NODE;
	}


	return true;
}






//FindPointNeighbor
//Mesh와 한 pointIndex가 주어졌을 때 NeighborPoint의 Index 반환
void CMeshUtil::FindPointNeighbor(vtkSmartPointer<vtkPolyData> polyData, vtkIdType tmp, vtkSmartPointer<vtkIdList> nei)
{
	unsigned short ncells;
	int i, j;
	const vtkIdType *pts;
	vtkIdType npts;
	vtkSmartPointer<vtkIdList> cellIdList = vtkSmartPointer<vtkIdList>::New();

	nei->Reset();
	polyData->GetPointCells(tmp, cellIdList);
	for (i = 0; i < cellIdList->GetNumberOfIds(); ++i)
	{

		polyData->GetCellPoints(cellIdList->GetId(i), npts, pts);
		for (j = 0; j < 3; ++j)
		{
			nei->InsertUniqueId(pts[j]);
		}
	}
}

//FindCellNeighbor
//Mesh와 한 pointIndex가 주어졌을 때 NeighborCell의 Index 반환
void CMeshUtil::FindCellNeighbor(vtkSmartPointer<vtkPolyData> polyData, vtkIdType tmp, vtkSmartPointer<vtkIdList> nei)
{
	nei->Reset();

	vtkSmartPointer<vtkIdList> cellIdList = vtkSmartPointer<vtkIdList>::New();
	polyData->GetPointCells(tmp, cellIdList);
	for (int i = 0; i < cellIdList->GetNumberOfIds(); ++i)
	{
		nei->InsertUniqueId(cellIdList->GetId(i));
	}
}