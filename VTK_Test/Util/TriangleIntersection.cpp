#include "TriangleIntersection.h"
bool CTriangleIntersection::TriRayIntersection(double p1[3], double q1[3], double r1[3], double rayOrigin[3], double rayDirection[3])
{
	double v0v1[3] = { q1[0] - p1[0],  q1[1] - p1[1], q1[2] - p1[2] };
	double v0v2[3] = { r1[0] - p1[0],  r1[1] - p1[1], r1[2] - p1[2] };
	double pvec[3];

	Cross(pvec, rayDirection, v0v2);
	double det = Dot(v0v1, pvec);

	double kEpsilon = std::numeric_limits<double>::epsilon();

	// ray and triangle are parallel if det is close to 0
	if (fabs(det) < kEpsilon)
		return false;
	double invDet = 1 / det;

	double tvec[3] = { rayOrigin[0] - p1[0],  rayOrigin[1] - p1[1], rayOrigin[2] - p1[2] };
	double u = Dot(tvec, pvec) * invDet;
	if (u < 0 || u > 1)
		return false;

	double qvec[3];
	Cross(qvec, tvec, v0v1);
	double v = Dot(rayDirection, qvec) * invDet;
	if (v < 0 || u + v > 1)
		return false;

	double t = Dot(v0v2, qvec) * invDet;

	return true;
}















void CTriangleIntersection::Cross(double* dest, double* v1, double* v2)
{
	dest[0] = v1[1] * v2[2] - v1[2] * v2[1];
	dest[1] = v1[2] * v2[0] - v1[0] * v2[2];
	dest[2] = v1[0] * v2[1] - v1[1] * v2[0];
}
double CTriangleIntersection::Dot(double * v1, double* v2)
{
	return v1[0] * v2[0] + v1[1] * v2[1] + v1[2] * v2[2];
}
void CTriangleIntersection::Sub(double* dest, double* v1, double* v2)
{
	dest[0] = v1[0] - v2[0];
	dest[1] = v1[1] - v2[1];
	dest[2] = v1[2] - v2[2];
}
void CTriangleIntersection::Scalar(double* dest, double alpha, double* v)
{
	dest[0] = alpha * v[0];
	dest[1] = alpha * v[1];
	dest[2] = alpha * v[2];
}


int CTriangleIntersection::CoplanarTriTri3D(
	double* p1, double* q1, double* r1,
	double* p2, double* q2, double* r2, double* N1)
{
	double P1[2], Q1[2], R1[2];
	double P2[2], Q2[2], R2[2];

	double n_x, n_y, n_z;

	n_x = ((N1[0]<0) ? -N1[0] : N1[0]);
	n_y = ((N1[1]<0) ? -N1[1] : N1[1]);
	n_z = ((N1[2]<0) ? -N1[2] : N1[2]);


	/* Projection of the triangles in 3D onto 2D such that the area of
	the projection is maximized. */


	if ((n_x > n_z) && (n_x >= n_y))
	{
		// Project onto plane YZ

		P1[0] = q1[2]; P1[1] = q1[1];
		Q1[0] = p1[2]; Q1[1] = p1[1];
		R1[0] = r1[2]; R1[1] = r1[1];

		P2[0] = q2[2]; P2[1] = q2[1];
		Q2[0] = p2[2]; Q2[1] = p2[1];
		R2[0] = r2[2]; R2[1] = r2[1];

	}
	else if ((n_y > n_z) && (n_y >= n_x))
	{
		// Project onto plane XZ

		P1[0] = q1[0]; P1[1] = q1[2];
		Q1[0] = p1[0]; Q1[1] = p1[2];
		R1[0] = r1[0]; R1[1] = r1[2];

		P2[0] = q2[0]; P2[1] = q2[2];
		Q2[0] = p2[0]; Q2[1] = p2[2];
		R2[0] = r2[0]; R2[1] = r2[2];

	}
	else
	{
		// Project onto plane XY

		P1[0] = p1[0]; P1[1] = p1[1];
		Q1[0] = q1[0]; Q1[1] = q1[1];
		R1[0] = r1[0]; R1[1] = r1[1];

		P2[0] = p2[0]; P2[1] = p2[1];
		Q2[0] = q2[0]; Q2[1] = q2[1];
		R2[0] = r2[0]; R2[1] = r2[1];
	}

	return TriTriOverlapTest2D(P1, Q1, R1, P2, Q2, R2);
}


int CTriangleIntersection::TriTriOverlapTest2D(
	double* p1, double* q1, double* r1,
	double* p2, double* q2, double* r2)
{
	if (Orient2D(p1, q1, r1) < 0.0f)
		if (Orient2D(p2, q2, r2) < 0.0f)
			return CCWTriTriIntersection2D(p1, r1, q1, p2, r2, q2);
		else
			return CCWTriTriIntersection2D(p1, r1, q1, p2, q2, r2);
	else
		if (Orient2D(p2, q2, r2) < 0.0f)
			return CCWTriTriIntersection2D(p1, q1, r1, p2, r2, q2);
		else
			return CCWTriTriIntersection2D(p1, q1, r1, p2, q2, r2);
}


int CTriangleIntersection::CCWTriTriIntersection2D(
	double* p1, double* q1, double* r1,
	double* p2, double* q2, double* r2)
{
	if (Orient2D(p2, q2, p1) >= 0.0f)
	{
		if (Orient2D(q2, r2, p1) >= 0.0f)
		{
			if (Orient2D(r2, p2, p1) >= 0.0f)
				return 1;
			else
				IntersectionTestEdge(p1, q1, r1, p2, q2, r2);
		}
		else
		{
			if (Orient2D(r2, p2, p1) >= 0.0f)
				IntersectionTestEdge(p1, q1, r1, r2, p2, q2);
			else
				IntersectionTestVertex(p1, q1, r1, p2, q2, r2);
		}
	}
	else
	{
		if (Orient2D(q2, r2, p1) >= 0.0f)
		{
			if (Orient2D(r2, p2, p1) >= 0.0f)
				IntersectionTestEdge(p1, q1, r1, q2, r2, p2);
			else
				IntersectionTestVertex(p1, q1, r1, q2, r2, p2);
		}
		else
			IntersectionTestVertex(p1, q1, r1, r2, p2, q2);
	}
}

int CTriangleIntersection::IntersectionTestVertex(
	double* p1, double* q1, double* r1,
	double* p2, double* q2, double* r2)
{
	if (Orient2D(r2, p2, q1) >= 0.0f)
	{
		if (Orient2D(r2, q2, q1) <= 0.0f)
		{
			if (Orient2D(p1, p2, q1) > 0.0f)
			{
				if (Orient2D(p1, q2, q1) <= 0.0f)
					return 1;
				else
					return 0;
			}
			else
			{
				if (Orient2D(p1, p2, r1) >= 0.0f)
				{
					if (Orient2D(q1, r1, p2) >= 0.0f)
						return 1;
					else
						return 0;
				}
				else
					return 0;
			}
		}
		else
		{
			if (Orient2D(p1, q2, q1) <= 0.0f)
			{
				if (Orient2D(r2, q2, r1) <= 0.0f)
				{
					if (Orient2D(q1, r1, q2) >= 0.0f)
						return 1;
					else
						return 0;
				}
				else
					return 0;
			}
			else
				return 0;
		}
	}
	else
	{
		if (Orient2D(r2, p2, r1) >= 0.0f)
		{
			if (Orient2D(q1, r1, r2) >= 0.0f)
			{
				if (Orient2D(p1, p2, r1) >= 0.0f)
					return 1;
				else
					return 0;
			}
			else
			{
				if (Orient2D(q1, r1, q2) >= 0.0f)
				{
					if (Orient2D(r2, r1, q2) >= 0.0f)
						return 1;
					else
						return 0;
				}
				else
					return 0;
			}
		}
		else
			return 0;
	}

}

int CTriangleIntersection::IntersectionTestEdge(
	double* p1, double* q1, double* r1,
	double* p2, double* q2, double* r2)
{
	if (Orient2D(r2, p2, q1) >= 0.0f)
	{
		if (Orient2D(p1, p2, q1) >= 0.0f)
		{
			if (Orient2D(p1, q1, r2) >= 0.0f)
				return 1;
			else
				return 0;
		}
		else
		{
			if (Orient2D(q1, r1, p2) >= 0.0f)
			{
				if (Orient2D(r1, p1, p2) >= 0.0f)
					return 1;
				else
					return 0;
			}
			else
				return 0;
		}
	}
	else
	{
		if (Orient2D(r2, p2, r1) >= 0.0f)
		{
			if (Orient2D(p1, p2, r1) >= 0.0f)
			{
				if (Orient2D(p1, r1, r2) >= 0.0f)
					return 1;
				else
				{
					if (Orient2D(q1, r1, r2) >= 0.0f)
						return 1;
					else
						return 0;
				}
			}
			else
				return 0;
		}
		else
			return 0;
	}
}

double CTriangleIntersection::Orient2D(double* a, double* b, double* c)
{
	return (a[0] - c[0])*(b[1] - c[1]) - (a[1] - c[1])*(b[0] - c[0]);
}

int CTriangleIntersection::ConstructIntersection(
	double* p1, double* q1, double* r1,
	double* p2, double* q2, double* r2,
	double* N1, double* N2, double* source, double* target)
{
	double v1[3], v2[3], v[3];
	double N[3];
	double alpha;

	Sub(v1, q1, p1);
	Sub(v2, r2, p1);
	Cross(N, v1, v2);
	Sub(v, p2, p1);
	if (Dot(v, N) > 0.0f)
	{
		Sub(v1, r1, p1);
		Cross(N, v1, v2);
		if (Dot(v, N) <= 0.0f)
		{
			Sub(v2, q2, p1);
			Cross(N, v1, v2);
			if (Dot(v, N) > 0.0f)
			{
				Sub(v1, p1, p2);
				Sub(v2, p1, r1);
				alpha = Dot(v1, N2) / Dot(v2, N2);
				Scalar(v1, alpha, v2);
				Sub(source, p1, v1);
				Sub(v1, p2, p1);
				Sub(v2, p2, r2);
				alpha = Dot(v1, N1) / Dot(v2, N1);
				Scalar(v1, alpha, v2);
				Sub(target, p2, v1);
				return 1;
			}
			else
			{
				Sub(v1, p2, p1);
				Sub(v2, p2, q2);
				alpha = Dot(v1, N1) / Dot(v2, N1);
				Scalar(v1, alpha, v2);
				Sub(source, p2, v1);
				Sub(v1, p2, p1);
				Sub(v2, p2, r2);
				alpha = Dot(v1, N1) / Dot(v2, N1);
				Scalar(v1, alpha, v2);
				Sub(target, p2, v1);
				return 1;
			}
		}
		else
		{
			return 0;
		}
	}
	else
	{
		Sub(v2, q2, p1);
		Cross(N, v1, v2);
		if (Dot(v, N) < 0.0f)
		{
			return 0;
		}
		else
		{
			Sub(v1, r1, p1);
			Cross(N, v1, v2);
			if (Dot(v, N) >= 0.0f)
			{

				Sub(v1, p1, p2);
				Sub(v2, p1, r1);
				alpha = Dot(v1, N2) / Dot(v2, N2);
				Scalar(v1, alpha, v2);
				Sub(source, p1, v1);
				Sub(v1, p1, p2);
				Sub(v2, p1, q1);
				alpha = Dot(v1, N2) / Dot(v2, N2);
				Scalar(v1, alpha, v2);
				Sub(target, p1, v1);
				return 1;
			}
			else
			{
				Sub(v1, p2, p1);
				Sub(v2, p2, q2);
				alpha = Dot(v1, N1) / Dot(v2, N1);
				Scalar(v1, alpha, v2);
				Sub(source, p2, v1);
				Sub(v1, p1, p2);
				Sub(v2, p1, q1);
				alpha = Dot(v1, N2) / Dot(v2, N2);
				Scalar(v1, alpha, v2);
				Sub(target, p1, v1);
				return 1;
			}
		}
	}

}


int CTriangleIntersection::TriTriInter3D(
	double* p1, double* q1, double* r1,
	double* p2, double* q2, double* r2, double* N1, int* coplanar,
	double dp2, double dq2, double dr2, double* source, double* target,

	double* N2)
{
	if (dp2 > 0.0f)
	{
		if (dq2 > 0.0f)
			ConstructIntersection(p1, r1, q1, r2, p2, q2, N1, N2, source, target);
		else if (dr2 > 0.0f)
			ConstructIntersection(p1, r1, q1, q2, r2, p2, N1, N2, source, target);
		else
			ConstructIntersection(p1, q1, r1, p2, q2, r2, N1, N2, source, target);
	}
	else if (dp2 < 0.0f)
	{
		if (dq2 < 0.0f)
			ConstructIntersection(p1, q1, r1, r2, p2, q2, N1, N2, source, target);
		else if (dr2 < 0.0f)
			ConstructIntersection(p1, q1, r1, q2, r2, p2, N1, N2, source, target);
		else
			ConstructIntersection(p1, r1, q1, p2, q2, r2, N1, N2, source, target);
	}
	else
	{
		if (dq2 < 0.0f)
		{
			if (dr2 >= 0.0f)
				ConstructIntersection(p1, r1, q1, q2, r2, p2, N1, N2, source, target);
			else
				ConstructIntersection(p1, q1, r1, p2, q2, r2, N1, N2, source, target);
		}
		else if (dq2 > 0.0f)
		{
			if (dr2 > 0.0f)
				ConstructIntersection(p1, r1, q1, p2, q2, r2, N1, N2, source, target);
			else
				ConstructIntersection(p1, q1, r1, q2, r2, p2, N1, N2, source, target);
		}
		else
		{
			if (dr2 > 0.0f)
				ConstructIntersection(p1, q1, r1, r2, p2, q2, N1, N2, source, target);
			else if (dr2 < 0.0f)
				ConstructIntersection(p1, r1, q1, r2, p2, q2, N1, N2, source, target);
			else
			{
				*coplanar = 1;
				return CoplanarTriTri3D(p1, q1, r1, p2, q2, r2, N1);
			}
		}
	}
}



int CTriangleIntersection::TriTriIntersectionTest3D(double p1[3], double q1[3], double r1[3],
	double p2[3], double q2[3], double r2[3],
	int * coplanar,
	double source[3], double target[3])

{
	double dp1, dq1, dr1, dp2, dq2, dr2;
	double v1[3], v2[3], v[3];
	double N1[3], N2[3], N[3];
	double alpha = 0;

	// Compute distance signs  of p1, q1 and r1 
	// to the plane of triangle(p2,q2,r2)


	Sub(v1, p2, r2);
	Sub(v2, q2, r2);
	Cross(N2, v1, v2);

	Sub(v1, p1, r2);
	dp1 = Dot(v1, N2);
	Sub(v1, q1, r2);
	dq1 = Dot(v1, N2);
	Sub(v1, r1, r2);
	dr1 = Dot(v1, N2);

	if (((dp1 * dq1) > 0.0f) && ((dp1 * dr1) > 0.0f))  return 0;

	// Compute distance signs  of p2, q2 and r2 
	// to the plane of triangle(p1,q1,r1)

	Sub(v1, q1, p1);
	Sub(v2, r1, p1);
	Cross(N1, v1, v2);

	Sub(v1, p2, r1);
	dp2 = Dot(v1, N1);
	Sub(v1, q2, r1);
	dq2 = Dot(v1, N1);
	Sub(v1, r2, r1);
	dr2 = Dot(v1, N1);

	if (((dp2 * dq2) > 0.0f) && ((dp2 * dr2) > 0.0f)) return 0;

	if (dp1 > 0.0f) {
		if (dq1 > 0.0f) TriTriInter3D(r1, p1, q1, p2, r2, q2, N1, coplanar,
			dp2, dr2, dq2, source, target, N2);
		else if (dr1 > 0.0f) TriTriInter3D(q1, r1, p1, p2, r2, q2, N1, coplanar,
			dp2, dr2, dq2, source, target, N2);

		else TriTriInter3D(p1, q1, r1, p2, q2, r2, N1, coplanar,
			dp2, dq2, dr2, source, target, N2);
	}
	else if (dp1 < 0.0f) {
		if (dq1 < 0.0f) TriTriInter3D(r1, p1, q1, p2, q2, r2, N1, coplanar,
			dp2, dq2, dr2, source, target, N2);
		else if (dr1 < 0.0f) TriTriInter3D(q1, r1, p1, p2, q2, r2, N1, coplanar,
			dp2, dq2, dr2, source, target, N2);
		else TriTriInter3D(p1, q1, r1, p2, r2, q2, N1, coplanar,
			dp2, dr2, dq2, source, target, N2);
	}
	else {
		if (dq1 < 0.0f) {
			if (dr1 >= 0.0f) TriTriInter3D(q1, r1, p1, p2, r2, q2, N1, coplanar,
				dp2, dr2, dq2, source, target, N2);
			else TriTriInter3D(p1, q1, r1, p2, q2, r2, N1, coplanar,
				dp2, dq2, dr2, source, target, N2);
		}
		else if (dq1 > 0.0f) {
			if (dr1 > 0.0f) TriTriInter3D(p1, q1, r1, p2, r2, q2, N1, coplanar,
				dp2, dr2, dq2, source, target, N2);
			else TriTriInter3D(q1, r1, p1, p2, q2, r2, N1, coplanar,
				dp2, dq2, dr2, source, target, N2);
		}
		else {
			if (dr1 > 0.0f) TriTriInter3D(r1, p1, q1, p2, q2, r2, N1, coplanar,
				dp2, dq2, dr2, source, target, N2);
			else if (dr1 < 0.0f) TriTriInter3D(r1, p1, q1, p2, r2, q2, N1, coplanar,
				dp2, dr2, dq2, source, target, N2);
			else {
				// triangles are co-planar

				*coplanar = 1;
				return CoplanarTriTri3D(p1, q1, r1, p2, q2, r2, N1);
			}
		}
	}
};