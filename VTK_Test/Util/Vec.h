#pragma once
#include <iostream>
//Data Structure
class CVec
{
private:

public:
	double x, y, z;

	CVec(double _x, double _y, double _z)
	{
		x = _x;
		y = _y;
		z = _z;
	}
	CVec(double _point[])
	{
		x = _point[0];
		y = _point[1];
		z = _point[2];
	}
	/*CVec(const CVec& p)
	{
		x = p.x;
		y = p.x;
		z = p.x;
	}*/
	CVec()
	{
		x = 0;
		y = 0;
		z = 0;
	}

	CVec operator + (CVec p) {
		x = x + p.x;
		y = y + p.y;
		z = z + p.z;
		return CVec(x, y, z);
	}

	CVec operator - (CVec p) {
		x = x - p.x;
		y = y - p.y;
		z = z - p.z;
		return CVec(x, y, z);
	}
	void operator = (CVec p) {
		x = p.x;
		y = p.y;
		z = p.z;
		return;
	}

	CVec operator + (double _x) {
		x = x + _x;
		y = y + _x;
		z = z + _x;
		return CVec(x, y, z);
	}
	CVec operator - (double _x) {
		x = x - _x;
		y = y - _x;
		z = z - _x;
		return CVec(x, y, z);
	}
	CVec operator * (double _x) {
		x = x * _x;
		y = y * _x;
		z = z * _x;
		return CVec(x, y, z);
	}
	CVec operator / (double _x) {
		x = x / _x;
		y = y / _x;
		z = z / _x;
		return CVec(x, y, z);
	}

	const double Dot(const CVec p) {
		return x * p.x + y * p.y + z * p.z;
	}
	const CVec Cross(const CVec p) {
		return CVec(y * p.z - z * p.y, z * p.x - x * p.z, x * p.y - y * p.x);
	}

	void Print()
	{
		std::cout << "    " << x << " // " << y << " // " << z << std::endl;
	}


};













class Ray
{
private:

public:
	Ray::Ray();
	Ray::Ray(CVec origin, CVec des, float size);
	Ray::~Ray();

	CVec origin;
	CVec direction;
	CVec destination;
	float raySize;
	void ComputeDestination();

};



class Util
{
private:
	bool IsSame(CVec p, CVec a, CVec b, CVec c);
	float Norm2(CVec v);

public:
	CVec			pos;
	Util();
	~Util();

	bool IsLine(CVec p, CVec a, CVec b);
	bool ComputePointInTriangle(CVec p, CVec a, CVec b, CVec c);
	bool ComputePointOnTriangle(CVec p, CVec a, CVec b, CVec c);

	bool LineToLineIntersection(CVec a, CVec b, CVec c, CVec d, CVec &iP);

	float GetVectorSize(CVec a);
	CVec CrossProduct(CVec a, CVec b);
	float DotProduct(CVec a, CVec b);

	bool operator ==(CVec &v)
	{
		if (pos.x == v.x && pos.y == v.y && pos.z == v.z)
			return true;
		return false;
	}
};











