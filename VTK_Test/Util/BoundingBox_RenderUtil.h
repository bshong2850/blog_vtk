#pragma once
#include <iostream>
#include <memory>
#include <string>

#include "vtkSmartPointer.h"
#include "BoundingBox.h"

class vtkPolyData;
class vtkActor;
class vtkMatrix4x4;

//CBBox_AABB
//Bounding Box Rendering ���� Class
class CBBox_RenderUtil
{
public:

	CBBox_RenderUtil() {};
	~CBBox_RenderUtil() {};
	
	static vtkSmartPointer<vtkActor> GetAABBLineActor(vtkSmartPointer<vtkPolyData> polyData, vtkSmartPointer<vtkMatrix4x4> matrix, std::string color);
	static vtkSmartPointer<vtkActor> GetAABBLineActor(CBBox_AABB aabb, std::string color);
	static vtkSmartPointer<vtkActor> GetAABBLineActor(double min[3], double max[3], std::string color);

	static void GetOBBData(vtkSmartPointer<vtkPolyData> polyData, vtkSmartPointer<vtkMatrix4x4> matrix, double axis[][3], double extent[], double center[]);
	static vtkSmartPointer<vtkActor> GetOBBLineActor(vtkSmartPointer<vtkPolyData> polyData, vtkSmartPointer<vtkMatrix4x4> matrix, std::string color);
	static vtkSmartPointer<vtkActor> GetOBBLineActor(double axis[3][3], double extent[3], double center[3], std::string color);

	static vtkSmartPointer<vtkActor> GetBoxActor(double point[8][3], std::string color);

	static vtkSmartPointer<vtkActor> GetSphereLineActor(vtkSmartPointer<vtkPolyData> polyData, vtkSmartPointer<vtkMatrix4x4> matrix, double radius, std::string color);
	static double ComputeRadius(vtkSmartPointer<vtkPolyData> polyData);
};