#pragma once

#include <vector>
#include <queue>

#include "BoundingBox.h"

#define assert(expression) ((void)0)

// Class TreeNode
// BVH의 Node들에 대한 Class
class TreeNode {
public:
	const static int NULL_NODE = -1;
	
	int parentID;
	int nextNodeID;
	int children[2];
	int data;
	int height;
	CBBox_AABB aabb;

	bool isLeaf() {
		return (height == 0);
	}
};

// Class CBVH
// Bounding Volume Hierarchy에 대한 Class
class CBVH {

private:

	TreeNode* mNodes;
	int mRootNodeID;
	int mFreeNodeID;
	int mNbAllocatedNodes;
	int mNbNodes;
	

	int AllocateNode();
	void ReleaseNode(int nodeID);
	void InsertLeafNode(int nodeID);
	void RemoveLeafNode(int nodeID);
	int BalanceSubTreeAtNode(int nodeID);
	int AddObjectInternal(CBBox_AABB aabb);
	void Initialize();
	
public:

	CBVH();
	~CBVH();

	void AddObject(CBBox_AABB aabb, int data);
	void RemoveObject(int nodeID);
	bool UpdateObject(int nodeID, CBBox_AABB newAABB, bool forceReinsert = false);
	int GetNodeData(int nodeID);
	CBBox_AABB GetRootAABB();
	std::vector<CBBox_AABB> GetTargetDepthAABBVec(int findDepth);
	void Reset();

	TreeNode* GetmNodes()
	{
		return mNodes;
	};

	int GetRootNodeID()
	{
		return mRootNodeID;
	}
	int GetMaxDepth()
	{
		return (GetmNodes() + GetRootNodeID())->height;
	}
	
};



