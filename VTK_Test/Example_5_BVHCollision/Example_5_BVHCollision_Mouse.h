#pragma once
#include "vtkSmartPointer.h"
#include "vtkInteractorStyleTrackballActor.h"
#include <vector>
#include <memory>

class vtkPolyData;
class vtkMatrix4x4;
class vtkRenderer;
class vtkActor;
class CBVH;

//Mouse Interactor 클래스
//Mouse와 관련되 함수들 제어
class CExample_5_MouseInteractorStyle : public vtkInteractorStyleTrackballActor
{
public:
	static CExample_5_MouseInteractorStyle* New()
	{
		return new CExample_5_MouseInteractorStyle;
	}

	CExample_5_MouseInteractorStyle();
	//virtual void OnMiddleButtonDown();
	virtual void OnMouseMove();

	void Initialize();
	void SetRenderer(vtkSmartPointer<vtkRenderer> vR);
	void SetPolyData(vtkSmartPointer<vtkPolyData> vPD, vtkSmartPointer<vtkMatrix4x4> matrix);
	
private:

	vtkSmartPointer<vtkRenderer> renderer = vtkSmartPointer<vtkRenderer>::New();

	std::vector<vtkSmartPointer<vtkPolyData>> polyDataVec;
	std::vector<vtkSmartPointer<vtkMatrix4x4>> matrixVec;
	std::vector<std::shared_ptr<CBVH>> bvhTree;
	
};