#pragma once

#include <vtkCommand.h>
#include <vtkSmartPointer.h>

#include <iostream>
#include <vector>
#include <memory>

class vtkPolyData;
class vtkMatrix4x4;
class vtkRenderer;
class vtkActor;

class CBVH;
class CExample_5_MouseInteractorStyle;

//다른 Example 만들 때 Sample로 불러오기 위한 Code
class  CExample_5_KeyPressCallback : public vtkCommand
{
public:
	static CExample_5_KeyPressCallback *New()
	{
		return new CExample_5_KeyPressCallback;
	}
	CExample_5_KeyPressCallback();
	virtual void Execute(vtkObject *caller, unsigned long, void*);
	void SetRenderer(vtkSmartPointer<vtkRenderer> vR);
	void SetDataVec(vtkSmartPointer<vtkPolyData> vpdVec, vtkSmartPointer<vtkMatrix4x4> matrixVec);

private:
	vtkSmartPointer<vtkRenderer> renderer;
	std::vector<vtkSmartPointer<vtkPolyData>> polyDataVec;
	std::vector<vtkSmartPointer<vtkMatrix4x4>> matrixVec;

	int depth;
	std::vector<std::shared_ptr<CBVH>> bvhTree;

	bool isBuild;

	
	int modelNum;
};