#include "Example_5_BVHCollision.h"
#include "Example_5_BVHCollision_Callback.h"
#include "../Util/MeshUtil.h"

#include <vtkActor.h>
#include <vtkCamera.h>
#include <vtkMatrix4x4.h>
#include <vtkNamedColors.h>
#include <vtkProperty.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkRenderer.h>
#include <vtkTransform.h>
#include <vtkSphereSource.h>
#include <vtkPolyDataMapper.h>

#include <vtkTransformFilter.h>
//Run
//Sphere 생성 및 Callback 붙여놓은 상태
void CExample_5_BVHCollision::Run()
{
	vtkSmartPointer<vtkPolyData> mesh1 = CMeshUtil::ReadObj("../data/CatSmall.obj");
	vtkSmartPointer<vtkMatrix4x4> matrix1 = vtkSmartPointer<vtkMatrix4x4>::New();

	auto actor1 = CMeshUtil::PolyDataToActor(mesh1, "Gold");
	actor1->SetUserMatrix(matrix1);

	vtkSmartPointer<vtkPolyData> mesh2 = CMeshUtil::ReadObj("../data/CatSmall.obj");
	vtkSmartPointer<vtkMatrix4x4> matrix2 = vtkSmartPointer<vtkMatrix4x4>::New();
	auto actor2 = CMeshUtil::PolyDataToActor(mesh2, "Tomato");
	actor2->SetUserMatrix(matrix2);

	vtkSmartPointer<vtkNamedColors> colors = vtkSmartPointer<vtkNamedColors>::New();

	vtkSmartPointer<vtkRenderer> renderer = vtkSmartPointer<vtkRenderer>::New();
	renderer->UseHiddenLineRemovalOn();
	renderer->AddActor(actor1);
	renderer->AddActor(actor2);

	renderer->SetBackground(colors->GetColor3d("Gray").GetData());
	renderer->UseHiddenLineRemovalOn();

	vtkSmartPointer<vtkRenderWindow> renderWindow = vtkSmartPointer<vtkRenderWindow>::New();
	renderWindow->SetSize(640, 480);
	renderWindow->AddRenderer(renderer);

	vtkSmartPointer<vtkRenderWindowInteractor> interactor = vtkSmartPointer<vtkRenderWindowInteractor>::New();
	interactor->SetRenderWindow(renderWindow);


	CExample_5_KeyPressCallback *keyFunction = CExample_5_KeyPressCallback::New();
	keyFunction->SetDataVec(mesh1, matrix1);
	keyFunction->SetDataVec(mesh2, matrix2);

	keyFunction->SetRenderer(renderer);

	interactor->AddObserver(vtkCommand::KeyPressEvent, keyFunction);

	renderWindow->SetWindowName("Example");
	renderWindow->Render();

	renderer->ResetCamera();
	renderWindow->Render();
	interactor->Start();
	return;
}
