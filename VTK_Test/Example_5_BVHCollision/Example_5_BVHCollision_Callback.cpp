#include "Example_5_BVHCollision_Callback.h"
#include "Example_5_BVHCollision_Mouse.h"
#include "../Util/BoundingVolumeHierarchy.h"
#include "../Util/BoundingBox_RenderUtil.h"
#include "../Util/MeshUtil.h"
#include "../Util/TriangleIntersection.h"

#include <vtkRenderWindowInteractor.h>
#include <vtkInteractorStyleTrackballCamera.h>
#include <vtkTransform.h>
#include <vtkTransformPolyDataFilter.h>
#include <vtkProperty.h>
#include <vtkRenderer.h>
#include <vtkRenderWindow.h>

//생성자
CExample_5_KeyPressCallback::CExample_5_KeyPressCallback() : modelNum(0), isBuild(false), depth(0)
{
}

//SetDataVec
//PolyData 및 Matrix를 set 해주는 함수
void CExample_5_KeyPressCallback::SetDataVec(vtkSmartPointer<vtkPolyData> polyData, vtkSmartPointer<vtkMatrix4x4> matrix)
{
	this->polyDataVec.push_back(polyData);
	this->matrixVec.push_back(matrix);
}

//SetRenderer
//Renderer를 set 해주는 함수
void CExample_5_KeyPressCallback::SetRenderer(vtkSmartPointer<vtkRenderer> vR)
{
	this->renderer = vR;
}

//Execute
//Keypress Event가 있을 때 불리는 함수
void CExample_5_KeyPressCallback::Execute(vtkObject *caller, unsigned long, void*)
{
	vtkRenderWindowInteractor *rwi = reinterpret_cast<vtkRenderWindowInteractor *>(caller);
	std::string key = rwi->GetKeySym();
	vtkSmartPointer<vtkInteractorStyle> style;
	vtkSmartPointer<CExample_5_MouseInteractorStyle> style1 = vtkSmartPointer<CExample_5_MouseInteractorStyle>::New();
	vtkSmartPointer<vtkActor> triangleActor1 = vtkSmartPointer<vtkActor>::New();
	vtkSmartPointer<vtkActor> triangleActor2 = vtkSmartPointer<vtkActor>::New();
	isBuild = false;
	if (key == "a")
	{
		std::cout << "Actor Mode     : Trackball Actor Mode" << std::endl;
		style = vtkSmartPointer<vtkInteractorStyleTrackballActor>::New();
		rwi->SetInteractorStyle(style);
	}
	if (key == "t")
	{
		std::cout << "Camera Mode    : Trackball Camera Mode" << std::endl;
		style = vtkSmartPointer<vtkInteractorStyleTrackballCamera>::New();
		rwi->SetInteractorStyle(style);		
	}
	
	bool isCollision = false;
	std::pair<int, int> collisionPair;
	if (key == "1" || key == "2")
	{
		clock_t collisionCheckTime;
		collisionCheckTime = clock();
		if (key == "1")
		{
			std::cout << "Collision Check With Bounding Volume Hierarchy" << std::endl;
			bvhTree.clear();
			bvhTree.push_back(CMeshUtil::GetBVH(polyDataVec[0], matrixVec[0]));
			bvhTree.push_back(CMeshUtil::GetBVH(polyDataVec[1], matrixVec[1]));
			isBuild = true;

			if (CMeshUtil::isCollision(CMeshUtil::SetTransform(polyDataVec[0], matrixVec[0]), bvhTree[0]
				, CMeshUtil::SetTransform(polyDataVec[1], matrixVec[1]), bvhTree[1], collisionPair) == 1)
			{
				isCollision = true;
			}
			std::cout << "Collision Check With BVH Time = " << clock() - collisionCheckTime << " ms" << std::endl;
		}
		if (key == "2")
		{
			std::cout << "Collision Check Whole Search" << std::endl;
			auto polyData1 = CMeshUtil::SetTransform(polyDataVec[0], matrixVec[0]);
			auto polyData2 = CMeshUtil::SetTransform(polyDataVec[1], matrixVec[1]);
			for (int i = 0; i < polyData1->GetNumberOfPoints(); ++i)
			{
				for (int j = 0; j < polyData2->GetNumberOfPoints(); ++j)
				{
					const vtkIdType* pts1;
					vtkIdType npts1;
					polyData1->GetCellPoints(i, npts1, pts1);
					double triangle1Points[3][3];
					for (int pointIndex = 0; pointIndex < 3; ++pointIndex)
					{
						polyData1->GetPoint(pts1[pointIndex], triangle1Points[pointIndex]);
					}

					const vtkIdType* pts2;
					vtkIdType npts2;
					polyData2->GetCellPoints(j, npts2, pts2);
					double triangle2Points[3][3];
					for (int pointIndex = 0; pointIndex < 3; ++pointIndex)
					{
						polyData2->GetPoint(pts2[pointIndex], triangle2Points[pointIndex]);
					}

					int tmp = 1;
					int * coplanar = &tmp;
					double source[3];
					double dest[3];

					CTriangleIntersection tritriIntersection;
					if (tritriIntersection.TriTriIntersectionTest3D(
						triangle1Points[0], triangle1Points[1], triangle1Points[2],
						triangle2Points[0], triangle2Points[1], triangle2Points[2], coplanar, source, dest) == 1)
					{
						isCollision = true;

						collisionPair.first = i;
						collisionPair.second = j;
					}

					//가속화 하고 싶다면 주석 풀기
					/*if (isCollision)
						break;*/
				}
				//가속화 하고 싶다면 주석 풀기
				/*if (isCollision)
					break;*/
			}
			std::cout << "Collision Check Whole Search Time = " << clock() - collisionCheckTime << " ms" << std::endl;
		}

		if (isCollision)
		{
			std::cout << "Collision!    Info" << std::endl;
			std::cout << " PolyData1 Triangle Index = " << collisionPair.first << std::endl;
			std::cout << " PolyData2 Triangle Index = " << collisionPair.second << std::endl;
		}
		else
		{
			std::cout << "No Collision!" << std::endl;
		}
	}

	if (key == "m")
	{
		std::cout << "Mouse Mode     : BVH Collision Check Mode" << std::endl;
		style1->Initialize();
		style1->SetPolyData(polyDataVec[0], matrixVec[0]);
		style1->SetPolyData(polyDataVec[1], matrixVec[1]);
		style1->SetRenderer(renderer);
		style1->SetDefaultRenderer(renderer);
		rwi->SetInteractorStyle(style1);
	}
	
	renderer->RemoveAllViewProps();
	renderer->UseHiddenLineRemovalOn();

	auto actor1 = CMeshUtil::PolyDataToActor(polyDataVec[0], "Gold");
	actor1->SetUserMatrix(matrixVec[0]);

	auto actor2 = CMeshUtil::PolyDataToActor(polyDataVec[1], "Tomato");
	actor2->SetUserMatrix(matrixVec[1]);
	renderer->AddActor(actor1);
	renderer->AddActor(actor2);
	if (isCollision)
	{
		triangleActor1 = CMeshUtil::PolyDataIndexTriangleToActor(CMeshUtil::SetTransform(polyDataVec[0], matrixVec[0]), collisionPair.first, "Red");
		triangleActor2 = CMeshUtil::PolyDataIndexTriangleToActor(CMeshUtil::SetTransform(polyDataVec[1], matrixVec[1]), collisionPair.second, "Gold");

		renderer->AddActor(triangleActor1);
		renderer->AddActor(triangleActor2);
	}
	renderer->GetRenderWindow()->Render();
}