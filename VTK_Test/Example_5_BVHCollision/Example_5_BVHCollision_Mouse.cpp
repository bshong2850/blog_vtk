
#include "Example_5_BVHCollision_Mouse.h"
#include "../Util/BoundingBox_RenderUtil.h"
#include "../Util/MeshUtil.h"
#include "../Util/BoundingVolumeHierarchy.h"

#include <vtkRenderWindowInteractor.h>
#include <vtkRenderWindow.h>
#include <vtkRenderer.h>
#include <vtkPolyData.h>
#include <vtkProperty.h>
#include <vtkMatrix4x4.h>

//#include <set>
//
//#define TimeLog 0

//생성자
//Bounding Box Mode를 AABB로 Initialize
CExample_5_MouseInteractorStyle::CExample_5_MouseInteractorStyle()
{

}

//SetPolyData
//polyDataPairVec에 PolyData Matrix Pair를 Set하는 함수
void CExample_5_MouseInteractorStyle::SetPolyData(vtkSmartPointer<vtkPolyData> vPD, vtkSmartPointer<vtkMatrix4x4> matrix)
{
	this->polyDataVec.push_back(vPD);
	this->matrixVec.push_back(matrix);
}
//Initialize
//polyDataPairVec를 초기화 해주는 함수
void CExample_5_MouseInteractorStyle::Initialize()
{
	polyDataVec.clear();
	std::vector<vtkSmartPointer<vtkPolyData>>().swap(polyDataVec);
	matrixVec.clear();
	std::vector<vtkSmartPointer<vtkMatrix4x4>>().swap(matrixVec);
}

//SetRenderer
//Renderer Setting
void CExample_5_MouseInteractorStyle::SetRenderer(vtkSmartPointer<vtkRenderer> vR)
{
	this->renderer = vR;
}

//OnMouseMove
//Mouse가 움직이고 있을 때 호출되는 함수
void CExample_5_MouseInteractorStyle::OnMouseMove()
{

	clock_t time;
	time = clock();

	bvhTree.clear();
	std::vector<std::shared_ptr<CBVH>>().swap(bvhTree);
	bvhTree.push_back(CMeshUtil::GetBVH(polyDataVec[0], matrixVec[0]));
	bvhTree.push_back(CMeshUtil::GetBVH(polyDataVec[1], matrixVec[1]));

	vtkSmartPointer<vtkActor> aabbActor1 = vtkSmartPointer<vtkActor>::New();
	vtkSmartPointer<vtkActor> aabbActor2 = vtkSmartPointer<vtkActor>::New();

	std::pair<int, int> collisionPair;
	std::string bboxColor = "Lime";
	if (CMeshUtil::isCollision(CMeshUtil::SetTransform(polyDataVec[0], matrixVec[0]), bvhTree[0]
		, CMeshUtil::SetTransform(polyDataVec[1], matrixVec[1]), bvhTree[1], collisionPair) == 1)
	{
		bboxColor = "Red";
	}

	
	aabbActor1 = CBBox_RenderUtil::GetAABBLineActor(bvhTree[0]->GetRootAABB(), bboxColor);
	aabbActor2 = CBBox_RenderUtil::GetAABBLineActor(bvhTree[1]->GetRootAABB(), bboxColor);

	renderer->RemoveAllViewProps();
	renderer->UseHiddenLineRemovalOn();

	auto actor1 = CMeshUtil::PolyDataToActor(polyDataVec[0], "Gold");
	actor1->SetUserMatrix(matrixVec[0]);

	auto actor2 = CMeshUtil::PolyDataToActor(polyDataVec[1], "Tomato");
	actor2->SetUserMatrix(matrixVec[1]);
	renderer->AddActor(actor1);
	renderer->AddActor(actor2);
	renderer->AddActor(aabbActor1);
	renderer->AddActor(aabbActor2);

	renderer->GetRenderWindow()->Render();

	//std::cout << "Update Time = " << clock() - time << std::endl;

	vtkInteractorStyleTrackballActor::OnMouseMove();
}


