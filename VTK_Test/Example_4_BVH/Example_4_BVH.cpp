#include "Example_4_BVH.h"
#include "Example_4_BVH_Callback.h"
#include "../Util/MeshUtil.h"

#include <vtkActor.h>
#include <vtkCamera.h>
#include <vtkMatrix4x4.h>
#include <vtkNamedColors.h>
#include <vtkProperty.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkRenderer.h>
#include <vtkTransform.h>
#include <vtkSphereSource.h>
#include <vtkPolyDataMapper.h>

#include <vtkTransformFilter.h>

//Run
//Sphere 생성 및 Callback 붙여놓은 상태
void CExample_4_BVH::Run()
{
	/*vtkSmartPointer<vtkObject> object = vtkSmartPointer<vtkObject>::New();
	object->GlobalWarningDisplayOff();

	vtkSmartPointer<vtkSphereSource> sphere = vtkSmartPointer<vtkSphereSource>::New();
	sphere->SetRadius(0.29);
	sphere->SetPhiResolution(11);
	sphere->SetThetaResolution(11);
	sphere->SetCenter(0.0, 0, 0);
	sphere->Update();

	vtkSmartPointer<vtkNamedColors> colors = vtkSmartPointer<vtkNamedColors>::New();
	vtkSmartPointer<vtkPolyDataMapper> mapper = vtkSmartPointer<vtkPolyDataMapper>::New();
	mapper->SetInputData(sphere->GetOutput());
	vtkSmartPointer<vtkMatrix4x4> matrix = vtkSmartPointer<vtkMatrix4x4>::New();
	vtkSmartPointer<vtkActor> actor = vtkSmartPointer<vtkActor>::New();
	actor->SetMapper(mapper);
	actor->GetProperty()->BackfaceCullingOn();
	actor->SetUserMatrix(matrix);
	actor->GetProperty()->SetDiffuseColor(colors->GetColor3d("Tomato").GetData());
	actor->GetProperty()->SetRepresentationToWireframe();*/

	////vtkSmartPointer<vtkPolyData> mesh = CMeshUtil::ReadObj("../data/cube.obj");
	vtkSmartPointer<vtkPolyData> mesh = CMeshUtil::ReadObj("../data/CatSmall.obj");
	vtkSmartPointer<vtkMatrix4x4> matrix = vtkSmartPointer<vtkMatrix4x4>::New();

	auto actor = CMeshUtil::PolyDataToActor(mesh, "Gold");
	actor->SetUserMatrix(matrix);

	vtkSmartPointer<vtkNamedColors> colors = vtkSmartPointer<vtkNamedColors>::New();

	vtkSmartPointer<vtkRenderer> renderer = vtkSmartPointer<vtkRenderer>::New();
	renderer->UseHiddenLineRemovalOn();
	renderer->AddActor(actor);

	renderer->SetBackground(colors->GetColor3d("Gray").GetData());
	renderer->UseHiddenLineRemovalOn();

	vtkSmartPointer<vtkRenderWindow> renderWindow = vtkSmartPointer<vtkRenderWindow>::New();
	renderWindow->SetSize(640, 480);
	renderWindow->AddRenderer(renderer);

	vtkSmartPointer<vtkRenderWindowInteractor> interactor = vtkSmartPointer<vtkRenderWindowInteractor>::New();
	interactor->SetRenderWindow(renderWindow);


	CExample_4_KeyPressCallback *keyFunction = CExample_4_KeyPressCallback::New();
	keyFunction->SetDataVec(mesh, matrix);

	keyFunction->SetRenderer(renderer);

	interactor->AddObserver(vtkCommand::KeyPressEvent, keyFunction);

	renderWindow->SetWindowName("Example");
	renderWindow->Render();

	renderer->ResetCamera();
	renderWindow->Render();
	interactor->Start();
	return;
}
