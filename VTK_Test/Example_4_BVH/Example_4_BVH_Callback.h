#pragma once

#include <vtkCommand.h>
#include <vtkSmartPointer.h>

#include <iostream>
#include <vector>
#include <memory>

class vtkPolyData;
class vtkMatrix4x4;
class vtkRenderer;
class vtkActor;

class CBVH;

//다른 Example 만들 때 Sample로 불러오기 위한 Code
class  CExample_4_KeyPressCallback : public vtkCommand
{
public:
	static CExample_4_KeyPressCallback *New()
	{
		return new CExample_4_KeyPressCallback;
	}
	CExample_4_KeyPressCallback();
	virtual void Execute(vtkObject *caller, unsigned long, void*);
	void SetRenderer(vtkSmartPointer<vtkRenderer> vR);
	void SetDataVec(vtkSmartPointer<vtkPolyData> vpdVec, vtkSmartPointer<vtkMatrix4x4> matrixVec);

private:
	vtkSmartPointer<vtkRenderer> renderer;
	vtkSmartPointer<vtkPolyData> polyData;
	vtkSmartPointer<vtkMatrix4x4> matrix;

	int depth;
	std::shared_ptr<CBVH> bvhTree;

	bool isBuild;
};