#include "Example_4_BVH_Callback.h"
#include "../Util/BoundingVolumeHierarchy.h"
#include "../Util/BoundingBox_RenderUtil.h"
#include "../Util/MeshUtil.h"

#include <vtkRenderWindowInteractor.h>
#include <vtkInteractorStyleTrackballActor.h>
#include <vtkInteractorStyleTrackballCamera.h>
#include <vtkTransform.h>
#include <vtkTransformPolyDataFilter.h>
#include <vtkProperty.h>
#include <vtkRenderer.h>
#include <vtkRenderWindow.h>

//생성자
CExample_4_KeyPressCallback::CExample_4_KeyPressCallback() : isBuild(false), depth(0)
{
}

//SetDataVec
//PolyData 및 Matrix를 set 해주는 함수
void CExample_4_KeyPressCallback::SetDataVec(vtkSmartPointer<vtkPolyData> polyData, vtkSmartPointer<vtkMatrix4x4> matrix)
{
	this->polyData = polyData;
	this->matrix = matrix;
}

//SetRenderer
//Renderer를 set 해주는 함수
void CExample_4_KeyPressCallback::SetRenderer(vtkSmartPointer<vtkRenderer> vR)
{
	this->renderer = vR;
}

//Execute
//Keypress Event가 있을 때 불리는 함수
void CExample_4_KeyPressCallback::Execute(vtkObject *caller, unsigned long, void*)
{
	vtkRenderWindowInteractor *rwi = reinterpret_cast<vtkRenderWindowInteractor *>(caller);
	std::string key = rwi->GetKeySym();
	vtkSmartPointer<vtkInteractorStyle> style;
	
	if (key == "a")
	{
		std::cout << "Actor Mode     : Trackball Actor Mode" << std::endl;
		style = vtkSmartPointer<vtkInteractorStyleTrackballActor>::New();
		rwi->SetInteractorStyle(style);
	}
	if (key == "t")
	{
		std::cout << "Camera Mode    : Trackball Camera Mode" << std::endl;
		style = vtkSmartPointer<vtkInteractorStyleTrackballCamera>::New();
		rwi->SetInteractorStyle(style);
	}

	if (key == "b")
	{
		vtkSmartPointer<vtkTransform> transform = vtkSmartPointer<vtkTransform>::New();
		transform->SetMatrix(matrix);

		vtkSmartPointer<vtkTransformPolyDataFilter> transformFilter = vtkSmartPointer<vtkTransformPolyDataFilter>::New();
		transformFilter->SetInputData(polyData);
		transformFilter->SetTransform(transform);
		transformFilter->Update();

		vtkSmartPointer<vtkPolyData> transformPolyData = vtkSmartPointer<vtkPolyData>::New();
		transformPolyData->DeepCopy(transformFilter->GetOutput());


		std::shared_ptr<CBVH> tree = std::make_shared<CBVH>();
		for (int i = 0; i < transformFilter->GetOutput()->GetNumberOfCells(); ++i)
		{
			const vtkIdType* pts;
			vtkIdType npts;
			transformPolyData->GetCellPoints(i, npts, pts);

			double trianglePoints[3][3];
			for (int pointIndex = 0; pointIndex < 3; ++pointIndex)
			{
				transformFilter->GetOutput()->GetPoint(pts[pointIndex], trianglePoints[pointIndex]);
			}
			CBBox_AABB aabb;
			aabb.SetAABBForTriangle(trianglePoints);
			tree->AddObject(aabb, i);
		}
		
		std::cout << "Success Building Bounding Volume Hierarchy!" << std::endl;
		bvhTree = tree;
		isBuild = true;
	}

	if (isBuild)
	{
		if (key == "0")
		{
			depth = 0;
		}
		if (key == "period")
		{
			depth = bvhTree->GetMaxDepth();
		}
		if (key == "plus")
		{
			if (depth < bvhTree->GetMaxDepth())
				depth++;
		}
		if (key == "minus")
		{
			if (depth > 0)
				depth--;
		}

		renderer->RemoveAllViewProps();
		renderer->UseHiddenLineRemovalOn();

		if (bvhTree != nullptr)
		{
			auto aabbVec = bvhTree->GetTargetDepthAABBVec(depth);
			std::vector<vtkSmartPointer<vtkActor>> depthBBoxVec;
			for (int i = 0; i < aabbVec.size(); ++i)
			{
				depthBBoxVec.push_back(CBBox_RenderUtil::GetAABBLineActor(aabbVec[i], "Red"));
			}

			std::cout << " Depth  = " << depth << " / " << bvhTree->GetMaxDepth() << std::endl;
			for (int i = 0; i < depthBBoxVec.size(); ++i)
			{
				renderer->AddActor(depthBBoxVec[i]);
			}
		}

		auto actor = CMeshUtil::PolyDataToActor(polyData, "Gold");
		actor->SetUserMatrix(matrix);
		renderer->AddActor(actor);
	}
	
	renderer->GetRenderWindow()->Render();	
}