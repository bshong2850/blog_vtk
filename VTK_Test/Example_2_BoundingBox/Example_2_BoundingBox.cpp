#include "Example_2_BoundingBox.h"
#include "Example_2_BoundingBox_Callback.h"
#include "Example_2_BoundingBox_Mouse.h"
#include "../Util/MeshUtil.h"

#include <vtkActor.h>
#include <vtkCamera.h>
#include <vtkMatrix4x4.h>
#include <vtkNamedColors.h>
#include <vtkNew.h>
#include <vtkProperty.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkRenderer.h>
#include <vtkSphereSource.h>
#include <vtkTextActor.h>
#include <vtkTextProperty.h>
#include <vtkTransform.h>

#include <chrono>
#include <sstream>
#include <thread>

#include <vtkOBJReader.h>
#include <vtkOutlineFilter.h>
#include <vtkOutlineFilter.h>
#include <vtkTransformFilter.h>


//ReadObj
//path에 있는 OBJ를 읽어서 Polydata로 return
vtkSmartPointer<vtkPolyData> CExample_2_BoundingBox::ReadObj(std::string path)
{
	vtkSmartPointer<vtkPolyData> obj = vtkSmartPointer<vtkPolyData>::New();
	vtkSmartPointer<vtkOBJReader> reader = vtkSmartPointer<vtkOBJReader>::New();
	reader->SetFileName(path.c_str());
	reader->Update();

	obj->DeepCopy(reader->GetOutput());

	return obj;

}

vtkSmartPointer<vtkActor> CExample_2_BoundingBox::MeshInit(vtkSmartPointer<vtkPolyData> mesh, vtkSmartPointer<vtkMatrix4x4>& matrix, std::string color)
{
	vtkSmartPointer<vtkTransform> transform = vtkSmartPointer<vtkTransform>::New();
	transform->SetMatrix(matrix);

	vtkSmartPointer<vtkTransformFilter> transformFilter = vtkSmartPointer<vtkTransformFilter>::New();
	transformFilter->SetInputData(mesh);
	transformFilter->SetTransform(transform);
	transformFilter->Update();
	vtkSmartPointer<vtkPolyData> transformFilterPolyData = vtkSmartPointer<vtkPolyData>::New();
	transformFilterPolyData->DeepCopy(transformFilter->GetOutput());
	auto actor = CMeshUtil::PolyDataToActor(transformFilterPolyData);
	actor->GetProperty()->BackfaceCullingOn();
	actor->SetUserTransform(transform);
	actor->GetProperty()->SetDiffuseColor(colors->GetColor3d(color).GetData());
	actor->SetUserMatrix(matrix);

	return actor;
}

//boundingBox
//Mesh의 Bounding Box를 만들어주는 함수
void CExample_2_BoundingBox::boundingBox()
{
	vtkSmartPointer<vtkPolyData> mesh1 = ReadObj("../data/CatSmall.obj");
	vtkSmartPointer<vtkMatrix4x4> matrix1 = vtkSmartPointer<vtkMatrix4x4>::New();
	vtkSmartPointer<vtkActor> actor1 = vtkSmartPointer<vtkActor>::New();

	vtkSmartPointer<vtkPolyData> mesh2 = ReadObj("../data/CatSmall.obj");
	vtkSmartPointer<vtkMatrix4x4> matrix2 = vtkSmartPointer<vtkMatrix4x4>::New();
	vtkSmartPointer<vtkActor> actor2 = vtkSmartPointer<vtkActor>::New();

	actor1 = MeshInit(mesh1, matrix1, "Tomato");
	actor2 = MeshInit(mesh2, matrix2, "Gold");
		
	vtkSmartPointer<vtkRenderer> renderer = vtkSmartPointer<vtkRenderer>::New();
	renderer->UseHiddenLineRemovalOn();
	renderer->AddActor(actor1);
	renderer->AddActor(actor2);
	renderer->SetBackground(colors->GetColor3d("Gray").GetData());
	renderer->UseHiddenLineRemovalOn();

	vtkSmartPointer<vtkRenderWindow> renderWindow = vtkSmartPointer<vtkRenderWindow>::New();
	renderWindow->SetSize(640, 480);
	renderWindow->AddRenderer(renderer);

	vtkSmartPointer<vtkRenderWindowInteractor> interactor = vtkSmartPointer<vtkRenderWindowInteractor>::New();
	interactor->SetRenderWindow(renderWindow);

	CExample_2_KeyPressCallback *keyFunction = CExample_2_KeyPressCallback::New();
	keyFunction->SetPolyData1(mesh1, matrix1);
	keyFunction->SetPolyData2(mesh2, matrix2);
	keyFunction->SetRenderer(renderer);

	interactor->AddObserver(vtkCommand::KeyPressEvent, keyFunction);

	renderWindow->SetWindowName("Bounding Box");
	renderWindow->Render();

	renderer->ResetCamera();
	renderWindow->Render();
	interactor->Start();
	return;
}