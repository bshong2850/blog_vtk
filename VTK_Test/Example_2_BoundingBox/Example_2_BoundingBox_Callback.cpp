
#include "Example_2_BoundingBox_Callback.h"
#include "Example_2_BoundingBox_Mouse.h"
#include "Example_2_Util.h"
#include "../Util/MeshUtil.h"

#include <vtkRenderWindowInteractor.h>
#include <vtkInteractorStyleTrackballActor.h>
#include <vtkInteractorStyleTrackballCamera.h>
#include <vtkTransform.h>
#include <vtkTransformPolyDataFilter.h>
#include <vtkOBJWriter.h>

#include <vtkRenderer.h>
#include <vtkRenderWindow.h>

//생성자
CExample_2_KeyPressCallback::CExample_2_KeyPressCallback() : isVisualization(false)
{
}

//SetPolyData1
//1번째 Mesh Data Setting
void CExample_2_KeyPressCallback::SetPolyData1(vtkSmartPointer<vtkPolyData> vPD, vtkSmartPointer<vtkMatrix4x4> matrix)
{
	this->polyData1 = vPD;
	this->matrix1 = matrix;
}

//SetPolyData2
//2번째 Mesh Data Setting
void CExample_2_KeyPressCallback::SetPolyData2(vtkSmartPointer<vtkPolyData> vPD, vtkSmartPointer<vtkMatrix4x4> matrix)
{
	this->polyData2 = vPD;
	this->matrix2 = matrix;
}

//SetRenderer
//Renderer Setting
void CExample_2_KeyPressCallback::SetRenderer(vtkSmartPointer<vtkRenderer> vR)
{
	this->renderer = vR;
}

//Execute
//KeyPress Event가 있을 때 호출되는 함수
void CExample_2_KeyPressCallback::Execute(vtkObject *caller, unsigned long, void*)
{
	vtkRenderWindowInteractor *rwi = reinterpret_cast<vtkRenderWindowInteractor *>(caller);
	std::string key = rwi->GetKeySym();


	vtkSmartPointer<vtkInteractorStyle> style;

	//a를 누르면 TrackballActor 모드로 전환(각 Mesh를 각각 Transform 가능)
	if (key == "a")
	{
		std::cout << "Camera Mode    : Trackball Actor Mode" << std::endl;
		style = vtkSmartPointer<vtkInteractorStyleTrackballActor>::New();
		rwi->SetInteractorStyle(style);
	}

	//z를 누르면 Data 저장
	if (key == "z")
	{
		vtkSmartPointer<vtkTransform> poly1Transform = vtkSmartPointer<vtkTransform>::New();
		poly1Transform->SetMatrix(matrix1);
		vtkSmartPointer<vtkTransformPolyDataFilter> poly1TransformFilter = vtkSmartPointer<vtkTransformPolyDataFilter>::New();

		poly1TransformFilter->SetTransform(poly1Transform);
		poly1TransformFilter->SetInputData(polyData1);
		poly1TransformFilter->Update();

		vtkSmartPointer<vtkOBJWriter> writer = vtkSmartPointer<vtkOBJWriter>::New();
		writer->SetFileName("../data/saveMesh1.obj");
		writer->SetInputData(poly1TransformFilter->GetOutput());
		writer->Update();

		vtkSmartPointer<vtkTransform> poly2Transform = vtkSmartPointer<vtkTransform>::New();
		poly2Transform->SetMatrix(matrix2);
		vtkSmartPointer<vtkTransformPolyDataFilter> poly2TransformFilter = vtkSmartPointer<vtkTransformPolyDataFilter>::New();

		poly2TransformFilter->SetTransform(poly2Transform);
		poly2TransformFilter->SetInputData(polyData2);
		poly2TransformFilter->Update();

		writer->SetFileName("../data/saveMesh2.obj");
		writer->SetInputData(poly2TransformFilter->GetOutput());
		writer->Update();
	}
	
	if (key == "space" || key == "4" || key == "5" || key == "6" || key == "t")
	{
		//Space를 누르면 Rendering On Off
		if (key == "space")
		{
			isVisualization = !isVisualization;
			std::cout << "Visualization On Off Mode : Now State = " << isVisualization << std::endl;
			style1->SetBBoxVisualizationOnOff(isVisualization);
		}

		//t를 누르면 Trackball Camera 모드로 전환(Camera Transform 가능)
		if (key == "t")
		{
			std::cout << "Camera Mode     : Trackball Camera Mode" << std::endl;
			isVisualization = false;
			style1->SetBBoxVisualizationOnOff(isVisualization);
		}

		if (key == "4")
		{
			//4를 누르면 Bounding Box가 AABB로 표현
			std::cout << "Bbox Mode     : AABB" << std::endl;
			style1->SetBBoxMode(BB_MODE::AABB);
		}
		else if (key == "5")
		{
			//5를 누르면 Bounding Box가 OBB로 표현
			std::cout << "Bbox Mode     : OBB" << std::endl;
			style1->SetBBoxMode(BB_MODE::OBB);
		}
		else if (key == "6")
		{
			//6를 누르면 Bounding Box가 Sphere로 표현
			std::cout << "Bbox Mode     : SPHERE" << std::endl;
			std::vector<double> radiusVec;
			radiusVec.push_back(CExample_2_Util::ComputeRadius(polyData1));
			radiusVec.push_back(CExample_2_Util::ComputeRadius(polyData2));
			style1->SetBBoxMode(BB_MODE::SPHERE);
			style1->SetRadius(radiusVec);
		}
		style1->SetPolyData1(polyData1, matrix1);
		style1->SetPolyData2(polyData2, matrix2);
		style1->SetRenderer(renderer);
		rwi->SetInteractorStyle(style1);
	}

}