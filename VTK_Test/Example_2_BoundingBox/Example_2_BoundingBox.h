#pragma once
#include <iostream>
#include <memory>
#include <string>

#include "vtkNew.h"
#include "vtkSmartPointer.h"
#include "vtkNamedColors.h"

class vtkPolyData;
class vtkActor;
class vtkMatrix4x4;

//2번째 Example Bounding Box와 관련된 Class
class CExample_2_BoundingBox
{
public:

	CExample_2_BoundingBox() 
	{
		colors = vtkSmartPointer<vtkNamedColors>::New();
	};
	~CExample_2_BoundingBox() {};

	void boundingBox();

private:
	vtkSmartPointer<vtkNamedColors> colors;

	vtkSmartPointer<vtkPolyData> ReadObj(std::string path);
	vtkSmartPointer<vtkActor> MeshInit(vtkSmartPointer<vtkPolyData> mesh, vtkSmartPointer<vtkMatrix4x4>& matrix, std::string color);
};