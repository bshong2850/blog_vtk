#pragma once


#include "vtkSmartPointer.h"
#include "vtkNamedColors.h"
#include "vtkInteractorStyleTrackballCamera.h"
#include <vector>
class vtkPolyData;
class vtkMatrix4x4;
class vtkRenderer;
class vtkActor;

//BB_MODE
//3가지의 Mode를 가지고 있음
enum BB_MODE
{
	AABB,
	OBB,
	SPHERE
};

//Mouse Interactor 클래스
//Mouse와 관련되 함수들 제어
class CExample_2_MouseInteractorStyle : public vtkInteractorStyleTrackballCamera
{
public:
	static CExample_2_MouseInteractorStyle* New()
	{
		return new CExample_2_MouseInteractorStyle;
	}

	CExample_2_MouseInteractorStyle();
	virtual void OnMouseMove();

	void SetRenderer(vtkSmartPointer<vtkRenderer> vR);
	void SetPolyData1(vtkSmartPointer<vtkPolyData> vPD, vtkSmartPointer<vtkMatrix4x4> matrix);
	void SetPolyData2(vtkSmartPointer<vtkPolyData> vPD, vtkSmartPointer<vtkMatrix4x4> matrix);
	void SetRadius(std::vector<double> radiusVec);

	void SetBBoxMode(BB_MODE m);
	void SetBBoxVisualizationOnOff(bool isVisualization);
	
private:
	vtkSmartPointer<vtkNamedColors> colors = vtkSmartPointer<vtkNamedColors>::New();

	vtkSmartPointer<vtkRenderer> renderer;

	vtkSmartPointer<vtkPolyData> polyData1;
	vtkSmartPointer<vtkMatrix4x4> matrix1;
	vtkSmartPointer<vtkPolyData> polyData2;
	vtkSmartPointer<vtkMatrix4x4> matrix2;
	std::vector<double> radiusVec;
	bool isVisualization = true;

	BB_MODE bMode;
};