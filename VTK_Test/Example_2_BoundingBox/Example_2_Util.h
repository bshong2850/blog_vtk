#pragma once
#include <iostream>
#include <memory>
#include <string>

#include "vtkSmartPointer.h"

class vtkPolyData;
class vtkActor;
class vtkMatrix4x4;

class CExample_2_Util
{
public:

	CExample_2_Util() {};
	~CExample_2_Util() {};

	static vtkSmartPointer<vtkActor> GetAABBLineActor(vtkSmartPointer<vtkPolyData> polyData, vtkSmartPointer<vtkMatrix4x4> matrix, std::string color);
	static vtkSmartPointer<vtkActor> CExample_2_Util::GetOBBLineActor(vtkSmartPointer<vtkPolyData> polyData, vtkSmartPointer<vtkMatrix4x4> matrix, std::string color);
	static vtkSmartPointer<vtkActor> CExample_2_Util::GetSphereLineActor(vtkSmartPointer<vtkPolyData> polyData, vtkSmartPointer<vtkMatrix4x4> matrix, double radius, std::string color);
	static double CExample_2_Util::ComputeRadius(vtkSmartPointer<vtkPolyData> polyData);
};