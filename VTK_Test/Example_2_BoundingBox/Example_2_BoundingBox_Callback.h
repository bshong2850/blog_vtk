#pragma once

#include <vtkCommand.h>
#include <vtkSmartPointer.h>

#include <iostream>
#include <vector>

class vtkPolyData;
class vtkMatrix4x4;
class vtkRenderer;
class CExample_2_MouseInteractorStyle;

class  CExample_2_KeyPressCallback : public vtkCommand
{
public:
	static CExample_2_KeyPressCallback *New()
	{
		return new CExample_2_KeyPressCallback;
	}
	CExample_2_KeyPressCallback();
	virtual void Execute(vtkObject *caller, unsigned long, void*);
	void SetRenderer(vtkSmartPointer<vtkRenderer> vR);
	void SetPolyData1(vtkSmartPointer<vtkPolyData> vPD, vtkSmartPointer<vtkMatrix4x4> matrix);
	void SetPolyData2(vtkSmartPointer<vtkPolyData> vPD, vtkSmartPointer<vtkMatrix4x4> matrix);

private:

	vtkSmartPointer<vtkRenderer> renderer;
	vtkSmartPointer<vtkPolyData> polyData1;
	vtkSmartPointer<vtkMatrix4x4> matrix1;
	vtkSmartPointer<vtkPolyData> polyData2;
	vtkSmartPointer<vtkMatrix4x4> matrix2;

	bool isVisualization;

	vtkSmartPointer<CExample_2_MouseInteractorStyle> style1 = vtkSmartPointer<CExample_2_MouseInteractorStyle>::New();
};