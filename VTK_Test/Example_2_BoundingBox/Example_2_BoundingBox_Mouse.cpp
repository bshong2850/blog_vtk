
#include "Example_2_BoundingBox_Mouse.h"
#include "Example_2_Util.h"
#include "../Util/MeshUtil.h"
#include <vtkRenderer.h>

#include "vtkRenderWindowInteractor.h"
#include <vtkRenderWindow.h>
#include <vtkRenderer.h>
#include <vtkPropPicker.h>
#include <vtkMapper.h>
#include <vtkPolyData.h>
#include <vtkProperty.h>
#include <vtkMatrix4x4.h>

//생성자
//Bounding Box Mode를 AABB로 Initialize
CExample_2_MouseInteractorStyle::CExample_2_MouseInteractorStyle()
{
	bMode = BB_MODE::AABB;
}

//SetPolyData1
//1번째 Mesh Data Setting
void CExample_2_MouseInteractorStyle::SetPolyData1(vtkSmartPointer<vtkPolyData> vPD, vtkSmartPointer<vtkMatrix4x4> matrix)
{
	this->polyData1 = vPD;
	this->matrix1 = matrix;
}

//SetPolyData2
//2번째 Mesh Data Setting
void CExample_2_MouseInteractorStyle::SetPolyData2(vtkSmartPointer<vtkPolyData> vPD, vtkSmartPointer<vtkMatrix4x4> matrix)
{
	this->polyData2 = vPD;
	this->matrix2 = matrix;
}

//SetRenderer
//Renderer Setting
void CExample_2_MouseInteractorStyle::SetRenderer(vtkSmartPointer<vtkRenderer> vR)
{
	this->renderer = vR;
}

//SetRadius
//Mesh 수 만큼의 radius를 vector로 Setting
void CExample_2_MouseInteractorStyle::SetRadius(std::vector<double> radiusVec)
{
	this->radiusVec = radiusVec;
}

//SetBBoxMode
//Bounding Box의 종류를 Setting
void CExample_2_MouseInteractorStyle::SetBBoxMode(BB_MODE m)
{
	this->bMode = m;
}

//SetBBoxVisualizationOnOff
//Bounding Box의 Rendering 여부를 Setting
void CExample_2_MouseInteractorStyle::SetBBoxVisualizationOnOff(bool isVisualization)
{
	this->isVisualization = isVisualization;
}

//OnMouseMove
//Mouse가 움직이고 있을 때 호출되는 함수
void CExample_2_MouseInteractorStyle::OnMouseMove()
{

	std::vector<vtkSmartPointer<vtkActor>> totalActorVec;
	if (isVisualization)
	{
		if (bMode == BB_MODE::AABB)
		{
			totalActorVec.push_back(CExample_2_Util::GetAABBLineActor(polyData1, matrix1, "Red"));
			totalActorVec.push_back(CExample_2_Util::GetAABBLineActor(polyData2, matrix2, "Red"));
		}
		else if (bMode == BB_MODE::OBB)
		{
			totalActorVec.push_back(CExample_2_Util::GetOBBLineActor(polyData1, matrix1, "Red"));
			totalActorVec.push_back(CExample_2_Util::GetOBBLineActor(polyData2, matrix2, "Red"));
		}
		else if (bMode == BB_MODE::SPHERE)
		{
			totalActorVec.push_back(CExample_2_Util::GetSphereLineActor(polyData1, matrix1, radiusVec[0], "Red"));
			totalActorVec.push_back(CExample_2_Util::GetSphereLineActor(polyData2, matrix2, radiusVec[1], "Red"));
		}
	}


	auto actor1 = CMeshUtil::PolyDataToActor(polyData1, "Tomato");
	actor1->SetUserMatrix(matrix1);
	auto actor2 = CMeshUtil::PolyDataToActor(polyData2, "Gold");
	actor2->SetUserMatrix(matrix2);
	totalActorVec.push_back(actor1);
	totalActorVec.push_back(actor2);

	renderer->RemoveAllViewProps();
	renderer->UseHiddenLineRemovalOn();

	for (int i = 0; i < totalActorVec.size(); ++i)
	{
		renderer->AddActor(totalActorVec[i]);
	}
	renderer->GetRenderWindow()->Render();

	vtkInteractorStyleTrackballCamera::OnMouseMove();
}



