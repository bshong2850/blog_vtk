#include "Example_2_Util.h"
#include "../Util/MeshUtil.h"

#include <vtkTransform.h>
#include <vtkTransformFilter.h>
#include <vtkOutlineFilter.h>
#include <vtkPolyDataMapper.h>
#include <vtkActor.h>
#include <vtkProperty.h>
#include <vtkNamedColors.h>
#include <time.h>

vtkSmartPointer<vtkActor> CExample_2_Util::GetAABBLineActor(
	vtkSmartPointer<vtkPolyData> polyData, vtkSmartPointer<vtkMatrix4x4> matrix, std::string color)
{
	auto transformPolyData = CMeshUtil::SetTransform(polyData, matrix);

	vtkSmartPointer<vtkOutlineFilter> outline = vtkSmartPointer<vtkOutlineFilter>::New();
	outline->SetInputData(transformPolyData);

	vtkSmartPointer<vtkPolyDataMapper> outlineMapper = vtkSmartPointer<vtkPolyDataMapper>::New();
	outlineMapper->SetInputConnection(outline->GetOutputPort());
	vtkSmartPointer<vtkActor> outlineActor = vtkSmartPointer<vtkActor>::New();
	outlineActor->SetMapper(outlineMapper);
	vtkSmartPointer<vtkNamedColors> colors = vtkSmartPointer<vtkNamedColors>::New();
	outlineActor->GetProperty()->SetColor(colors->GetColor3d(color).GetData());
	outlineActor->PickableOff();
	return outlineActor;
}


#include <vtkOBBTree.h>
vtkSmartPointer<vtkActor> CExample_2_Util::GetOBBLineActor(
	vtkSmartPointer<vtkPolyData> polyData, vtkSmartPointer<vtkMatrix4x4> matrix, std::string color)
{
	auto transformPolyData = CMeshUtil::SetTransform(polyData, matrix);

	vtkSmartPointer<vtkOBBTree> obbTree = vtkSmartPointer<vtkOBBTree>::New();
	obbTree->SetDataSet(transformPolyData);
	obbTree->SetMaxLevel(1);
	obbTree->BuildLocator();
	vtkSmartPointer<vtkPolyData> polydata = vtkSmartPointer<vtkPolyData>::New();
	obbTree->GenerateRepresentation(0, polydata);

	vtkSmartPointer<vtkActor> obbtreeActor = vtkSmartPointer<vtkActor>::New();
	obbtreeActor = CMeshUtil::PolyDataToActor(polydata, color);
	obbtreeActor->GetProperty()->SetInterpolationToFlat();
	obbtreeActor->GetProperty()->SetOpacity(.5);
	obbtreeActor->GetProperty()->EdgeVisibilityOn();
	obbtreeActor->GetProperty()->SetRepresentationToWireframe();
	obbtreeActor->PickableOff();

	return obbtreeActor;
}


#include <vtkSphereSource.h>
vtkSmartPointer<vtkActor> CExample_2_Util::GetSphereLineActor(
	vtkSmartPointer<vtkPolyData> polyData, vtkSmartPointer<vtkMatrix4x4> matrix, double radius , std::string color)
{

	double center[3];
	double maxLength = std::numeric_limits<double>::min();

	auto transformPolyData = CMeshUtil::SetTransform(polyData, matrix);
	transformPolyData->GetCenter(center);

	vtkSmartPointer<vtkSphereSource> sphere = vtkSmartPointer<vtkSphereSource>::New();
	sphere->SetRadius(radius);
	sphere->SetPhiResolution(31);
	sphere->SetThetaResolution(31);
	sphere->SetCenter(center);
	sphere->Update();

	vtkSmartPointer<vtkActor> outlineActor = vtkSmartPointer<vtkActor>::New();
	outlineActor = CMeshUtil::PolyDataToActor(sphere->GetOutput(), color);
	outlineActor->PickableOff();
	outlineActor->GetProperty()->SetRepresentationToWireframe();

	return outlineActor;

}


double CExample_2_Util::ComputeRadius(vtkSmartPointer<vtkPolyData> polyData)
{
	double center[3];
	double maxLength = std::numeric_limits<double>::min();

	polyData->GetCenter(center);
	int nSize = polyData->GetNumberOfPoints();

	for (int i = 0; i < nSize; ++i)
	{
		double point[3];
		polyData->GetPoint(i, point);
		double length = std::sqrt((point[0] - center[0]) * (point[0] - center[0])
			+ (point[1] - center[1]) * (point[1] - center[1])
			+ (point[2] - center[2]) * (point[2] - center[2]));

		if (length > maxLength)
			maxLength = length;
	}
	return maxLength;
}