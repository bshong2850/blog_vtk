#include "Example_6_Parameterization.h"
#include "Example_6_Parameterization_Callback.h"
#include "../Util/MeshUtil.h"

#include <vtkActor.h>
#include <vtkCamera.h>
#include <vtkMatrix4x4.h>
#include <vtkNamedColors.h>
#include <vtkProperty.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkRenderer.h>
#include <vtkTransform.h>
#include <vtkSphereSource.h>
#include <vtkPolyDataMapper.h>

#include <vtkTransformFilter.h>

//Run
//Sphere 생성 및 Callback 붙여놓은 상태
void CExample_6_Parameterization::Run()
{
	vtkSmartPointer<vtkPolyData> mesh = CMeshUtil::ReadObj("../data/faceMesh.obj");
	vtkSmartPointer<vtkMatrix4x4> matrix = vtkSmartPointer<vtkMatrix4x4>::New();

	auto actor = CMeshUtil::PolyDataToActor(mesh, "Gold");
	actor->SetUserMatrix(matrix);
	


	vtkSmartPointer<vtkNamedColors> colors = vtkSmartPointer<vtkNamedColors>::New();
	std::vector<vtkSmartPointer<vtkRenderer>> renderer;
	vtkSmartPointer<vtkRenderWindow> renderWindow = vtkSmartPointer<vtkRenderWindow>::New();
	vtkSmartPointer<vtkRenderWindowInteractor> renderInteractor = vtkSmartPointer<vtkRenderWindowInteractor>::New();

	renderer.push_back(vtkSmartPointer<vtkRenderer>::New());
	renderer.push_back(vtkSmartPointer<vtkRenderer>::New());
	renderer[0]->SetViewport(0, 0, 1.0 / 2.0, 1);
	renderer[1]->SetViewport(1.0 / 2.0, 0, 1, 1);
	
	//Actor Rendering
	renderer[0]->UseHiddenLineRemovalOn();
	renderer[0]->AddActor(actor);
	renderer[0]->SetBackground(colors->GetColor3d("Gray").GetData());

	renderer[1]->UseHiddenLineRemovalOn();
	renderer[1]->SetBackground(colors->GetColor3d("Black").GetData());

	renderWindow->AddRenderer(renderer[0]);
	renderWindow->AddRenderer(renderer[1]);
	renderWindow->SetSize(1024, 512);


	vtkSmartPointer<vtkRenderWindowInteractor> interactor = vtkSmartPointer<vtkRenderWindowInteractor>::New();
	interactor->SetRenderWindow(renderWindow);


	CExample_6_KeyPressCallback *keyFunction = CExample_6_KeyPressCallback::New();
	keyFunction->SetDataVec(mesh, matrix);

	keyFunction->SetRenderer(renderer);

	interactor->AddObserver(vtkCommand::KeyPressEvent, keyFunction);

	renderWindow->SetWindowName("Example");
	renderWindow->Render();

	renderer[0]->ResetCamera();
	renderer[1]->ResetCamera();

	renderWindow->Render();
	interactor->Start();

	return;
}
