#pragma once

#include <vtkCommand.h>
#include <vtkSmartPointer.h>

#include <iostream>
#include <vector>
#include <memory>

class vtkPolyData;
class vtkMatrix4x4;
class vtkRenderer;
class CParameterization;

//다른 Example 만들 때 Sample로 불러오기 위한 Code
class  CExample_6_KeyPressCallback : public vtkCommand
{
public:
	static CExample_6_KeyPressCallback *New()
	{
		return new CExample_6_KeyPressCallback;
	}
	CExample_6_KeyPressCallback();
	virtual void Execute(vtkObject *caller, unsigned long, void*);
	void SetRenderer(std::vector<vtkSmartPointer<vtkRenderer>> vR);
	void SetDataVec(vtkSmartPointer<vtkPolyData> vpdVec, vtkSmartPointer<vtkMatrix4x4> matrixVec);

private:
	std::vector<vtkSmartPointer<vtkRenderer>> renderer;

	std::vector<vtkSmartPointer<vtkPolyData>> polyDataVec;
	std::vector<vtkSmartPointer<vtkMatrix4x4>> matrixVec;

	bool isParameterization;
	std::shared_ptr<CParameterization> prarmeterization = std::make_shared<CParameterization>();
};