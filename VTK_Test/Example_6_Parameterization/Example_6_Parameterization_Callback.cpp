#include "Example_6_Parameterization_Callback.h"

#include "../Util/Parameterization.h"
#include "../Util/MeshUtil.h"

#include <vtkRenderWindowInteractor.h>
#include <vtkInteractorStyleTrackballActor.h>
#include <vtkInteractorStyleTrackballCamera.h>
#include <vtkTransform.h>
#include <vtkTransformPolyDataFilter.h>
#include <vtkOBJWriter.h>
#include <vtkCubeSource.h>
#include <vtkSphereSource.h>
#include <vtkProperty.h>

#include <vtkRenderer.h>
#include <vtkRenderWindow.h>

//생성자
CExample_6_KeyPressCallback::CExample_6_KeyPressCallback() : isParameterization(false)
{
}

//SetDataVec
//PolyData 및 Matrix를 set 해주는 함수
void CExample_6_KeyPressCallback::SetDataVec(vtkSmartPointer<vtkPolyData> polyData, vtkSmartPointer<vtkMatrix4x4> matrix)
{
	this->polyDataVec.push_back(polyData);
	this->matrixVec.push_back(matrix);
}

//SetRenderer
//Renderer를 set 해주는 함수
void CExample_6_KeyPressCallback::SetRenderer(std::vector<vtkSmartPointer<vtkRenderer>> vR)
{
	this->renderer = vR;
}


//Execute
//Keypress Event가 있을 때 불리는 함수
void CExample_6_KeyPressCallback::Execute(vtkObject *caller, unsigned long, void*)
{
	vtkRenderWindowInteractor *rwi = reinterpret_cast<vtkRenderWindowInteractor *>(caller);
	std::string key = rwi->GetKeySym();
	vtkSmartPointer<vtkInteractorStyle> style;

	if (key == "a")
	{
		std::cout << "Actor Mode     : Trackball Actor Mode" << std::endl;
		style = vtkSmartPointer<vtkInteractorStyleTrackballActor>::New();
		rwi->SetInteractorStyle(style);
	}
	if (key == "t")
	{
		std::cout << "Camera Mode    : Trackball Camera Mode" << std::endl;
		style = vtkSmartPointer<vtkInteractorStyleTrackballCamera>::New();
		rwi->SetInteractorStyle(style);
	}
	if (key == "1")
	{
		prarmeterization->Parametrization(CMeshUtil::SetTransform(polyDataVec[0], matrixVec[0]), WeightOption::Harmonic);
		isParameterization = true;
	}

	if (isParameterization)
	{
		auto actor = CMeshUtil::PolyDataToActor(CMeshUtil::SetTransform(polyDataVec[0], matrixVec[0]), "Gold");

		auto boundaryLine3DActor = CMeshUtil::PolyDataToActor(prarmeterization->boundaryLine3DPolyData, "Blue");
		boundaryLine3DActor->GetProperty()->SetLineWidth(2.0);
		auto boundaryPoints3DActor = CMeshUtil::PolyDataToActor(prarmeterization->boundaryPoints3DPolyData, "Blue");
		boundaryPoints3DActor->GetProperty()->SetPointSize(4.0);
		auto InnerPoints3DActor = CMeshUtil::PolyDataToActor(prarmeterization->InnerPoints3DPolyData, "Red");
		InnerPoints3DActor->GetProperty()->SetPointSize(4.0);
		 
		auto line2DActor = CMeshUtil::PolyDataToActor(prarmeterization->innerLine2DPolyData, "Gold");
		line2DActor->GetProperty()->SetLineWidth(2.0);
		auto boundaryLine2DActor = CMeshUtil::PolyDataToActor(prarmeterization->boundaryLine2DPolyData, "Blue");
		boundaryLine2DActor->GetProperty()->SetLineWidth(2.0);
		auto boundaryPoints2DActor = CMeshUtil::PolyDataToActor(prarmeterization->boundaryPoints2DPolyData, "White");
		boundaryPoints2DActor->GetProperty()->SetPointSize(4.0);
		auto InnerPoints2DActor = CMeshUtil::PolyDataToActor(prarmeterization->innerPoints2DPolyData, "Red");
		InnerPoints2DActor->GetProperty()->SetPointSize(4.0);


		//Actor Rendering
		renderer[0]->RemoveAllViewProps();
		renderer[0]->UseHiddenLineRemovalOn();
		renderer[0]->AddActor(boundaryLine3DActor);
		renderer[0]->AddActor(boundaryPoints3DActor);
		renderer[0]->AddActor(InnerPoints3DActor);
		renderer[0]->AddActor(actor);
		renderer[0]->ResetCamera();

		renderer[1]->RemoveAllViewProps();
		renderer[1]->UseHiddenLineRemovalOn();
		renderer[1]->AddActor(line2DActor);
		renderer[1]->AddActor(boundaryLine2DActor);
		renderer[1]->AddActor(boundaryPoints2DActor);
		renderer[1]->AddActor(InnerPoints2DActor);
		renderer[1]->ResetCamera();

		renderer[0]->GetRenderWindow()->Render();
		renderer[1]->GetRenderWindow()->Render();
	}
	
}