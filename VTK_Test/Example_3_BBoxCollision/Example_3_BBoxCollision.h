#pragma once
#include <iostream>
#include <memory>
#include <string>

#include "vtkNew.h"
#include "vtkSmartPointer.h"
#include "vtkNamedColors.h"

class vtkPolyData;
class vtkActor;
class vtkMatrix4x4;

//3번째 Example Bounding Box Collision Detection과 관련된 Class
class CExample_3_BBoxCollision
{
public:

	CExample_3_BBoxCollision()
	{
		colors = vtkSmartPointer<vtkNamedColors>::New();
	};
	~CExample_3_BBoxCollision() {};

	void BboxCollisionDetection();

private:
	vtkSmartPointer<vtkNamedColors> colors;

	vtkSmartPointer<vtkActor> MeshInit(vtkSmartPointer<vtkPolyData> mesh, vtkSmartPointer<vtkMatrix4x4>& matrix, std::string color);
};