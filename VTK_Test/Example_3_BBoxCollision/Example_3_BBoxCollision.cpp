#include "Example_3_BBoxCollision.h"
#include "Example_3_BBoxCollision_Callback.h"
#include "Example_3_BBoxCollision_Mouse.h"
#include "../Util/MeshUtil.h"

#include <vtkActor.h>
#include <vtkCamera.h>
#include <vtkMatrix4x4.h>
#include <vtkNamedColors.h>
#include <vtkProperty.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkRenderer.h>
#include <vtkSphereSource.h>
#include <vtkTextActor.h>
#include <vtkTextProperty.h>
#include <vtkTransform.h>

#include <chrono>
#include <sstream>
#include <thread>

#include <vtkOBJReader.h>
#include <vtkOutlineFilter.h>
#include <vtkOutlineFilter.h>
#include <vtkTransformFilter.h>


vtkSmartPointer<vtkActor> CExample_3_BBoxCollision::MeshInit(vtkSmartPointer<vtkPolyData> mesh, vtkSmartPointer<vtkMatrix4x4>& matrix, std::string color)
{
	vtkSmartPointer<vtkTransform> transform = vtkSmartPointer<vtkTransform>::New();
	transform->SetMatrix(matrix);

	vtkSmartPointer<vtkTransformFilter> transformFilter = vtkSmartPointer<vtkTransformFilter>::New();
	transformFilter->SetInputData(mesh);
	transformFilter->SetTransform(transform);
	transformFilter->Update();
	vtkSmartPointer<vtkPolyData> transformFilterPolyData = vtkSmartPointer<vtkPolyData>::New();
	transformFilterPolyData->DeepCopy(transformFilter->GetOutput());
	auto actor = CMeshUtil::PolyDataToActor(transformFilterPolyData);
	actor->GetProperty()->BackfaceCullingOn();
	actor->SetUserTransform(transform);
	actor->GetProperty()->SetDiffuseColor(colors->GetColor3d(color).GetData());
	actor->SetUserMatrix(matrix);

	return actor;
}

//BboxCollisionDetection
//Mesh의 Bounding Box를 만들어주는 함수
void CExample_3_BBoxCollision::BboxCollisionDetection()
{
	vtkSmartPointer<vtkPolyData> mesh1 = CMeshUtil::ReadObj("../data/CatSmall.obj");
	//vtkSmartPointer<vtkPolyData> mesh1 = ReadObj("../data/Cat.obj");
	vtkSmartPointer<vtkMatrix4x4> matrix1 = vtkSmartPointer<vtkMatrix4x4>::New();
	vtkSmartPointer<vtkActor> actor1 = vtkSmartPointer<vtkActor>::New();

	vtkSmartPointer<vtkPolyData> mesh2 = CMeshUtil::ReadObj("../data/CatSmall.obj");
	//vtkSmartPointer<vtkPolyData> mesh2 = ReadObj("../data/Cat.obj");
	vtkSmartPointer<vtkMatrix4x4> matrix2 = vtkSmartPointer<vtkMatrix4x4>::New();
	vtkSmartPointer<vtkActor> actor2 = vtkSmartPointer<vtkActor>::New();

	std::vector<std::string> colorVec;
	colorVec.push_back("Tomato");
	colorVec.push_back("Gold");
	actor1 = MeshInit(mesh1, matrix1, colorVec[0]);
	actor2 = MeshInit(mesh2, matrix2, colorVec[1]);
		
	vtkSmartPointer<vtkRenderer> renderer = vtkSmartPointer<vtkRenderer>::New();
	renderer->UseHiddenLineRemovalOn();
	renderer->AddActor(actor1);
	renderer->AddActor(actor2);
	renderer->SetBackground(colors->GetColor3d("Gray").GetData());
	renderer->UseHiddenLineRemovalOn();

	vtkSmartPointer<vtkRenderWindow> renderWindow = vtkSmartPointer<vtkRenderWindow>::New();
	renderWindow->SetSize(640, 480);
	renderWindow->AddRenderer(renderer);
	renderWindow->SetWindowName("BoundingBoxCollision");

	vtkSmartPointer<vtkRenderWindowInteractor> interactor = vtkSmartPointer<vtkRenderWindowInteractor>::New();
	interactor->SetRenderWindow(renderWindow);

	CExample_3_KeyPressCallback *keyFunction = CExample_3_KeyPressCallback::New();
	keyFunction->SetPolyData(mesh1, matrix1, colorVec[0]);
	keyFunction->SetPolyData(mesh2, matrix2, colorVec[1]);
	keyFunction->SetRenderer(renderer);

	interactor->AddObserver(vtkCommand::KeyPressEvent, keyFunction);
	
	renderer->ResetCamera();
	renderWindow->Render();
	interactor->Start();
	return;
}