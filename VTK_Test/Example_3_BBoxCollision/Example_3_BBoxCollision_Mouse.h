#pragma once


#include "vtkSmartPointer.h"
#include "vtkNamedColors.h"
#include "vtkInteractorStyleTrackballActor.h"
#include "../Util/BoundingBox.h"
#include <vector>
#include <set>

class vtkPolyData;
class vtkMatrix4x4;
class vtkRenderer;
class vtkActor;

//BB_MODE
//3가지의 Mode를 가지고 있음
enum BB_MODE
{
	AABB,
	OBB,
	SPHERE
};

//Mouse Interactor 클래스
//Mouse와 관련되 함수들 제어
class CExample_3_MouseInteractorStyle : public vtkInteractorStyleTrackballActor
{
public:
	static CExample_3_MouseInteractorStyle* New()
	{
		return new CExample_3_MouseInteractorStyle;
	}

	CExample_3_MouseInteractorStyle();
	virtual void OnMouseMove();
	virtual void OnRightButtonDown();

	void Initialize();
	void SetRenderer(vtkSmartPointer<vtkRenderer> vR);
	void SetPolyData(vtkSmartPointer<vtkPolyData> vPD, vtkSmartPointer<vtkMatrix4x4> matrix, std::string color);
	//void SetPolyData2(vtkSmartPointer<vtkPolyData> vPD, vtkSmartPointer<vtkMatrix4x4> matrix);
	void SetRadius(std::vector<double> radiusVec);

	void SetBBoxMode(BB_MODE m);
	void SetBBoxVisualizationOnOff(bool isVisualization);
	
private:
	vtkSmartPointer<vtkNamedColors> colors = vtkSmartPointer<vtkNamedColors>::New();

	vtkSmartPointer<vtkRenderer> renderer;

	std::set<int> BBoxCollisionCheck();
	void BBoxInitialize();
	std::vector<CBBox_AABB> aabbVec;
	std::vector<CBBox_OBB> obbVec;
	std::vector<CBBox_SPHERE> sphereBboxVec;

	std::vector<std::string> colorVec;
	std::vector<std::pair<vtkSmartPointer<vtkPolyData>, vtkSmartPointer<vtkMatrix4x4>>> polyDataPairVec;
	std::vector<double> radiusVec;
	bool isVisualization = false;

	BB_MODE bMode;
};