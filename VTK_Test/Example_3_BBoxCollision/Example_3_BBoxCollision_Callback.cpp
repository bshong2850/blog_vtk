
#include "Example_3_BBoxCollision_Callback.h"
#include "Example_3_BBoxCollision_Mouse.h"
#include "../Util/BoundingBox_RenderUtil.h"
#include "../Util/MeshUtil.h"

#include <vtkRenderWindowInteractor.h>
#include <vtkInteractorStyleTrackballActor.h>
#include <vtkInteractorStyleTrackballCamera.h>
#include <vtkTransform.h>
#include <vtkTransformPolyDataFilter.h>
#include <vtkOBJWriter.h>

#include <vtkRenderer.h>
#include <vtkRenderWindow.h>
#include <vtkTransformFilter.h>

#include <vtkOBBTree.h>


//생성자
CExample_3_KeyPressCallback::CExample_3_KeyPressCallback() : isVisualization(false)
{
}

//SetPolyData
//polyDataPairVec에 PolyData Matrix Pair를 Set하는 함수
void CExample_3_KeyPressCallback::SetPolyData(vtkSmartPointer<vtkPolyData> vPD, vtkSmartPointer<vtkMatrix4x4> matrix, std::string color)
{

	polyDataPairVec.push_back(std::pair<vtkSmartPointer<vtkPolyData>, vtkSmartPointer<vtkMatrix4x4>>(vPD, matrix));
	colorVec.push_back(color);
}

//Initialize
//polyDataPairVec를 초기화 해주는 함수
void CExample_3_KeyPressCallback::Initialize()
{
	polyDataPairVec.clear();
	std::vector<std::pair<vtkSmartPointer<vtkPolyData>, vtkSmartPointer<vtkMatrix4x4>>>().swap(polyDataPairVec);
}

//SetRenderer
//Renderer Setting
void CExample_3_KeyPressCallback::SetRenderer(vtkSmartPointer<vtkRenderer> vR)
{
	this->renderer = vR;
}

//Execute
//KeyPress Event가 있을 때 호출되는 함수
void CExample_3_KeyPressCallback::Execute(vtkObject *caller, unsigned long, void*)
{
	vtkRenderWindowInteractor *rwi = reinterpret_cast<vtkRenderWindowInteractor *>(caller);
	std::string key = rwi->GetKeySym();

	//t를 누르면 Trackball Camera 모드로 전환(Camera Transform 가능)
	if (key == "t")
	{
		vtkSmartPointer<vtkInteractorStyle> style;
		std::cout << "Camera Mode     : Trackball Camera Mode" << std::endl;
		style = vtkSmartPointer<vtkInteractorStyleTrackballCamera>::New();
		rwi->SetInteractorStyle(style);
	}

	//z를 누르면 Data 저장
	if (key == "z")
	{
		vtkSmartPointer<vtkOBJWriter> writer = vtkSmartPointer<vtkOBJWriter>::New();
		for (int i = 0; i < polyDataPairVec.size(); ++i)
		{
			vtkSmartPointer<vtkTransform> transform = vtkSmartPointer<vtkTransform>::New();
			transform->SetMatrix(polyDataPairVec[i].second);

			vtkSmartPointer<vtkTransformFilter> transformFilter = vtkSmartPointer<vtkTransformFilter>::New();
			transformFilter->SetInputData(polyDataPairVec[i].first);
			transformFilter->SetTransform(transform);
			transformFilter->Update();

			std::string fileName = "../data/saveMesh" + std::to_string(i) + ".obj";
			writer->SetFileName(fileName.c_str());
			writer->SetInputData(transformFilter->GetOutput());
			writer->Update();
		}

	}
	
	if (key == "space" || key == "4" || key == "5" || key == "6" || key == "a")
	{
		//Space를 누르면 Rendering On Off
		if (key == "space")
		{
			isVisualization = !isVisualization;
			std::cout << "Visualization On Off Mode : Now State = " << isVisualization << std::endl;
			style1->SetBBoxVisualizationOnOff(isVisualization);
		}

		//a를 누르면 TrackballActor 모드로 전환(각 Mesh를 각각 Transform 가능)
		if (key == "a")
		{
			std::cout << "Camera Mode    : Trackball Actor Mode" << std::endl;
			isVisualization = false;
			style1->SetBBoxVisualizationOnOff(isVisualization);
		}

		if (key == "4")
		{
			//4를 누르면 Bounding Box가 AABB로 표현
			std::cout << "Bbox Mode     : AABB" << std::endl;
			style1->SetBBoxMode(BB_MODE::AABB);
		}
		else if (key == "5")
		{
			//5를 누르면 Bounding Box가 OBB로 표현
			std::cout << "Bbox Mode     : OBB" << std::endl;
			style1->SetBBoxMode(BB_MODE::OBB);
		}
		else if (key == "6")
		{
			//6를 누르면 Bounding Box가 Sphere로 표현
			std::cout << "Bbox Mode     : SPHERE" << std::endl;
			std::vector<double> radiusVec;
			for (int i = 0; i < polyDataPairVec.size(); ++i)
			{
				radiusVec.push_back(CBBox_RenderUtil::ComputeRadius(polyDataPairVec[i].first));
			}
			style1->SetBBoxMode(BB_MODE::SPHERE);
			style1->SetRadius(radiusVec);

			
		}
		style1->Initialize();
		for (int i = 0; i < polyDataPairVec.size(); ++i)
		{
			style1->SetPolyData(polyDataPairVec[i].first, polyDataPairVec[i].second, colorVec[i]);
		}
		
		style1->SetRenderer(renderer);
		rwi->SetInteractorStyle(style1);
	}



}