#pragma once

#include <vtkCommand.h>
#include <vtkSmartPointer.h>

#include <iostream>
#include <vector>

class vtkPolyData;
class vtkMatrix4x4;
class vtkRenderer;
class CExample_3_MouseInteractorStyle;

class  CExample_3_KeyPressCallback : public vtkCommand
{
public:
	static CExample_3_KeyPressCallback *New()
	{
		return new CExample_3_KeyPressCallback;
	}
	CExample_3_KeyPressCallback();
	virtual void Execute(vtkObject *caller, unsigned long, void*);

	void Initialize();

	void SetRenderer(vtkSmartPointer<vtkRenderer> vR);
	void SetPolyData(vtkSmartPointer<vtkPolyData> vPD, vtkSmartPointer<vtkMatrix4x4> matrix, std::string color);

private:

	vtkSmartPointer<vtkRenderer> renderer;

	std::vector<std::string> colorVec;
	std::vector<std::pair<vtkSmartPointer<vtkPolyData>, vtkSmartPointer<vtkMatrix4x4>>> polyDataPairVec;
	bool isVisualization;

	vtkSmartPointer<CExample_3_MouseInteractorStyle> style1 = vtkSmartPointer<CExample_3_MouseInteractorStyle>::New();
};