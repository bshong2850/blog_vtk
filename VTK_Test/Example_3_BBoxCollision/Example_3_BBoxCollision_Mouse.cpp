
#include "Example_3_BBoxCollision_Mouse.h"
#include "../Util/BoundingBox_RenderUtil.h"
#include "../Util/MeshUtil.h"
#include <vtkRenderer.h>

#include "vtkRenderWindowInteractor.h"
#include <vtkRenderWindow.h>
#include <vtkRenderer.h>
#include <vtkPropPicker.h>
#include <vtkMapper.h>
#include <vtkPolyData.h>
#include <vtkProperty.h>
#include <vtkMatrix4x4.h>

#include <set>

#define TimeLog 0

//생성자
//Bounding Box Mode를 AABB로 Initialize
CExample_3_MouseInteractorStyle::CExample_3_MouseInteractorStyle()
{
	bMode = BB_MODE::AABB;
}

//SetPolyData
//polyDataPairVec에 PolyData Matrix Pair를 Set하는 함수
void CExample_3_MouseInteractorStyle::SetPolyData(vtkSmartPointer<vtkPolyData> vPD, vtkSmartPointer<vtkMatrix4x4> matrix, std::string color)
{
	polyDataPairVec.push_back(std::pair<vtkSmartPointer<vtkPolyData>, vtkSmartPointer<vtkMatrix4x4>>(vPD, matrix));
	colorVec.push_back(color);
}
//Initialize
//polyDataPairVec를 초기화 해주는 함수
void CExample_3_MouseInteractorStyle::Initialize()
{
	colorVec.clear();
	std::vector<std::string>().swap(colorVec);
		
	polyDataPairVec.clear();
	std::vector<std::pair<vtkSmartPointer<vtkPolyData>, vtkSmartPointer<vtkMatrix4x4>>>().swap(polyDataPairVec);
}

//SetRenderer
//Renderer Setting
void CExample_3_MouseInteractorStyle::SetRenderer(vtkSmartPointer<vtkRenderer> vR)
{
	this->renderer = vR;
}

//SetRadius
//Mesh 수 만큼의 radius를 vector로 Setting
void CExample_3_MouseInteractorStyle::SetRadius(std::vector<double> radiusVec)
{
	this->radiusVec = radiusVec;
}

//SetBBoxMode
//Bounding Box의 종류를 Setting
void CExample_3_MouseInteractorStyle::SetBBoxMode(BB_MODE m)
{
	this->bMode = m;
}

//SetBBoxVisualizationOnOff
//Bounding Box의 Rendering 여부를 Setting
void CExample_3_MouseInteractorStyle::SetBBoxVisualizationOnOff(bool isVisualization)
{
	this->isVisualization = isVisualization;
}


void CExample_3_MouseInteractorStyle::OnRightButtonDown()
{
	for (int i = 0; i < obbVec.size(); ++i)
	{
		std::cout << "   //////////////" << i << "/////////////" << std::endl;
		obbVec[i].PrintInfo();
	}
		
}

//OnMouseMove
//Mouse가 움직이고 있을 때 호출되는 함수
void CExample_3_MouseInteractorStyle::OnMouseMove()
{

	clock_t time;
	time = clock();

	std::vector<vtkSmartPointer<vtkActor>> totalActorVec;
	if (isVisualization)
	{
		BBoxInitialize();

		if (TimeLog)
		{
			std::cout << "Bbox Initialize Time = " << clock() - time << std::endl;
			time = clock();
		}
			

		std::set<int> collisionIndex = BBoxCollisionCheck();	


		if (TimeLog)
		{
			std::cout << "Bbox Collision Check Time = " << clock() - time << std::endl;
			time = clock();
		}

		for (int i = 0; i < polyDataPairVec.size(); ++i)
		{
			std::string color;
			auto tmp = collisionIndex.find(i);
			if (tmp == collisionIndex.end())
				color = "Lime";
			else
				color = "Red";
			
			if (bMode == BB_MODE::AABB)
				totalActorVec.push_back(CBBox_RenderUtil::GetAABBLineActor(polyDataPairVec[i].first, polyDataPairVec[i].second, color));
			else if (bMode == BB_MODE::OBB)
				totalActorVec.push_back(CBBox_RenderUtil::GetOBBLineActor(polyDataPairVec[i].first, polyDataPairVec[i].second, color));
			else if (bMode == BB_MODE::SPHERE)
				totalActorVec.push_back(CBBox_RenderUtil::GetSphereLineActor(polyDataPairVec[i].first, polyDataPairVec[i].second, radiusVec[i], color));
		}

	}

	for (int i = 0; i < polyDataPairVec.size(); ++i)
	{
		auto actor = CMeshUtil::PolyDataToActor(polyDataPairVec[i].first, colorVec[i]);
		actor->SetUserMatrix(polyDataPairVec[i].second);
		totalActorVec.push_back(actor);
	}

	if (TimeLog)
	{
		std::cout << "Set Actor Time = " << clock() - time << std::endl;
		time = clock();
	}
	renderer->RemoveAllViewProps();
	renderer->UseHiddenLineRemovalOn();

	for (int i = 0; i < totalActorVec.size(); ++i)
	{
		renderer->AddActor(totalActorVec[i]);
	}

	if (TimeLog)
	{
		std::cout << "Rendering Time = " << clock() - time << std::endl << std::endl;
		time = clock();
	}
	renderer->GetRenderWindow()->Render();

	vtkInteractorStyleTrackballActor::OnMouseMove();
}

//BBoxInitialize
//Bounding Box Mode에 따라 각 Bbox 채우기
void CExample_3_MouseInteractorStyle::BBoxInitialize()
{
	aabbVec.clear();
	std::vector<CBBox_AABB>().swap(aabbVec);
	obbVec.clear();
	std::vector<CBBox_OBB>().swap(obbVec);
	sphereBboxVec.clear();
	std::vector<CBBox_SPHERE>().swap(sphereBboxVec);

	for (int i = 0; i < polyDataPairVec.size(); ++i)
	{
		auto transformPolyData = CMeshUtil::SetTransform(polyDataPairVec[i].first, polyDataPairVec[i].second);
		if (bMode == BB_MODE::AABB)
		{
			double bound[6];
			transformPolyData->GetBounds(bound);
			double min[3] = { bound[0], bound[2], bound[4] };
			double max[3] = { bound[1], bound[3], bound[5] };
			CBBox_AABB aabb = CBBox_AABB(min, max);
			aabbVec.push_back(aabb);
		}
		else if (bMode == BB_MODE::OBB)
		{
			double axis[3][3];
			double extent[3];
			double center[3];
			// axis extent center 구하는 부분
			CBBox_RenderUtil::GetOBBData(polyDataPairVec[i].first, polyDataPairVec[i].second, axis, extent, center);
			CBBox_OBB obb = CBBox_OBB(axis, extent, center);

			//obb.PrintInfo();

			obbVec.push_back(obb);
		}
		else if (bMode == BB_MODE::SPHERE)
		{
			double center[3];
			transformPolyData->GetCenter(center);
			sphereBboxVec.push_back(CBBox_SPHERE(center, radiusVec[i]));
		}
	}
}

//BBoxCollisionCheck
//Bounding Box Mode에 따라 각 Bbox 의 Collision 된 Index 반환
std::set<int> CExample_3_MouseInteractorStyle::BBoxCollisionCheck()
{
	std::set<int> collisionIndex;

	size_t vecSize = 0;

	if (bMode == BB_MODE::AABB)
		vecSize = aabbVec.size();
	else if (bMode == BB_MODE::OBB)
		vecSize = obbVec.size();
	else if (bMode == BB_MODE::SPHERE)
		vecSize = sphereBboxVec.size();
	
	for (int i = 0; i < vecSize - 1; ++i)
	{
		for (int j = i + 1; j < vecSize; ++j)
		{
			bool isCollision = false;
			if (bMode == BB_MODE::AABB)
				isCollision = aabbVec[i].isCollision(aabbVec[j]);
			else if (bMode == BB_MODE::OBB)
				isCollision = obbVec[i].isCollision(obbVec[j]);
			else if (bMode == BB_MODE::SPHERE)
				isCollision = sphereBboxVec[i].isCollision(sphereBboxVec[j]);

			if (isCollision)
			{
				collisionIndex.insert(i);
				collisionIndex.insert(j);
			}				
		}
	}

	return collisionIndex;
}

