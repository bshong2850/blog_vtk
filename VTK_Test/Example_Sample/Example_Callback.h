#pragma once

#include <vtkCommand.h>
#include <vtkSmartPointer.h>

#include <iostream>
#include <vector>
#include <memory>

class vtkPolyData;
class vtkMatrix4x4;
class vtkRenderer;
class vtkActor;

//다른 Example 만들 때 Sample로 불러오기 위한 Code
class  CExampleKeyPressCallback : public vtkCommand
{
public:
	static CExampleKeyPressCallback *New()
	{
		return new CExampleKeyPressCallback;
	}
	CExampleKeyPressCallback();
	virtual void Execute(vtkObject *caller, unsigned long, void*);
	void SetRenderer(vtkSmartPointer<vtkRenderer> vR);
	void SetDataVec(vtkSmartPointer<vtkPolyData> vpdVec, vtkSmartPointer<vtkMatrix4x4> matrixVec);

private:
	vtkSmartPointer<vtkRenderer> renderer;

	vtkSmartPointer<vtkPolyData> polyData;
	vtkSmartPointer<vtkMatrix4x4> matrix;
};