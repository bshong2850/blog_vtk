#include "Example_Callback.h"

#include <vtkRenderWindowInteractor.h>
#include <vtkInteractorStyleTrackballActor.h>
#include <vtkInteractorStyleTrackballCamera.h>
#include <vtkTransform.h>
#include <vtkTransformPolyDataFilter.h>
#include <vtkOBJWriter.h>
#include <vtkCubeSource.h>
#include <vtkSphereSource.h>
#include <vtkProperty.h>

#include <vtkRenderer.h>
#include <vtkRenderWindow.h>

//생성자
CExampleKeyPressCallback::CExampleKeyPressCallback()
{

}

//SetDataVec
//PolyData 및 Matrix를 set 해주는 함수
void CExampleKeyPressCallback::SetDataVec(vtkSmartPointer<vtkPolyData> polyData, vtkSmartPointer<vtkMatrix4x4> matrix)
{
	this->polyData = polyData;
	this->matrix = matrix;
}

//SetRenderer
//Renderer를 set 해주는 함수
void CExampleKeyPressCallback::SetRenderer(vtkSmartPointer<vtkRenderer> vR)
{
	this->renderer = vR;
}


//Execute
//Keypress Event가 있을 때 불리는 함수
void CExampleKeyPressCallback::Execute(vtkObject *caller, unsigned long, void*)
{
	vtkRenderWindowInteractor *rwi = reinterpret_cast<vtkRenderWindowInteractor *>(caller);
	std::string key = rwi->GetKeySym();
	vtkSmartPointer<vtkInteractorStyle> style;
	
	if (key == "a")
	{
		std::cout << "Actor Mode     : Trackball Actor Mode" << std::endl;
		style = vtkSmartPointer<vtkInteractorStyleTrackballActor>::New();
		rwi->SetInteractorStyle(style);
	}
	if (key == "t")
	{
		std::cout << "Camera Mode    : Trackball Camera Mode" << std::endl;
		style = vtkSmartPointer<vtkInteractorStyleTrackballCamera>::New();
		rwi->SetInteractorStyle(style);
	}
	if (key == "b")
	{
		
	}
	
}