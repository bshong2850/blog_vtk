#pragma once
#include <iostream>
#include <memory>

#include "vtkSmartPointer.h"

class CExample_1_CollisionDetection
{
public:

	CExample_1_CollisionDetection() {};
	~CExample_1_CollisionDetection() {};

	static void CollisionDetectionAuto();
	static void CollisionDetectionMove();

private:
};