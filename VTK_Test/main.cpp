#pragma once
#include <vtkAutoInit.h>

VTK_MODULE_INIT(vtkRenderingOpenGL2);
VTK_MODULE_INIT(vtkInteractionStyle);
VTK_MODULE_INIT(vtkRenderingFreeType);

#include "Example_0_CubeRendering/Example_0_CubeRendering.h"
#include "Example_1_CollisionDetection/Example_1_CollisionDetection.h"
#include "Example_2_BoundingBox/Example_2_BoundingBox.h"
#include "Example_3_BBoxCollision/Example_3_BBoxCollision.h"
#include "Example_4_BVH/Example_4_BVH.h"
#include "Example_5_BVHCollision/Example_5_BVHCollision.h"
#include "Example_6_Parameterization/Example_6_Parameterization.h"

int main()
{
	/*CExample_1_CollisionDetection::CollisionDetectionAuto();
	CExample_1_CollisionDetection::CollisionDetectionMove();*/

	/*CExample_2_BoundingBox boundingBox;
	boundingBox.boundingBox();*/

	CExample_3_BBoxCollision bBoxCollision;
	bBoxCollision.BboxCollisionDetection();

	/*CExample_4_BVH bvh;
	bvh.Run();*/
	
	/*CExample_5_BVHCollision bvhCollision;
	bvhCollision.Run();*/

	/*CExample_6_Parameterization parameterization;
	parameterization.Run();*/

	/*CExampleMain exampleMain;
	exampleMain.Run();*/
}